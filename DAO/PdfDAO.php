<?php

	class PdfDAO
	{
		
		public static function getPdf($id_comp){
			$database=DataSource::getDatabase();
			
			$sql="select * from compound where id_comp= '".$id_comp."';";
			$data_table = $database->query($sql)->fetchAll();
			
			$compound = null;
			if (count($data_table) == 1){
				foreach ($data_table as $clave => $valor){
					$id_comp= $data_table[$clave]['id_comp'];
					$sql2="select * from names where id_comp = '" . $id_comp . "';";
					$data_table2 = $database->query($sql2)->fetchAll();
					
					$sql3="select distinct * from comp_class natural join class where id_comp = '" . $id_comp . "';";
					$data_table3 = $database->query($sql3)->fetchAll();
					
					$sql4="select distinct * from comp_ext natural join ext_db where id_comp = '" . $id_comp . "';";
					$data_table4 = $database->query($sql4)->fetchAll();
					
					$compound = new Compound();
					$compound->setId_comp($data_table[$clave]['id_comp']);
					$compound->setMol_weight($data_table[$clave]['mol_weight']);
					$compound->setFormula($data_table[$clave]['formula']);
					$compound->setType($data_table[$clave]['id_type']);
					$compound->setActivity($data_table[$clave]['activity']);
					$compound->setOrigen($data_table[$clave]['origen']);
					$arr_name=array();
					foreach ($data_table2 as $names){
						array_push($arr_name,$names['name']);
					}
					$compound->setNames($arr_name);
					
					$arr_class=array();
					foreach ($data_table3 as $class){
						array_push($arr_class,[$class['name_class'],$class['id_class']]);
					}
					$compound->setClass($arr_class);
					
					$arr_db=array();
					foreach ($data_table4 as $db){
						array_push($arr_db,[$db['db_name'],($db['link'] . $db['code_db'])]);
					}
					$compound->setExt_db($arr_db);
				}
				return $compound;
			}
			else{
				return null;
			}
			
		}
		
	}
?>
