<?php

	class PropertiesDAO
	{
		
		public static function getPropertiesById($id_comp){
			$database=DataSource::getDatabase();
			
			$sql="select * from properties where id_comp= '".$id_comp."';";
			$data_tables = $database->query($sql)->fetchAll();
			
			$properties = null;
			if (count($data_tables) == 1){
				$properties = new Properties();
				foreach ($data_tables as $clave => $valor){
						//~ 
					array_push($properties->id_comp,(int)$data_tables[$clave]['id_comp']);
					array_push($properties->stars,(int)$data_tables[$clave]['stars']);
					array_push($properties->amine,(int)$data_tables[$clave]['amine']);
					array_push($properties->amidine,(int)$data_tables[$clave]['amidine']);
					array_push($properties->acid,(int)$data_tables[$clave]['acid']);
					array_push($properties->amide,(int)$data_tables[$clave]['amide']);
					array_push($properties->rotor,(int)$data_tables[$clave]['rotor']);
					array_push($properties->rtvfg,(int)$data_tables[$clave]['rtvfg']);
					array_push($properties->cns,(int)$data_tables[$clave]['cns']);
					array_push($properties->mol_mw,(int)$data_tables[$clave]['mol_mw']);
					array_push($properties->dipole,(int)$data_tables[$clave]['dipole']);
					array_push($properties->sasa,(int)$data_tables[$clave]['sasa']);
					array_push($properties->fosa,(int)$data_tables[$clave]['fosa']);
					array_push($properties->fisa,(int)$data_tables[$clave]['fisa']);
					array_push($properties->pisa,(int)$data_tables[$clave]['pisa']);
					array_push($properties->wpsa,(int)$data_tables[$clave]['wpsa']);
					array_push($properties->volume,(int)$data_tables[$clave]['volume']);
					array_push($properties->donorhb,(int)$data_tables[$clave]['donorhb']);
					array_push($properties->accpthb,(int)$data_tables[$clave]['accpthb']);
					array_push($properties->dip2_v,(int)$data_tables[$clave]['dip2_v']);
					array_push($properties->acxdn5_sa,(int)$data_tables[$clave]['acxdn5_sa']);
					array_push($properties->glob,(int)$data_tables[$clave]['glob']);
					array_push($properties->qppolrz,(int)$data_tables[$clave]['qppolrz']);
					array_push($properties->qplogpc16,(int)$data_tables[$clave]['qplogpc16']);
					array_push($properties->qplogpoct,(int)$data_tables[$clave]['qplogpoct']);
					array_push($properties->qplogpw,(int)$data_tables[$clave]['qplogpw']);
					array_push($properties->qplogpo_w,(int)$data_tables[$clave]['qplogpo_w']);
					array_push($properties->qplogs,(int)$data_tables[$clave]['qplogs']);
					array_push($properties->ciqplogs,(int)$data_tables[$clave]['ciqplogs']);
					array_push($properties->qplogherg,(int)$data_tables[$clave]['qplogherg']);
					array_push($properties->qppcaco,(int)$data_tables[$clave]['qppcaco']);
					array_push($properties->qplogbb,(int)$data_tables[$clave]['qplogbb']);
					array_push($properties->qppmdck,(int)$data_tables[$clave]['qppmdck']);
					array_push($properties->qplogkp,(int)$data_tables[$clave]['qplogkp']);
					array_push($properties->ip,(int)$data_tables[$clave]['ip']);
					array_push($properties->ea,(int)$data_tables[$clave]['ea']);
					array_push($properties->metab,(int)$data_tables[$clave]['metab']);
					array_push($properties->qplogkhsa,(int)$data_tables[$clave]['qplogkhsa']);
					array_push($properties->humanoralabsorption,(int)$data_tables[$clave]['humanoralabsorption']);
					array_push($properties->percenthumanoralabsorption,(int)$data_tables[$clave]['percenthumanoralabsorption']);
					array_push($properties->safluorine,(int)$data_tables[$clave]['safluorine']);
					array_push($properties->saamideo,(int)$data_tables[$clave]['saamideo']);
					array_push($properties->psa,(int)$data_tables[$clave]['psa']);
					array_push($properties->nando,(int)$data_tables[$clave]['nando']);
					array_push($properties->ruleoffive,(int)$data_tables[$clave]['ruleoffive']);
					array_push($properties->ringatoms,(int)$data_tables[$clave]['ringatoms']);
					array_push($properties->in34,(int)$data_tables[$clave]['in34']);
					array_push($properties->in56,(int)$data_tables[$clave]['in56']);
					array_push($properties->noncon,(int)$data_tables[$clave]['noncon']);
					array_push($properties->nonhatm,(int)$data_tables[$clave]['nonhatm']);
					array_push($properties->ruleofthree,(int)$data_tables[$clave]['ruleofthree']);
					array_push($properties->jm,(int)$data_tables[$clave]['jm']);
					//~ 
				}
				$properties->sortProp();
				return $properties;
			}
			else{
				return null;
			}
			
		}	
		public static function getProperties(){
			$database=DataSource::getDatabase();
			$data_table=null; 
			
			$sql="select * from properties;";
			$data_tables = $database->query($sql)->fetchAll();
			
			$properties = null;
			if (count($data_tables) > 0){
				$properties = new Properties();
				foreach ($data_tables as $clave => $valor){
						//~ 
					array_push($properties->id_comp,(int)$data_tables[$clave]['id_comp']);
					array_push($properties->stars,(int)$data_tables[$clave]['stars']);
					array_push($properties->amine,(int)$data_tables[$clave]['amine']);
					array_push($properties->amidine,(int)$data_tables[$clave]['amidine']);
					array_push($properties->acid,(int)$data_tables[$clave]['acid']);
					array_push($properties->amide,(int)$data_tables[$clave]['amide']);
					array_push($properties->rotor,(int)$data_tables[$clave]['rotor']);
					array_push($properties->rtvfg,(int)$data_tables[$clave]['rtvfg']);
					array_push($properties->cns,(int)$data_tables[$clave]['cns']);
					array_push($properties->mol_mw,(int)$data_tables[$clave]['mol_mw']);
					array_push($properties->dipole,(int)$data_tables[$clave]['dipole']);
					array_push($properties->sasa,(int)$data_tables[$clave]['sasa']);
					array_push($properties->fosa,(int)$data_tables[$clave]['fosa']);
					array_push($properties->fisa,(int)$data_tables[$clave]['fisa']);
					array_push($properties->pisa,(int)$data_tables[$clave]['pisa']);
					array_push($properties->wpsa,(int)$data_tables[$clave]['wpsa']);
					array_push($properties->volume,(int)$data_tables[$clave]['volume']);
					array_push($properties->donorhb,(int)$data_tables[$clave]['donorhb']);
					array_push($properties->accpthb,(int)$data_tables[$clave]['accpthb']);
					array_push($properties->dip2_v,(int)$data_tables[$clave]['dip2_v']);
					array_push($properties->acxdn5_sa,(int)$data_tables[$clave]['acxdn5_sa']);
					array_push($properties->glob,(int)$data_tables[$clave]['glob']);
					array_push($properties->qppolrz,(int)$data_tables[$clave]['qppolrz']);
					array_push($properties->qplogpc16,(int)$data_tables[$clave]['qplogpc16']);
					array_push($properties->qplogpoct,(int)$data_tables[$clave]['qplogpoct']);
					array_push($properties->qplogpw,(int)$data_tables[$clave]['qplogpw']);
					array_push($properties->qplogpo_w,(int)$data_tables[$clave]['qplogpo_w']);
					array_push($properties->qplogs,(int)$data_tables[$clave]['qplogs']);
					array_push($properties->ciqplogs,(int)$data_tables[$clave]['ciqplogs']);
					array_push($properties->qplogherg,(int)$data_tables[$clave]['qplogherg']);
					array_push($properties->qppcaco,(int)$data_tables[$clave]['qppcaco']);
					array_push($properties->qplogbb,(int)$data_tables[$clave]['qplogbb']);
					array_push($properties->qppmdck,(int)$data_tables[$clave]['qppmdck']);
					array_push($properties->qplogkp,(int)$data_tables[$clave]['qplogkp']);
					array_push($properties->ip,(int)$data_tables[$clave]['ip']);
					array_push($properties->ea,(int)$data_tables[$clave]['ea']);
					array_push($properties->metab,(int)$data_tables[$clave]['metab']);
					array_push($properties->qplogkhsa,(int)$data_tables[$clave]['qplogkhsa']);
					array_push($properties->humanoralabsorption,(int)$data_tables[$clave]['humanoralabsorption']);
					array_push($properties->percenthumanoralabsorption,(int)$data_tables[$clave]['percenthumanoralabsorption']);
					array_push($properties->safluorine,(int)$data_tables[$clave]['safluorine']);
					array_push($properties->saamideo,(int)$data_tables[$clave]['saamideo']);
					array_push($properties->psa,(int)$data_tables[$clave]['psa']);
					array_push($properties->nando,(int)$data_tables[$clave]['nando']);
					array_push($properties->ruleoffive,(int)$data_tables[$clave]['ruleoffive']);
					array_push($properties->ringatoms,(int)$data_tables[$clave]['ringatoms']);
					array_push($properties->in34,(int)$data_tables[$clave]['in34']);
					array_push($properties->in56,(int)$data_tables[$clave]['in56']);
					array_push($properties->noncon,(int)$data_tables[$clave]['noncon']);
					array_push($properties->nonhatm,(int)$data_tables[$clave]['nonhatm']);
					array_push($properties->ruleofthree,(int)$data_tables[$clave]['ruleofthree']);
					array_push($properties->jm,(int)$data_tables[$clave]['jm']);
					//~ 
				}
				$properties->sortProp();
				return $properties;
			}
			else{
				return null;
			}
			
		}
		public static function getPropertiesClass($id_class){
			$database=DataSource::getDatabase();
			$data_tables="";
			
			$sql="WITH RECURSIVE nodes(id_class,name_class,id_father) AS (
				SELECT s1.id_class, s1.name_class, s1.id_father
				FROM class s1 WHERE id_father = '".$id_class."'
					UNION
				SELECT s2.id_class, s2.name_class, s2.id_father
				FROM class s2, nodes s1 WHERE s2.id_father = s1.id_class
			)

			select * from properties natural join comp_class where id_class = '".$id_class."' or id_class in (
			SELECT id_class FROM nodes
			);";
			$data_tables = $database->query($sql)->fetchAll();

			if (count($data_tables) > 0){
				$properties = new Properties();
				foreach ($data_tables as $clave => $valor){
						//~ 
					array_push($properties->id_comp,(int)$data_tables[$clave]['id_comp']);
					array_push($properties->stars,(int)$data_tables[$clave]['stars']);
					array_push($properties->amine,(int)$data_tables[$clave]['amine']);
					array_push($properties->amidine,(int)$data_tables[$clave]['amidine']);
					array_push($properties->acid,(int)$data_tables[$clave]['acid']);
					array_push($properties->amide,(int)$data_tables[$clave]['amide']);
					array_push($properties->rotor,(int)$data_tables[$clave]['rotor']);
					array_push($properties->rtvfg,(int)$data_tables[$clave]['rtvfg']);
					array_push($properties->cns,(int)$data_tables[$clave]['cns']);
					array_push($properties->mol_mw,(int)$data_tables[$clave]['mol_mw']);
					array_push($properties->dipole,(int)$data_tables[$clave]['dipole']);
					array_push($properties->sasa,(int)$data_tables[$clave]['sasa']);
					array_push($properties->fosa,(int)$data_tables[$clave]['fosa']);
					array_push($properties->fisa,(int)$data_tables[$clave]['fisa']);
					array_push($properties->pisa,(int)$data_tables[$clave]['pisa']);
					array_push($properties->wpsa,(int)$data_tables[$clave]['wpsa']);
					array_push($properties->volume,(int)$data_tables[$clave]['volume']);
					array_push($properties->donorhb,(int)$data_tables[$clave]['donorhb']);
					array_push($properties->accpthb,(int)$data_tables[$clave]['accpthb']);
					array_push($properties->dip2_v,(int)$data_tables[$clave]['dip2_v']);
					array_push($properties->acxdn5_sa,(int)$data_tables[$clave]['acxdn5_sa']);
					array_push($properties->glob,(int)$data_tables[$clave]['glob']);
					array_push($properties->qppolrz,(int)$data_tables[$clave]['qppolrz']);
					array_push($properties->qplogpc16,(int)$data_tables[$clave]['qplogpc16']);
					array_push($properties->qplogpoct,(int)$data_tables[$clave]['qplogpoct']);
					array_push($properties->qplogpw,(int)$data_tables[$clave]['qplogpw']);
					array_push($properties->qplogpo_w,(int)$data_tables[$clave]['qplogpo_w']);
					array_push($properties->qplogs,(int)$data_tables[$clave]['qplogs']);
					array_push($properties->ciqplogs,(int)$data_tables[$clave]['ciqplogs']);
					array_push($properties->qplogherg,(int)$data_tables[$clave]['qplogherg']);
					array_push($properties->qppcaco,(int)$data_tables[$clave]['qppcaco']);
					array_push($properties->qplogbb,(int)$data_tables[$clave]['qplogbb']);
					array_push($properties->qppmdck,(int)$data_tables[$clave]['qppmdck']);
					array_push($properties->qplogkp,(int)$data_tables[$clave]['qplogkp']);
					array_push($properties->ip,(int)$data_tables[$clave]['ip']);
					array_push($properties->ea,(int)$data_tables[$clave]['ea']);
					array_push($properties->metab,(int)$data_tables[$clave]['metab']);
					array_push($properties->qplogkhsa,(int)$data_tables[$clave]['qplogkhsa']);
					array_push($properties->humanoralabsorption,(int)$data_tables[$clave]['humanoralabsorption']);
					array_push($properties->percenthumanoralabsorption,(int)$data_tables[$clave]['percenthumanoralabsorption']);
					array_push($properties->safluorine,(int)$data_tables[$clave]['safluorine']);
					array_push($properties->saamideo,(int)$data_tables[$clave]['saamideo']);
					array_push($properties->psa,(int)$data_tables[$clave]['psa']);
					array_push($properties->nando,(int)$data_tables[$clave]['nando']);
					array_push($properties->ruleoffive,(int)$data_tables[$clave]['ruleoffive']);
					array_push($properties->ringatoms,(int)$data_tables[$clave]['ringatoms']);
					array_push($properties->in34,(int)$data_tables[$clave]['in34']);
					array_push($properties->in56,(int)$data_tables[$clave]['in56']);
					array_push($properties->noncon,(int)$data_tables[$clave]['noncon']);
					array_push($properties->nonhatm,(int)$data_tables[$clave]['nonhatm']);
					array_push($properties->ruleofthree,(int)$data_tables[$clave]['ruleofthree']);
					array_push($properties->jm,(int)$data_tables[$clave]['jm']);
					//~ 
				}
				$properties->sortProp();
				return $properties;
			}
			else{
				return null;
			}
		}
		public static function getPropertiesSearch($id_comp,$name,$mol_weight_min,$mol_weight_max,$formula,$activity,$origen,$class,$ext_db){
			$database=DataSource::getDatabase();
			$data_tables="";
			
			
			$sql="select * from compound natural join properties where 1=1";
			if($id_comp!=null){$sql.=" and id_comp::TEXT like '%".$id_comp."%'";}
			if($formula!=null){$sql.=" and formula like '%".$formula."%'";}
			if($mol_weight_min!=null){$sql.=" and mol_weight > '".$mol_weight_min."'";}
			if($mol_weight_max!=null){$sql.=" and mol_weight < '".$mol_weight_max."'";}
			if($activity!=null){$sql.=" and activity like '%".$activity."%'";}
			if($origen!=null){$sql.=" and activity like '%".$origen."%'";}
			$sql.=";";
			//~ return $sql;
			$data_tables = $database->query($sql)->fetchAll();
			if (count($data_tables) > 0){
				$properties = new Properties();
				foreach ($data_tables as $clave => $valor){
						//~ 
					array_push($properties->id_comp,(int)$data_tables[$clave]['id_comp']);
					array_push($properties->stars,(int)$data_tables[$clave]['stars']);
					array_push($properties->amine,(int)$data_tables[$clave]['amine']);
					array_push($properties->amidine,(int)$data_tables[$clave]['amidine']);
					array_push($properties->acid,(int)$data_tables[$clave]['acid']);
					array_push($properties->amide,(int)$data_tables[$clave]['amide']);
					array_push($properties->rotor,(int)$data_tables[$clave]['rotor']);
					array_push($properties->rtvfg,(int)$data_tables[$clave]['rtvfg']);
					array_push($properties->cns,(int)$data_tables[$clave]['cns']);
					array_push($properties->mol_mw,(int)$data_tables[$clave]['mol_mw']);
					array_push($properties->dipole,(int)$data_tables[$clave]['dipole']);
					array_push($properties->sasa,(int)$data_tables[$clave]['sasa']);
					array_push($properties->fosa,(int)$data_tables[$clave]['fosa']);
					array_push($properties->fisa,(int)$data_tables[$clave]['fisa']);
					array_push($properties->pisa,(int)$data_tables[$clave]['pisa']);
					array_push($properties->wpsa,(int)$data_tables[$clave]['wpsa']);
					array_push($properties->volume,(int)$data_tables[$clave]['volume']);
					array_push($properties->donorhb,(int)$data_tables[$clave]['donorhb']);
					array_push($properties->accpthb,(int)$data_tables[$clave]['accpthb']);
					array_push($properties->dip2_v,(int)$data_tables[$clave]['dip2_v']);
					array_push($properties->acxdn5_sa,(int)$data_tables[$clave]['acxdn5_sa']);
					array_push($properties->glob,(int)$data_tables[$clave]['glob']);
					array_push($properties->qppolrz,(int)$data_tables[$clave]['qppolrz']);
					array_push($properties->qplogpc16,(int)$data_tables[$clave]['qplogpc16']);
					array_push($properties->qplogpoct,(int)$data_tables[$clave]['qplogpoct']);
					array_push($properties->qplogpw,(int)$data_tables[$clave]['qplogpw']);
					array_push($properties->qplogpo_w,(int)$data_tables[$clave]['qplogpo_w']);
					array_push($properties->qplogs,(int)$data_tables[$clave]['qplogs']);
					array_push($properties->ciqplogs,(int)$data_tables[$clave]['ciqplogs']);
					array_push($properties->qplogherg,(int)$data_tables[$clave]['qplogherg']);
					array_push($properties->qppcaco,(int)$data_tables[$clave]['qppcaco']);
					array_push($properties->qplogbb,(int)$data_tables[$clave]['qplogbb']);
					array_push($properties->qppmdck,(int)$data_tables[$clave]['qppmdck']);
					array_push($properties->qplogkp,(int)$data_tables[$clave]['qplogkp']);
					array_push($properties->ip,(int)$data_tables[$clave]['ip']);
					array_push($properties->ea,(int)$data_tables[$clave]['ea']);
					array_push($properties->metab,(int)$data_tables[$clave]['metab']);
					array_push($properties->qplogkhsa,(int)$data_tables[$clave]['qplogkhsa']);
					array_push($properties->humanoralabsorption,(int)$data_tables[$clave]['humanoralabsorption']);
					array_push($properties->percenthumanoralabsorption,(int)$data_tables[$clave]['percenthumanoralabsorption']);
					array_push($properties->safluorine,(int)$data_tables[$clave]['safluorine']);
					array_push($properties->saamideo,(int)$data_tables[$clave]['saamideo']);
					array_push($properties->psa,(int)$data_tables[$clave]['psa']);
					array_push($properties->nando,(int)$data_tables[$clave]['nando']);
					array_push($properties->ruleoffive,(int)$data_tables[$clave]['ruleoffive']);
					array_push($properties->ringatoms,(int)$data_tables[$clave]['ringatoms']);
					array_push($properties->in34,(int)$data_tables[$clave]['in34']);
					array_push($properties->in56,(int)$data_tables[$clave]['in56']);
					array_push($properties->noncon,(int)$data_tables[$clave]['noncon']);
					array_push($properties->nonhatm,(int)$data_tables[$clave]['nonhatm']);
					array_push($properties->ruleofthree,(int)$data_tables[$clave]['ruleofthree']);
					array_push($properties->jm,(int)$data_tables[$clave]['jm']);
					//~ 
				}
				$properties->sortProp();
				return $properties;
			}
			else{
				return null;
			}
		}
		
	}
?>
