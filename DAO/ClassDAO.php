<?php
	class ClassDAO
	{
		public static function getTree(){
			$database=DataSource::getDatabase();
			$sql="select * from class order by level desc, id_father desc , id_class;";
			$data_table = $database->query($sql)->fetchAll();
			$arr_tree = array();
			$arr_tree_aux = array();
			foreach($data_table as $element){
				$node = new ClassVO();
				$node->setId_class($element["id_class"]);
				$node->setName_class($element["name_class"]);
				$node->setId_father($element["id_father"]);
				$node->setLevel($element["level"]);
				array_push($arr_tree, $node);
				array_push($arr_tree_aux, $element["id_class"]);
			}
			$end=array();
			foreach ($arr_tree as $key2 => $node){
				if($node->getId_father()!=null){
					$key=array_search($node->getId_father(), $arr_tree_aux);
					$aux=$arr_tree[$key]->getChildren();
					array_push($aux, $node);
					$arr_tree[$key]->setChildren($aux);
				}else {
					array_push($end, $node);
				}
			}
			return $end;
		}
		public static function getNode($id){
			$database=DataSource::getDatabase();
			$sql="select * from class where id_father = '" . $id . "';";
			$data_table = $database->query($sql)->fetchAll();
			$tree=array();
			if(count ($data_table)>0){
				foreach ($data_table as $subnode ){
					$node = new ClassVO();
					$node->setName_class($subnode['name_class']);
					$node->setId_class($subnode['id_class']);
					$node->setLevel($subnode['level']);
					$node->setSons(ClassDAO::getNode($subnode['id_class']));
					array_push($tree,$node);
				}
				
				return $tree;
			}
			else{
				return null;
			}
		}
	}
?>
