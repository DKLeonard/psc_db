<?php

	class CompoundDAO
	{
		
		public static function getCompoundById($id_comp){
			$database=DataSource::getDatabase();
			
			$sql="select * from compound where id_comp= '".$id_comp."';";
			$data_table = $database->query($sql)->fetchAll();
			
			$compound = null;
			if (count($data_table) == 1){
				foreach ($data_table as $clave => $valor){
					$id_comp= $data_table[$clave]['id_comp'];
					$sql2="select * from names where id_comp = '" . $id_comp . "';";
					$data_table2 = $database->query($sql2)->fetchAll();
					
					
					
					$sql3="	WITH RECURSIVE x(id_class,id_father,parents,last_id, depth) AS (
								SELECT id_class, id_father, ARRAY[id_class] AS parents, id_class AS last_id, 0 AS depth FROM class
								UNION ALL
								SELECT x.id_class, x.id_father, parents||t1.id_father, t1.id_father AS last_id, x.depth + 1
								FROM x 
									INNER JOIN class t1 
									ON (last_id= t1.id_class)
								WHERE t1.id_father IS NOT NULL
								)
								select * from class where id_class in (
									SELECT unnest(parents) as part
									FROM x 
									WHERE id_class=(
										select distinct id_class from comp_class natural join class where id_comp = 144 limit 1
										)
									group by part
									order by part asc
							);";
					$data_table3 = $database->query($sql3)->fetchAll();
					
					
					$sql4="select distinct * from comp_ext natural join ext_db where id_comp = '" . $id_comp . "';";
					$data_table4 = $database->query($sql4)->fetchAll();
					
					$compound = new Compound();
					$compound->setId_comp($data_table[$clave]['id_comp']);
					$compound->setMol_weight($data_table[$clave]['mol_weight']);
					$compound->setFormula($data_table[$clave]['formula']);
					$compound->setType($data_table[$clave]['id_type']);
					$compound->setActivity($data_table[$clave]['activity']);
					$compound->setOrigen($data_table[$clave]['origen']);
					$arr_name=array();
					foreach ($data_table2 as $names){
						array_push($arr_name,$names['name']);
					}
					$compound->setNames($arr_name);
					
					$arr_class=array();
					foreach ($data_table3 as $class){
						array_push($arr_class,[$class['name_class'],$class['id_class']]);
					}
					$compound->setClass($arr_class);
					
					$arr_db=array();
					foreach ($data_table4 as $db){
						array_push($arr_db,[$db['db_name'],($db['link'] . $db['code_db'])]);
					}
					$compound->setExt_db($arr_db);
				}
				return $compound;
			}
			else{
				return null;
			}
			
		}
		
		public static function getCompounds(){
			$database=DataSource::getDatabase();
			$data_table=null; 
			
			$sql="select * from compound;";
			$data_table = $database->query($sql)->fetchAll();
			
			$compound = null;
			$compounds = array();
			if (count($data_table) > 0){
				foreach ($data_table as $clave => $valor){
					$id_comp=$data_table[$clave]['id_comp'];
					
					$sql2="select * from names where id_comp = '" . $id_comp . "';";
					$data_table2 = $database->query($sql2)->fetchAll();
					
					$compound = new Compound();
					$compound->setId_comp($data_table[$clave]['id_comp']);
					$compound->setMol_weight($data_table[$clave]['mol_weight']);
					$compound->setFormula($data_table[$clave]['formula']);
					$compound->setType($data_table[$clave]['id_type']);
					$compound->setActivity($data_table[$clave]['activity']);
					$compound->setOrigen($data_table[$clave]['origen']);
					$compound->setNames($data_table2[0]['name']);
					array_push($compounds,$compound);
				}
				return $compounds;
			}
			else{
				return null;
			}
			
		}
		
		public static function searchCompound($id_comp,$name,$mol_weight_min,$mol_weight_max,$formula,$activity,$origen,$class,$ext_db){
			$database=DataSource::getDatabase();
			$data_table="";
			
			
			$sql="select * from compound where 1=1";
			if($id_comp!=null){$sql.=" and id_comp::TEXT like '%".$id_comp."%'";}
			if($formula!=null){$sql.=" and formula like '%".$formula."%'";}
			if($mol_weight_min!=null){$sql.=" and mol_weight > '".$mol_weight_min."'";}
			if($mol_weight_max!=null){$sql.=" and mol_weight < '".$mol_weight_max."'";}
			if($activity!=null){$sql.=" and activity like '%".$activity."%'";}
			if($origen!=null){$sql.=" and activity like '%".$origen."%'";}
			$sql.=";";
			//~ return $sql;
			$data_table = $database->query($sql)->fetchAll();
			
			
			//~ print_r( $sql); 
			$compound = null;
			$compounds = array();
			
			//~ return $data_table;
			if (count($data_table) > 0 and $data_table!=null){
				foreach ($data_table as $clave => $valor){
					$id_comp=$data_table[$clave]['id_comp'];
					
					$sql2="select * from names where id_comp = '" . $id_comp . "'";
					if($name!=null){$sql2.=" and name like '%".$name."%'";}
					$sql2.=";";
					$data_table2 = $database->query($sql2)->fetchAll();
					
					$sql3="select distinct * from comp_class natural join class where id_comp = '" . $id_comp . "'";
					if($class!=null){$sql3.=" and name_class like '%".$class."%'";}
					$sql3.=";";
					$data_table3 = $database->query($sql3)->fetchAll();
					
					$sql4="select distinct * from comp_ext natural join ext_db where id_comp = '" . $id_comp . "'";
					if($ext_db!=null){$sql4.=" and db_name like '%".$ext_db."%'";}
					$sql4.=";";
					$data_table4 = $database->query($sql4)->fetchAll();
					
					
					if ($data_table2 != null and $data_table3 != null and $data_table4 != null){
						$compound = new Compound();
						$compound->setId_comp($data_table[$clave]['id_comp']);
						$compound->setMol_weight($data_table[$clave]['mol_weight']);
						$compound->setFormula($data_table[$clave]['formula']);
						$compound->setType($data_table[$clave]['id_type']);
						$compound->setActivity($data_table[$clave]['activity']);
						$compound->setOrigen($data_table[$clave]['origen']);
						$compound->setNames($data_table2[0]['name']);
						$compound->setClass($data_table3[0]['name_class']);
						$compound->setExt_db($data_table4[0]['db_name']);
						array_push($compounds,$compound);
					}
				}
				 
				return $compounds;
			}
			else{
				return null;
			}
		}
		
		
		public static function getClass($id_class){
			$database=DataSource::getDatabase();
			$data_table="";
			
			$sql="WITH RECURSIVE nodes(id_class,name_class,id_father) AS (
				SELECT s1.id_class, s1.name_class, s1.id_father
				FROM class s1 WHERE id_father = '".$id_class."'
					UNION
				SELECT s2.id_class, s2.name_class, s2.id_father
				FROM class s2, nodes s1 WHERE s2.id_father = s1.id_class
			)

			select distinct * from compound natural join comp_class where id_class = '".$id_class."' or id_class in (
			SELECT id_class FROM nodes
			) order by id_comp;";
			$data_table = $database->query($sql)->fetchAll();
			
			$compound = null;
			$compounds = array();

			if (count($data_table) > 0 and $data_table!=null){
				foreach ($data_table as $clave => $valor){
					$id_comp=$data_table[$clave]['id_comp'];
					
					$sql2="select * from names where id_comp = '" . $id_comp . "';";
					$data_table2 = $database->query($sql2)->fetchAll();			
					
					if ($data_table2 != null){
						$compound = new Compound();
						$compound->setId_comp($data_table[$clave]['id_comp']);
						$compound->setMol_weight($data_table[$clave]['mol_weight']);
						$compound->setFormula($data_table[$clave]['formula']);
						$compound->setType($data_table[$clave]['id_type']);
						$compound->setActivity($data_table[$clave]['activity']);
						$compound->setOrigen($data_table[$clave]['origen']);
						$compound->setNames($data_table2[0]['name']);
						array_push($compounds,$compound);
					}
				}
				 
				return $compounds;
			}
			else{
				return null;
			}
		}
		
		
		
		
		
		public static function complexQuery($sql){
			$database=DataSource::getDatabase();
			$data_table = $database->query($sql)->fetchAll(PDO::FETCH_ASSOC);
			$compound = null;
			$compounds = array();
			
			//~ return $data_table;
			if (count($data_table) > 0){
				foreach ($data_table as $clave => $valor){
					$id_comp=$data_table[$clave]['id_comp'];
					
					$sql2="select * from names where id_comp = '" . $id_comp . "'";
					if($name!=null){$sql2.=" and name like '%".$name."%'";}
					$sql2.=";";
					$data_table2 = $database->query($sql2)->fetchAll();
					
					$sql3="select distinct * from comp_class natural join class where id_comp = '" . $id_comp . "'";
					if($class!=null){$sql3.=" and name_class like '%".$class."%'";}
					$sql3.=";";
					$data_table3 = $database->query($sql3)->fetchAll();
					
					$sql4="select distinct * from comp_ext natural join ext_db where id_comp = '" . $id_comp . "'";
					if($ext_db!=null){$sql4.=" and db_name like '%".$ext_db."%'";}
					$sql4.=";";
					$data_table4 = $database->query($sql4)->fetchAll();
					
					$compound = new Compound();
					$compound->setId_comp($data_table[$clave]['id_comp']);
					$compound->setMol_weight($data_table[$clave]['mol_weight']);
					$compound->setFormula($data_table[$clave]['formula']);
					$compound->setType($data_table[$clave]['id_type']);
					$compound->setActivity($data_table[$clave]['activity']);
					$compound->setOrigen($data_table[$clave]['origen']);
					$compound->setNames($data_table2[0]['name']);
					$compound->setClass($data_table3[0]['name_class']);
					$compound->setExt_db($data_table4['db_name']);
					array_push($compounds,$compound);
				}
				return $compounds;
			}
			else{
				return null;
			}
		}
		
		public static function getMol(){
			$database=DataSource::getDatabase();
			$data_table=null; 
			
			$sql="select mol_weight from compound order by mol_weight;";
			$data_table = $database->query($sql)->fetchAll();
			
			$compound = null;
			$compounds = array();
			if (count($data_table) > 0){
				foreach ($data_table as $clave => $valor){
					$compound=$data_table[$clave]['mol_weight'];
					array_push($compounds,$compound);
				}
				return $compounds;
			}
			else{
				return null;
			}
			
		}
		
		public static function getMolClass($id_class){
			$database=DataSource::getDatabase();
			$data_table="";
			
			$sql="WITH RECURSIVE nodes(id_class,name_class,id_father) AS (
				SELECT s1.id_class, s1.name_class, s1.id_father
				FROM class s1 WHERE id_father = '".$id_class."'
					UNION
				SELECT s2.id_class, s2.name_class, s2.id_father
				FROM class s2, nodes s1 WHERE s2.id_father = s1.id_class
			)

			select distinct mol_weight from compound natural join comp_class where id_class = '".$id_class."' or id_class in (
			SELECT id_class FROM nodes
			) order by mol_weight;";
			$data_table = $database->query($sql)->fetchAll();
			
			$compound = null;
			$compounds = array();

			if (count($data_table) > 0 and $data_table!=null){
				foreach ($data_table as $clave => $valor){
					$compound=$data_table[$clave]['mol_weight'];
					array_push($compounds,$compound);
				}
				 
				return $compounds;
			}
			else{
				return null;
			}
		}
		public static function getMolSearch($id_comp,$name,$mol_weight_min,$mol_weight_max,$formula,$activity,$origen,$class,$ext_db){
			$database=DataSource::getDatabase();
			$data_table="";
			
			
			$sql="select * from compound where 1=1";
			if($id_comp!=null){$sql.=" and id_comp::TEXT like '%".$id_comp."%'";}
			if($formula!=null){$sql.=" and formula like '%".$formula."%'";}
			if($mol_weight_min!=null){$sql.=" and mol_weight > '".$mol_weight_min."'";}
			if($mol_weight_max!=null){$sql.=" and mol_weight < '".$mol_weight_max."'";}
			if($activity!=null){$sql.=" and activity like '%".$activity."%'";}
			if($origen!=null){$sql.=" and activity like '%".$origen."%'";}
			$sql.="order by mol_weight;";
			//~ return $sql;
			$data_table = $database->query($sql)->fetchAll();
			
			
			//~ print_r( $sql); 
			$compound = null;
			$compounds = array();
			
			//~ return $data_table;
			if (count($data_table) > 0 and $data_table!=null){
				foreach ($data_table as $clave => $valor){
					$id_comp=$data_table[$clave]['id_comp'];
					
					$sql2="select * from names where id_comp = '" . $id_comp . "'";
					if($name!=null){$sql2.=" and name like '%".$name."%'";}
					$sql2.=";";
					$data_table2 = $database->query($sql2)->fetchAll();
					
					$sql3="select distinct * from comp_class natural join class where id_comp = '" . $id_comp . "'";
					if($class!=null){$sql3.=" and name_class like '%".$class."%'";}
					$sql3.=";";
					$data_table3 = $database->query($sql3)->fetchAll();
					
					$sql4="select distinct * from comp_ext natural join ext_db where id_comp = '" . $id_comp . "'";
					if($ext_db!=null){$sql4.=" and db_name like '%".$ext_db."%'";}
					$sql4.=";";
					$data_table4 = $database->query($sql4)->fetchAll();
					
					
					if ($data_table2 != null and $data_table3 != null and $data_table4 != null){
						array_push($compounds,$data_table[$clave]['mol_weight']);
					}
				}
				 
				return $compounds;
			}
			else{
				return null;
			}
		}
		
		
		
		
		
		public static function insertCompound($compuesto){
			$database=DataSource::getDatabase();
			$sql = "INSERT INTO compound VALUES (".$compuesto->getId_comp().",".$compuesto->getMol_weight().",".$compuesto->getetc().")";
			$resultado = $database->query($sql);
			return $resultado;
		}
		public static function deleteCompound($id){
			$database=DataSource::getDatabase();
			$resultado = $database->query("DELETE FROM compound where id_comp = ".$id.";");
			return $resultado;
		}
		public static function updateCompound(){
			return null;
		}
	}
?>
