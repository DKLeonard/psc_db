 Primary Metabolites & Reactive FGs:
 > Reactive FG: unhindered ester
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: unhindered ester
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: acetal or analog
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   478.538  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     8.888  (   1.0 /  12.5) 
     Solute        Total        SASA     =   659.671  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   461.350  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   169.027  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    29.293  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1318.332  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   148.384  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     6.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     3.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    11.900  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.881  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.183  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.278  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    43.033M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    12.519M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    25.917M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    17.447M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.451  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.429  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.451  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.192  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.249  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -3.590  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       247  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       109M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.955  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.020  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        78  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00686.mol:
          Name                           Similarity(%)
Triamcinolone                                 83.20
Sulisatin                                     77.96
Triamcinolone                                 76.25
Rilmazafone                                   74.22
Fludrocortisone                               73.59

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.900    H-bond Donor        -1.205 
 H-bond Acceptor     -5.795    H-bond Acceptor     -6.231 
 Volume               8.601    SASA                12.501 
 Ac x Dn^.5/SASA      1.386    Ac x Dn^.5/SASA      3.123 
 FISA                -1.170    Rotor Bonds         -0.977 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.034    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.451    Total                3.429


                log BB                    log PMDCK
 Hydrophilic SASA    -1.450    Hydrophilic SASA    -1.733 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.362                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.249    Total                2.038

