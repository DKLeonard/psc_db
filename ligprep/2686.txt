 Primary Metabolites & Reactive FGs:
 > Reactive FG: unhindered ester
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: ether dealkylation
 > Metabolism likely: alpha hydroxylation of carbonyl
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acetal or analog

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   422.474  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.600  (   1.0 /  12.5) 
     Solute        Total        SASA     =   578.995  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   423.955  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   127.143  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    27.897  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1136.662  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   114.668  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     3.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    10.650  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.910  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.886  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.178  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    37.675M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    10.506M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    21.515M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    14.925M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.231  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.921  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.924  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.300  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.642  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -3.126  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       616  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       293  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.476  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.169  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        84  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02686.mol:
          Name                           Similarity(%)
Deflazacort                                   84.29
Meprednisone                                  82.44
Cortisone                                     82.28
Fludrocortisone                               82.11
Rubitecan                                     80.90

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -5.187    H-bond Acceptor     -5.576 
 Volume               7.416    SASA                10.972 
 Ac x Dn^.5/SASA      1.154    Ac x Dn^.5/SASA      2.600 
 FISA                -0.880    Rotor Bonds         -0.488 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.033    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.231    Total                2.921


                log BB                    log PMDCK
 Hydrophilic SASA    -1.024    Hydrophilic SASA    -1.303 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.181                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.642    Total                2.468

