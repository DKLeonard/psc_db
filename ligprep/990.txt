 Primary Metabolites & Reactive FGs:
 > Reactive FG: heteroatom in 3-ring
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acetal or analog
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   584.748  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.090  (   1.0 /  12.5) 
     Solute        Total        SASA     =   987.350  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   773.418  (   0.0 / 750.0)*
     Solute        Hydrophilic  SASA     =   129.169  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    84.763  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1863.374  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   110.998  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    15.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     8.150  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.742  (  0.75 /  0.95)*
     Solute Ionization Potential (eV)    =     9.338  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.637  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    59.596M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    17.467M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    23.921M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     8.269M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     6.688  (  -2.0 /   6.5)*
   QP log S  for   aqueous solubility    =    -8.218  (  -6.5 /   0.5)*
   QP log S - conformation independent   =    -7.832  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     1.210  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.906  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         7  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -6.116  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       590  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       279M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.161  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         2  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        90  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  4
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00990.mol:
          Name                           Similarity(%)
Prednimustine                                 69.44
Pirozadil                                     67.26
Gallopamil                                    66.24
Indometacin                                   65.49
Dilazep                                       64.71

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -3.969    H-bond Acceptor     -4.267 
 Volume              12.157    SASA                18.710 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.894    Rotor Bonds         -2.442<
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.099    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                6.688    Total                8.218


                log BB                    log PMDCK
 Hydrophilic SASA    -1.565    Hydrophilic SASA    -1.324 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.904<                        0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000<   COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.906    Total                2.447

