 Primary Metabolites & Reactive FGs:
 > Reactive FG: acetal or analog
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: unhindered ester
 > Reactive FG: unhindered ester
 > Reactive FG: unhindered ester
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: tertiary alcohol E1 or SN1

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   809.946  ( 130.0 / 725.0)*
     Solute        Dipole Moment (D)     =     5.689  (   1.0 /  12.5) 
     Solute        Total        SASA     =  1062.103  ( 300.0 /1000.0)*
     Solute        Hydrophobic  SASA     =   853.856  (   0.0 / 750.0)*
     Solute        Hydrophilic  SASA     =   208.248  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =     0.000  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  2235.535  ( 500.0 /2000.0)*
     Solute        vdW Polar SA (PSA)    =   235.859  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    14.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     5.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    16.150  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.778  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.379  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.858  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    74.323M (  13.0 /  70.0)*
   QP log P  for     hexadecane/gas      =    21.200M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    41.312M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    23.834M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     4.055  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -5.780M (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -7.802M (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.744M (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -2.173M (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.842M (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        26M (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        10M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -6.076M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        37M (<25% is poor)
   Qual. Model for Human Oral Absorption =    MediumM (>80% is high)

       A * indicates a violation of the 95% range. # stars =  8
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02245.mol:
          Name                           Similarity(%)
Saquinavir                                    70.95
Erythromycin                                  70.85
Erythromycin                                  69.33
Bisbentiamine                                 65.90
Flurithromycin                                65.69

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.500    H-bond Donor        -2.008 
 H-bond Acceptor     -7.865    H-bond Acceptor     -8.456 
 Volume              14.585    SASA                20.127<
 Ac x Dn^.5/SASA      1.508    Ac x Dn^.5/SASA      3.399 
 FISA                -1.441    Rotor Bonds         -2.279 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.000    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                4.055    Total                5.780


                log BB                    log PMDCK
 Hydrophilic SASA    -2.291    Hydrophilic SASA    -2.135 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.844                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000<   COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -2.173    Total                1.028

