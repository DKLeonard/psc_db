 Primary Metabolites & Reactive FGs:
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Reactive FG: acceptor carbonyl or derivative
 > Metabolism likely: primary amide-> RCONHOH
 > Metabolism likely: primary amide-> RCONHOH
 > Metabolism likely: aromatic OH oxidation

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   550.659  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =    17.840  (   1.0 /  12.5)*
     Solute        Total        SASA     =  1004.810  ( 300.0 /1000.0)*
     Solute        Hydrophobic  SASA     =   320.067  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   423.652  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   261.091  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1789.959  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   244.770  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    17.000  (   0.0 /  15.0)*
     Solute as Donor -    Hydrogen Bonds =    11.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =    10.500  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.710  (  0.75 /  0.95)*
     Solute Ionization Potential (eV)    =     8.517  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.644  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    57.058M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    23.083M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    43.896M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    31.266M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.846  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -5.249  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.310  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.568  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -6.071  (  -3.0 /   1.2)*
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -6.103  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         0  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0M (<25 poor, >500 great)
   QP log Kp for skin permeability       =   -10.901  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         0  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars = 10
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00442.mol:
          Name                           Similarity(%)
Thiamine                                      65.95
Glucametacin                                  65.45
Mopidamol                                     62.84
Difebarbamate                                 61.91
Benzthiazide                                  60.22

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -3.299<   H-bond Donor        -4.418<
 H-bond Acceptor     -5.114    H-bond Acceptor     -5.498 
 Volume              11.678    SASA                19.041 
 Ac x Dn^.5/SASA      1.537    Ac x Dn^.5/SASA      3.464 
 FISA                -2.932    Rotor Bonds         -2.768<
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides      -0.626    Non-con amides      -0.790 
 WPSA & PISA          0.306    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.846    Total                5.249


                log BB                    log PMDCK
 Hydrophilic SASA    -5.610<   Hydrophilic SASA    -4.342<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -1.024<                        0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -6.071    Total               -0.572

