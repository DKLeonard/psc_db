 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: amine dealkylation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   327.379  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.765  (   1.0 /  12.5) 
     Solute        Total        SASA     =   557.679  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   370.453  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    58.170  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   129.056  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1001.038  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    56.505  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     4.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     5.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.868  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.427  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.260  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    32.572M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.349M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    15.859M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     9.307M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.575  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.761  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.536  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.197  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.206  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         8  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -5.013  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       693  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       368  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.816  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.087  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        93  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02735.mol:
          Name                           Similarity(%)
Naratriptan                                   87.29
Nalorphine                                    87.20
Tretoquinol                                   86.95
Lisuride                                      85.74
Perisoxal                                     85.69

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -2.435    H-bond Acceptor     -2.618 
 Volume               6.531    SASA                10.568 
 Ac x Dn^.5/SASA      0.562    Ac x Dn^.5/SASA      1.267 
 FISA                -0.403    Rotor Bonds         -0.651 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.151    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.575    Total                2.761


                log BB                    log PMDCK
 Hydrophilic SASA    -0.515    Hydrophilic SASA    -0.596 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.241                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.206    Total                2.567

