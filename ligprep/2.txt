 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   386.660  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     1.746  (   1.0 /  12.5) 
     Solute        Total        SASA     =   732.607  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   653.745  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    48.894  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    29.968  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1384.089  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    22.273  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     6.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     1.700  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.820  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.554  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.996  (  -0.9 /   1.7)*
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    45.671M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    11.840M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    16.820M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     3.834M (   4.0 /  45.0)*
   QP log P  for     octanol/water       =     6.997  (  -2.0 /   6.5)*
   QP log S  for   aqueous solubility    =    -8.064  (  -6.5 /   0.5)*
   QP log S - conformation independent   =    -6.445  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     1.842  (  -1.5 /   1.5)*
   QP log BB for     brain/blood         =    -0.283  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -4.590  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      3405  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      1860  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.739  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         1  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  6
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00002.mol:
          Name                           Similarity(%)
Chlorotrianisene                              85.72
Estradiol                                     85.54
Cinacalcet                                    84.94
Norethindrone                                 82.70
Quinestrol                                    82.03

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -0.828    H-bond Acceptor     -0.890 
 Volume               9.030    SASA                13.883 
 Ac x Dn^.5/SASA      0.103    Ac x Dn^.5/SASA      0.232 
 FISA                -0.338    Rotor Bonds         -0.977 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.035    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                6.997    Total                8.064


                log BB                    log PMDCK
 Hydrophilic SASA    -0.485    Hydrophilic SASA    -0.501 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.362                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000<   COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.283    Total                3.270

