 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   316.483  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.210  (   1.0 /  12.5) 
     Solute        Total        SASA     =   730.362  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   556.054  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    83.635  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    90.674  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1265.502  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    41.773  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    11.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     1.500  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.775  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.029  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.209  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    38.294M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    11.776M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    15.523M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     4.643M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     5.877  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -6.968  (  -6.5 /   0.5)*
   QP log S - conformation independent   =    -5.210  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     1.180  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.028  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         9  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.610  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      1595  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       819  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.685  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.001  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         1  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  3
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02849.mol:
          Name                           Similarity(%)
Suloctidil                                    83.12
Acitretin                                     83.07
Etretinate                                    80.21
Fluvoxamine                                   77.73
Retinol                                       77.13

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -0.730    H-bond Acceptor     -0.785 
 Volume               8.256    SASA                13.840 
 Ac x Dn^.5/SASA      0.129    Ac x Dn^.5/SASA      0.290 
 FISA                -0.579    Rotor Bonds         -1.791 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.106    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                5.877    Total                6.968


                log BB                    log PMDCK
 Hydrophilic SASA    -0.929    Hydrophilic SASA    -0.857 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.663                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.028    Total                2.914

