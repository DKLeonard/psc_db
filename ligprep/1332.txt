 Primary Metabolites & Reactive FGs:
 > Metabolism likely: alpha hydroxylation of cyclic ether
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   262.305  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.224  (   1.0 /  12.5) 
     Solute        Total        SASA     =   438.055  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   304.163  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   101.707  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    32.185  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   797.530  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    76.252  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     1.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     6.700  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.949  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.736  (   7.9 /  10.5)*
     Solute Electron Affinity    (eV)    =     0.265  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    25.428M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     7.119M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    12.046M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     8.063M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.569  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -0.847  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -1.607  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.901  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.249  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -2.504  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      1075  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       534  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.185  (Kp in cm/hr)
   Jm, max transdermal transport rate    =    24.368  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        85  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01332.mol:
          Name                           Similarity(%)
Flosequinan                                   86.75
Emorfazone                                    86.29
Thalidomide                                   85.58
Chlormezanone                                 83.87
Anisindione                                   83.46

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -3.263    H-bond Acceptor     -3.508 
 Volume               5.203    SASA                 8.301 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.704    Rotor Bonds         -0.163 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.038    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.569    Total                0.847


                log BB                    log PMDCK
 Hydrophilic SASA    -0.752    Hydrophilic SASA    -1.042 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.060                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.249    Total                2.728

