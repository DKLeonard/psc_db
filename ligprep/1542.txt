 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Reactive FG: acetal or analog
 > Reactive FG: acetal or analog
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   610.568  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.738  (   1.0 /  12.5) 
     Solute        Total        SASA     =   846.853  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   356.497  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   340.844  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   149.512  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1620.600  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   239.596  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    14.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     7.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =    20.050  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.788  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.941  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.769  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    51.141M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    18.364M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    38.931M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    32.006M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -1.401  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.938  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.330  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.212  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -3.941  (  -3.0 /   1.2)*
   No. of Primary Metabolites            =        11  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.677  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         5  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         1M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -5.930M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.001M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         0  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  8
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01542.mol:
          Name                           Similarity(%)
Monoxerutin                                   74.04
Troxerutin                                    58.52
Thiamine                                      54.68
Lactulose                                     54.63
Lymecycline                                   51.28

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -2.099    H-bond Donor        -2.811 
 H-bond Acceptor     -9.764    H-bond Acceptor    -10.498 
 Volume              10.573    SASA                16.048 
 Ac x Dn^.5/SASA      2.779    Ac x Dn^.5/SASA      6.262 
 FISA                -2.359    Rotor Bonds         -2.279 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.175    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               -1.401    Total                2.938


                log BB                    log PMDCK
 Hydrophilic SASA    -3.660<   Hydrophilic SASA    -3.494<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.844                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -3.941    Total                0.277

