 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Reactive FG: acetal or analog

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   420.415  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.363  (   1.0 /  12.5) 
     Solute        Total        SASA     =   715.596  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   210.938  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   286.573  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   218.086  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1261.495  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   152.525  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    13.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     6.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    11.500  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.789  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.432  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.647  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    38.066M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    14.926M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    28.070M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    22.121M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.143  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.165  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.830  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.749  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -3.289  (  -3.0 /   1.2)*
   No. of Primary Metabolites            =         7  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.986  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        18  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         6  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.785  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.005  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         1  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        38  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01908.mol:
          Name                           Similarity(%)
Mopidamol                                     80.27
Aminopterin                                   73.55
Methotrexate                                  71.89
Amprenavir                                    69.27
Methacycline                                  68.69

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.799    H-bond Donor        -2.410 
 H-bond Acceptor     -5.601    H-bond Acceptor     -6.021 
 Volume               8.230    SASA                13.561 
 Ac x Dn^.5/SASA      1.746    Ac x Dn^.5/SASA      3.935 
 FISA                -1.983    Rotor Bonds         -2.116 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.255    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.143    Total                3.165


                log BB                    log PMDCK
 Hydrophilic SASA    -3.069<   Hydrophilic SASA    -2.937 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.783                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -3.289    Total                0.833

