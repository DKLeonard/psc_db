 Primary Metabolites & Reactive FGs:
 > Reactive FG: heteroatom in 3-ring
 > Metabolism likely: alpha hydroxylation of cyclic ether
 > Reactive FG: unhindered ester
 > Reactive FG: unhindered ester
 > Metabolism likely: furan epoxidation
 > Reactive FG: activated cyclopropane
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   470.518  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.246  (   1.0 /  12.5) 
     Solute        Total        SASA     =   614.454  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   344.115  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   149.345  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   120.994  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1236.163  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   123.903  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     0.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    11.250  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.907  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.757  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.006  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    44.473M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    11.701M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    21.303M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    14.118M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.989  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.971  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.314  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.803  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.648  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -3.621  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       379  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       173M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.846  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.718  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        79  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00204.mol:
          Name                           Similarity(%)
Eplerenone                                    74.09
Loprazolam                                    69.41
Zopiclone                                     68.14
Droxicam                                      63.25
Niceritrol                                    62.04

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -5.479    H-bond Acceptor     -5.890 
 Volume               8.065    SASA                11.644 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -1.034    Rotor Bonds          0.000 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.142    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.989    Total                1.971


                log BB                    log PMDCK
 Hydrophilic SASA    -1.212    Hydrophilic SASA    -1.531 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds          0.000                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.648    Total                2.240

