 Primary Metabolites & Reactive FGs:
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   222.370  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     1.977  (   1.0 /  12.5) 
     Solute        Total        SASA     =   583.525  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   493.855  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    57.296  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    32.374  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   981.029  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    23.037  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     8.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     1.700  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.818  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.353  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.841  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    28.279M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     8.097M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    10.604  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     3.108  (   4.0 /  45.0)*
   QP log P  for     octanol/water       =     4.338  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.972  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.988  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.613  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.489  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         9  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -4.627  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      2835  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      1525  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.693  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.048  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  3
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00068.mol:
          Name                           Similarity(%)
Butoctamide                                   82.26
Febuprol                                      77.61
Bifemelane                                    77.55
Butibufen                                     76.97
Mestilbol                                     76.17

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -0.828    H-bond Acceptor     -0.890 
 Volume               6.400    SASA                11.058 
 Ac x Dn^.5/SASA      0.129    Ac x Dn^.5/SASA      0.291 
 FISA                -0.397    Rotor Bonds         -1.302 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.038    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                4.338    Total                4.972


                log BB                    log PMDCK
 Hydrophilic SASA    -0.571    Hydrophilic SASA    -0.587 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.482                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.489    Total                3.184

