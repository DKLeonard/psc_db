 Primary Metabolites & Reactive FGs:
 > Metabolism likely: alpha hydroxylation of cyclic ether
 > Reactive FG: unhindered ester
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: unhindered ester
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   478.538  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.820  (   1.0 /  12.5) 
     Solute        Total        SASA     =   663.365  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   448.493  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   185.533  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    29.339  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1320.667  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   148.029  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     6.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     3.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    12.850  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.878  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.386  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.492  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    43.127M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    12.652M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    25.951M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    18.505M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.992  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.232  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.095  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.339  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.404  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         8  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -3.653  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       172  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        73M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.259  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.015  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        73  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00719.mol:
          Name                           Similarity(%)
Triamcinolone                                 82.24
Sulisatin                                     75.26
Triamcinolone                                 73.40
Benzthiazide                                  73.29
Methacycline                                  73.00

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.900    H-bond Donor        -1.205 
 H-bond Acceptor     -6.258    H-bond Acceptor     -6.728 
 Volume               8.616    SASA                12.571 
 Ac x Dn^.5/SASA      1.488    Ac x Dn^.5/SASA      3.354 
 FISA                -1.284    Rotor Bonds         -0.977 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.034    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.992    Total                3.232


                log BB                    log PMDCK
 Hydrophilic SASA    -1.606    Hydrophilic SASA    -1.902 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.362                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.404    Total                1.869

