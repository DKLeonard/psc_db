 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: alpha hydroxylation of carbonyl
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acceptor carbonyl or derivative

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   516.673  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.192  (   1.0 /  12.5) 
     Solute        Total        SASA     =   728.664  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   495.117  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   216.902  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    16.644  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1504.942  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   146.713  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     8.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     4.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    10.900  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.872  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.885  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.334  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    49.096M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    14.550M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    28.692M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    17.891M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.451  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.400  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.377  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.290  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.822  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         7  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -3.689  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        86  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        35M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.690  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         1  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        63  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00732.mol:
          Name                           Similarity(%)
Colforsin                                     81.06
Triamcinolone                                 78.05
"Estriol                                      77.07
Prednisolone                                  75.96
Amprenavir                                    74.41

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.200    H-bond Donor        -1.606 
 H-bond Acceptor     -5.308    H-bond Acceptor     -5.707 
 Volume               9.818    SASA                13.808 
 Ac x Dn^.5/SASA      1.327    Ac x Dn^.5/SASA      2.991 
 FISA                -1.501    Rotor Bonds         -1.302 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.019    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.451    Total                4.400


                log BB                    log PMDCK
 Hydrophilic SASA    -1.904    Hydrophilic SASA    -2.223 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.482                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.822    Total                1.548

