 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: ether dealkylation
 > Metabolism likely: amine dealkylation

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   453.575  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.756  (   1.0 /  12.5) 
     Solute        Total        SASA     =   649.563  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   546.063  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   103.500  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =     0.000  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1283.047  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    90.361  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     9.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     4.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    12.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.879  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.805  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -2.079  (  -0.9 /   1.7)*
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    39.415M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    11.774M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    25.325M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    17.679M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.018  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.645  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.538  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.386  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.473  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -4.214  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       257  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       126M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.626  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.243  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        76  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00636.mol:
          Name                           Similarity(%)
Methacycline                                  79.18
Rilmazafone                                   77.86
Doxycycline                                   77.38
Idarubicin                                    75.05
Reproterol                                    74.94

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.200    H-bond Donor        -1.606 
 H-bond Acceptor     -5.844    H-bond Acceptor     -6.283 
 Volume               8.371    SASA                12.309 
 Ac x Dn^.5/SASA      1.639    Ac x Dn^.5/SASA      3.693 
 FISA                -0.716    Rotor Bonds         -1.465 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.000    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.018    Total                1.645


                log BB                    log PMDCK
 Hydrophilic SASA    -0.893    Hydrophilic SASA    -1.061 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.542                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.473    Total                2.102

