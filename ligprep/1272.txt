 Primary Metabolites & Reactive FGs:
 > Reactive FG: heteroatom in 3-ring
 > Reactive FG: heteroatom in 3-ring
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: benzylic-like H -> alcohol
 > Reactive FG: acceptor carbonyl or derivative
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   392.405  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.844  (   1.0 /  12.5) 
     Solute        Total        SASA     =   549.735  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   379.918  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   128.650  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    41.167  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1091.522  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   130.221  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     5.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    10.450  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.933  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.325  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.500  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    34.711M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    10.099M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    18.695M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    12.852M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.028  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.848  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.774  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.581  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.724  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -2.819  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       596  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       283  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.265  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     3.025  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        83  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01272.mol:
          Name                           Similarity(%)
Meprednisone                                  75.84
Cortisone                                     74.48
Triamcinolone                                 74.48
Meprednisone                                  74.25
Prednisone                                    73.29

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -5.089    H-bond Acceptor     -5.472 
 Volume               7.121    SASA                10.417 
 Ac x Dn^.5/SASA      0.843    Ac x Dn^.5/SASA      1.900 
 FISA                -0.890    Rotor Bonds         -0.814 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.048    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.028    Total                1.848


                log BB                    log PMDCK
 Hydrophilic SASA    -0.986    Hydrophilic SASA    -1.319 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.301                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.724    Total                2.452

