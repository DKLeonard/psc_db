 Primary Metabolites & Reactive FGs:
 > Metabolism likely: alpha hydroxylation of carbonyl
 > Metabolism likely: alpha hydroxylation of carbonyl
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   562.834  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.705  (   1.0 /  12.5) 
     Solute        Total        SASA     =  1135.378  ( 300.0 /1000.0)*
     Solute        Hydrophobic  SASA     =   815.979  (   0.0 / 750.0)*
     Solute        Hydrophilic  SASA     =   105.952  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   213.446  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  2085.652  ( 500.0 /2000.0)*
     Solute        vdW Polar SA (PSA)    =    56.702  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    19.000  (   0.0 /  15.0)*
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     4.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.695  (  0.75 /  0.95)*
     Solute Ionization Potential (eV)    =     8.102  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     1.380  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    67.152M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    21.040M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    23.848M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     4.373M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    10.471  (  -2.0 /   6.5)*
   QP log S  for   aqueous solubility    =   -12.545  (  -6.5 /   0.5)*
   QP log S - conformation independent   =    -9.630  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     2.796  (  -1.5 /   1.5)*
   QP log BB for     brain/blood         =    -2.043  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         8  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -7.480  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       979  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       483M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -0.896  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         2  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  9
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00580.mol:
          Name                           Similarity(%)
Tocofibrate                                   75.19
Tocopherol                                    69.29
Tocopherol                                    66.89
Indometacin                                   65.22
Probucol                                      61.87

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -1.948    H-bond Acceptor     -2.094 
 Volume              13.607    SASA                21.515<
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.733    Rotor Bonds         -3.093<
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.250    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               10.471    Total               12.545


                log BB                    log PMDCK
 Hydrophilic SASA    -1.461    Hydrophilic SASA    -1.086 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -1.145<                        0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000<   COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -2.043    Total                2.685

