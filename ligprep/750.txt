 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: unhindered ester
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   520.662  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.668  (   1.0 /  12.5) 
     Solute        Total        SASA     =   763.250  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   499.887  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   252.895  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    10.468  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1515.625  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   156.838  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     9.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     5.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    11.600  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.836  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.443  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.500  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    48.822M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    15.152M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    30.263M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    20.111M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.804  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.531  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.159  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.083  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -2.391  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         8  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.235  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        39  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        15M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -5.279  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         1  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        53  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00750.mol:
          Name                           Similarity(%)
Prednisolone                                  77.04
Idarubicin                                    76.93
Sulfamazone                                   73.31
Dipyridamole                                  72.68
Tritoqualine                                  70.24

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.500    H-bond Donor        -2.008 
 H-bond Acceptor     -5.649    H-bond Acceptor     -6.074 
 Volume               9.888    SASA                14.464 
 Ac x Dn^.5/SASA      1.508    Ac x Dn^.5/SASA      3.397 
 FISA                -1.750    Rotor Bonds         -1.465 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.012    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.804    Total                4.531


                log BB                    log PMDCK
 Hydrophilic SASA    -2.412<   Hydrophilic SASA    -2.592 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.542                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -2.391    Total                1.179

