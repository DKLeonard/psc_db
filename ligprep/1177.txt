 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: amine dealkylation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   297.353  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.782  (   1.0 /  12.5) 
     Solute        Total        SASA     =   533.909  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   284.944  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    92.793  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   156.172  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   935.573  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    54.091  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     3.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     4.250  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.866  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.499  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.333  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    30.853M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.154M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    15.087M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     9.151M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.242  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.724  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.212  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.188  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.043  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         7  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -5.164  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       325  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       162  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.454  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.020  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        85  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01177.mol:
          Name                           Similarity(%)
Dienogest                                     87.32
Nalorphine                                    87.01
Epimestrol                                    86.54
Perisoxal                                     85.72
Lisuride                                      85.00

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -2.070    H-bond Acceptor     -2.225 
 Volume               6.104    SASA                10.118 
 Ac x Dn^.5/SASA      0.499    Ac x Dn^.5/SASA      1.125 
 FISA                -0.642    Rotor Bonds         -0.488 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.183    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.242    Total                2.724


                log BB                    log PMDCK
 Hydrophilic SASA    -0.824    Hydrophilic SASA    -0.951 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.181                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.043    Total                2.212

