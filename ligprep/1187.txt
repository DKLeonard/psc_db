 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   264.321  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     7.793  (   1.0 /  12.5) 
     Solute        Total        SASA     =   469.524  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   276.859  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   141.515  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    51.150  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   843.544  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    79.744  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     3.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     6.400  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.920  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.774  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.047  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    26.167M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     8.187M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    15.673M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    10.731M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.017  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.399  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.057  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.306  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.733  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         7  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -3.078  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       450  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       209  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.659  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.231  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        80  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01187.mol:
          Name                           Similarity(%)
Heptabarbital                                 84.34
Pyricarbate                                   82.14
Didanosine                                    81.79
Azathioprine                                  81.18
Fenspiride                                    80.55

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -3.117    H-bond Acceptor     -3.351 
 Volume               5.503    SASA                 8.897 
 Ac x Dn^.5/SASA      0.855    Ac x Dn^.5/SASA      1.927 
 FISA                -0.979    Rotor Bonds         -0.488 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.060    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.017    Total                2.399


                log BB                    log PMDCK
 Hydrophilic SASA    -1.116    Hydrophilic SASA    -1.451 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.181                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.733    Total                2.320

