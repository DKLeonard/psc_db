 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   590.496  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     8.554  (   1.0 /  12.5) 
     Solute        Total        SASA     =   785.930  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =    17.211  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   469.532  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   299.188  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1522.989  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   253.410  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=     9.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     7.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =    11.200  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.815  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.794  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.431  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    51.874M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    19.314M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    35.932M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    26.138M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.451  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.739  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -7.866  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.003  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -4.697  (  -3.0 /   1.2)*
   No. of Primary Metabolites            =        12  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.960  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         0  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -8.255  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         0  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  7
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01550.mol:
          Name                           Similarity(%)
Methotrexate                                  61.47
Aminopterin                                   61.11
Penimepicycline                               60.14
Veralipride                                   60.13
Capecitabine                                  60.08

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -2.099    H-bond Donor        -2.811 
 H-bond Acceptor     -5.454    H-bond Acceptor     -5.864 
 Volume               9.936    SASA                14.893 
 Ac x Dn^.5/SASA      1.673    Ac x Dn^.5/SASA      3.769 
 FISA                -3.250<   Rotor Bonds         -1.465 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.350    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.451    Total                4.739


                log BB                    log PMDCK
 Hydrophilic SASA    -4.718<   Hydrophilic SASA    -4.813<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.542                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -4.697    Total               -1.042

