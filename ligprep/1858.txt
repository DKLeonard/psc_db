 Primary Metabolites & Reactive FGs:
 > Reactive FG: acetal or analog
 > Reactive FG: unhindered ester
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Reactive FG: unhindered ester
 > Reactive FG: unhindered ester
 > Reactive FG: unhindered ester
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Reactive FG: unhindered ester
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Reactive FG: acetal or analog
 > Metabolism likely: para hydroxylation of aryl
 > Reactive FG: acetal or analog
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =  1120.762  ( 130.0 / 725.0)*
     Solute        Dipole Moment (D)     =     8.521  (   1.0 /  12.5) 
     Solute        Total        SASA     =  1164.883  ( 300.0 /1000.0)*
     Solute        Hydrophobic  SASA     =   101.389  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   854.018  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   209.477  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  2454.673  ( 500.0 /2000.0)*
     Solute        vdW Polar SA (PSA)    =   562.929  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    21.000  (   0.0 /  15.0)*
     Solute as Donor -    Hydrogen Bonds =    17.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =    28.950  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.755  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.770  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     1.773  (  -0.9 /   1.7)*
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    80.598M (  13.0 /  70.0)*
   QP log P  for     hexadecane/gas      =    32.104M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    71.348M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    58.538M (   4.0 /  45.0)*
   QP log P  for     octanol/water       =    -4.492M (  -2.0 /   6.5)*
   QP log S  for   aqueous solubility    =    -3.130M (  -6.5 /   0.5) 
   QP log S - conformation independent   =   -10.550M (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.750M (  -1.5 /   1.5)*
   QP log BB for     brain/blood         =   -10.678M (  -3.0 /   1.2)*
   No. of Primary Metabolites            =        15  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.680M (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         0M (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0M (<25 poor, >500 great)
   QP log Kp for skin permeability       =   -14.505M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         0M (<25% is poor)
   Qual. Model for Human Oral Absorption =       lowM (>80% is high)

       A * indicates a violation of the 95% range. # stars = 17
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01858.mol:
          Name                           Similarity(%)
Glycopin                                      44.29
Glucosulfone                                  43.02
Caspofungin                                   37.69
Troxerutin                                    35.47
Monoxerutin                                   32.05

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -5.098<   H-bond Donor        -6.827<
 H-bond Acceptor    -14.099<   H-bond Acceptor    -15.158<
 Volume              16.014    SASA                22.075<
 Ac x Dn^.5/SASA      4.546<   Ac x Dn^.5/SASA     10.243<
 FISA                -5.395<   Rotor Bonds         -3.419<
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.245    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               -4.492    Total                3.130


                log BB                    log PMDCK
 Hydrophilic SASA    -9.976<   Hydrophilic SASA    -8.754<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -1.266<                        0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids     -0.540 
 Constant             0.564    Constant             3.771
 Total              -10.678    Total               -5.523

