 Primary Metabolites & Reactive FGs:
 > Reactive FG: unhindered ester
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   234.338  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.525  (   1.0 /  12.5) 
     Solute        Total        SASA     =   432.780  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   351.889  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    46.968  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    33.923  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   783.462  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    37.523  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     0.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.950  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.983  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.393  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    25.524M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     6.616M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =     9.908  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     4.266  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.660  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.848  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.854  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.090  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.216  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -2.508  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      3552  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      1947  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.266  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     1.806  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01229.mol:
          Name                           Similarity(%)
Trioxsalen                                    95.37
Isonixin                                      86.81
Fadrozole                                     86.48
Phenindione                                   85.10
Methsuximide                                  84.69

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -1.461    H-bond Acceptor     -1.571 
 Volume               5.111    SASA                 8.201 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.325    Rotor Bonds          0.000 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.040    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.660    Total                2.848


                log BB                    log PMDCK
 Hydrophilic SASA    -0.347    Hydrophilic SASA    -0.481 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds          0.000                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.216    Total                3.289

