 Primary Metabolites & Reactive FGs:
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   230.306  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.223  (   1.0 /  12.5) 
     Solute        Total        SASA     =   461.720  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   307.728  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    64.426  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    89.565  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   802.068  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    39.014  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     1.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.904  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.805  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.195  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    26.158M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     6.954M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    10.298  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     4.612  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.726  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.233  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.653  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.101  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.022  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -3.509  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      2426  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      1289  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.295  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.682  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01218.mol:
          Name                           Similarity(%)
Epirizole                                     89.24
Isonixin                                      86.04
Fadrozole                                     85.91
Trioxsalen                                    84.81
Enoximone                                     84.28

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -1.461    H-bond Acceptor     -1.571 
 Volume               5.233    SASA                 8.750 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.446    Rotor Bonds         -0.163 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.105    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.726    Total                3.233


                log BB                    log PMDCK
 Hydrophilic SASA    -0.525    Hydrophilic SASA    -0.660 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.060                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.022    Total                3.110

