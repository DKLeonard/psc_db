 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   166.219  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.110  (   1.0 /  12.5) 
     Solute        Total        SASA     =   384.093  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   196.942  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    78.665  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   108.486  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   629.808  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    41.107  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     3.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.400  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.925  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.538  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.724  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    18.161  (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     6.090  (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    10.218  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     7.533  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.286  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.675  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -1.489  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.417  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.230  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         4  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -3.261  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      1777  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       921  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.299  (Kp in cm/hr)
   Jm, max transdermal transport rate    =    17.631  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        93  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)


       5 of   1712 molecules most similar to BC01634.mol:
          Name                           Similarity(%)
Sobrerol                                      95.89
Prothionamide                                 90.08
Propylthiouracil                              89.17
Talipexole                                    88.74
Ethionamide                                   88.62

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -1.656    H-bond Acceptor     -1.780 
 Volume               4.109    SASA                 7.279 
 Ac x Dn^.5/SASA      0.555    Ac x Dn^.5/SASA      1.251 
 FISA                -0.544    Rotor Bonds         -0.488 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.127    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.286    Total                1.675


                log BB                    log PMDCK
 Hydrophilic SASA    -0.613    Hydrophilic SASA    -0.806 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.181                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.230    Total                2.964

