 Primary Metabolites & Reactive FGs:
 > Metabolism likely: amine dealkylation
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   188.272  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.014  (   1.0 /  12.5) 
     Solute        Total        SASA     =   449.633  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   199.121  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    35.197  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   215.315  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   736.658  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    20.076  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     3.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     2.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.877  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.198  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.085  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    23.457  (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     7.015  (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =     9.684  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     5.278  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.506  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.026  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -1.689  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.102  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.476  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         2  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      ++ 
   HERG K+ Channel Blockage: log IC50    =    -5.270  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      1145  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       633  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.184  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     1.161  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        96  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)


       5 of   1712 molecules most similar to BC00439.mol:
          Name                           Similarity(%)
Mephentermine                                 91.06
Homarylamine                                  90.91
Methoxyphenamine                              89.95
Etilamfetamine                                89.65
Chlorphentermine                              89.10

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -0.974    H-bond Acceptor     -1.047 
 Volume               4.806    SASA                 8.521 
 Ac x Dn^.5/SASA      0.197    Ac x Dn^.5/SASA      0.445 
 FISA                -0.244    Rotor Bonds         -0.488 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.252    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.506    Total                2.026


                log BB                    log PMDCK
 Hydrophilic SASA    -0.305    Hydrophilic SASA    -0.361 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.181                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.476    Total                2.802

