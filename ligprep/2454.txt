 Primary Metabolites & Reactive FGs:
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   204.355  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     0.477  (   1.0 /  12.5)*
     Solute        Total        SASA     =   469.538  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   405.562  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =     0.000  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =    63.976  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   818.324  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =     0.000  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=     1.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     0.000  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.901  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.431  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -1.083  (  -0.9 /   1.7)*
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    26.564M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     6.456M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =     7.797  (   8.0 /  35.0)*
   QP log P  for     water/gas           =    -0.050  (   4.0 /  45.0)*
   QP log P  for     octanol/water       =     5.394  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -6.334  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -6.334  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.951  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     1.004  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         4  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      ++ 
   HERG K+ Channel Blockage: log IC50    =    -3.437  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      9906  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      5899  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.198  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.006  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         1  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  7
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02454.mol:
          Name                           Similarity(%)
Xibornol                                      82.67
Chlornaphazine                                81.85
Pyrantel                                      78.02
Epirizole                                     77.24
Mitotane                                      77.19

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor      0.000    H-bond Acceptor      0.000 
 Volume               8.018    SASA                 8.517 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                 0.000    Rotor Bonds          0.000 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.000    WPSA                 0.000 
 Constant            -2.623    Constant            -2.183 
 Total                5.394    Total                6.334


                log BB                    log PMDCK
 Hydrophilic SASA     0.000    Hydrophilic SASA     0.000 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds          0.000                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.812    COOH/SO3H acids      0.000 
 Constant             0.192    Constant             3.771
 Total                1.004    Total                3.771

