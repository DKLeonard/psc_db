 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: amine dealkylation
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   165.235  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.023  (   1.0 /  12.5) 
     Solute        Total        SASA     =   420.078  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   203.650  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    61.587  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   154.841  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   674.562  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    28.699  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     4.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     2.750  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.885  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.980  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.229  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    19.752  (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     6.188  (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =     8.718  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     5.493  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.575  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -0.723  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -0.820  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.230  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.197  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -4.850  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       643  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       340  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.788  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     2.044  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        86  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)


       5 of   1712 molecules most similar to BC00345.mol:
          Name                           Similarity(%)
Methoxyphenamine                              95.03
Pargyline                                     94.72
Betahistine                                   93.81
Etilamfetamine                                92.59
Fenproporex                                   91.49

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -1.339    H-bond Acceptor     -1.440 
 Volume               4.401    SASA                 7.960 
 Ac x Dn^.5/SASA      0.290    Ac x Dn^.5/SASA      0.654 
 FISA                -0.426    Rotor Bonds         -0.651 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.181    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.575    Total                1.120


                log BB                    log PMDCK
 Hydrophilic SASA    -0.524    Hydrophilic SASA    -0.631 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.241                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.197    Total                2.532

