 Primary Metabolites & Reactive FGs:
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Reactive FG: acceptor carbonyl or derivative
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   125.213  ( 130.0 / 725.0)*
     Solute        Dipole Moment (D)     =     2.095  (   1.0 /  12.5) 
     Solute        Total        SASA     =   409.836  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   372.667  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    37.170  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =     0.000  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   629.986  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    19.782  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     2.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     1.000  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.867  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.780  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -1.009  (  -0.9 /   1.7)*
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    17.774  (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     4.069  (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =     5.288  (   8.0 /  35.0)*
   QP log P  for     water/gas           =     1.502  (   4.0 /  45.0)*
   QP log P  for     octanol/water       =     2.621  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.058  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -1.369  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.094M (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.113  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         2  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -3.681M (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      4399  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      2453M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.013  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.893  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  5
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01770.mol:
          Name                           Similarity(%)
Clomethiazole                                 88.18
Phenylpropanol                                85.25
Pempidine                                     85.05
Isaxonine                                     84.64
Mephentermine                                 82.50

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -0.487    H-bond Acceptor     -0.524 
 Volume               4.110    SASA                 7.766 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.257    Rotor Bonds         -0.326 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.000    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.661    Total                3.134


                log BB                    log PMDCK
 Hydrophilic SASA    -0.330    Hydrophilic SASA    -0.381 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.121                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.113    Total                3.390

