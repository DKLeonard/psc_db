 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: unhindered ester
 > Metabolism likely: alpha hydroxylation of carbonyl
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   560.726  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     7.548  (   1.0 /  12.5) 
     Solute        Total        SASA     =   788.681  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   563.045  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   212.389  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    13.246  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1640.659  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   158.391  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     9.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     3.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    12.150  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.853  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.852  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.403  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    53.853M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    15.418M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    29.639M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    17.528M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.911  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.798  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.825  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.352  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.925  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         7  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -3.974  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        95  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        39M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.523  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         1  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        67  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02855.mol:
          Name                           Similarity(%)
Prednisolone                                  76.56
"Estriol                                      76.39
Colforsin                                     75.60
Meproscillarin                                74.13
Amprenavir                                    72.43

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.900    H-bond Donor        -1.205 
 H-bond Acceptor     -5.917    H-bond Acceptor     -6.362 
 Volume              10.704    SASA                14.946 
 Ac x Dn^.5/SASA      1.184    Ac x Dn^.5/SASA      2.667 
 FISA                -1.470    Rotor Bonds         -1.465 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.016    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.911    Total                4.798


                log BB                    log PMDCK
 Hydrophilic SASA    -1.946    Hydrophilic SASA    -2.177 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.542                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.925    Total                1.594

