 Primary Metabolites & Reactive FGs:
 > Metabolism likely: enol oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   460.523  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =    10.408  (   1.0 /  12.5) 
     Solute        Total        SASA     =   769.744  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   584.240  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   164.679  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    20.825  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1438.437  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   144.371  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    13.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     7.750  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.801  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.373  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.977  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    43.264M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    13.375M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    21.958M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    10.529M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     3.822  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -5.250  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -6.085  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.417  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.933  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         9  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.858  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       271  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       121M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.233  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.002  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        93  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02144.mol:
          Name                           Similarity(%)
Cerivastatin                                  87.61
Enprostil                                     81.97
Rosuvastatin                                  81.95
Sofalcone                                     79.89
Verapamil                                     78.12

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -3.774    H-bond Acceptor     -4.058 
 Volume               9.384    SASA                14.587 
 Ac x Dn^.5/SASA      0.632    Ac x Dn^.5/SASA      1.423 
 FISA                -1.140    Rotor Bonds         -2.116 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.024    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                3.822    Total                5.250


                log BB                    log PMDCK
 Hydrophilic SASA    -1.713    Hydrophilic SASA    -1.688 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.783                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.933    Total                2.083

