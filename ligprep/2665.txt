 Primary Metabolites & Reactive FGs:
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   307.432  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.045  (   1.0 /  12.5) 
     Solute        Total        SASA     =   687.067  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   483.507  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    85.972  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   117.588  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1174.976  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    65.621  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    11.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     4.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.784  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.123  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.140  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    34.928M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    11.065M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    15.955M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     9.323M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     3.695  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.422  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.505  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.158  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.032  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         4  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.217  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       895  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       775  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.634  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.271  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02665.mol:
          Name                           Similarity(%)
Fluvoxamine                                   91.14
Butofilolol                                   89.23
Ambucetamide                                  85.12
Butamirate                                    84.93
Capobenic                                     84.58

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -1.948    H-bond Acceptor     -2.094 
 Volume               7.666    SASA                13.020 
 Ac x Dn^.5/SASA      0.365    Ac x Dn^.5/SASA      0.823 
 FISA                -0.595    Rotor Bonds         -1.791 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides      -0.626    Non-con amides      -0.950 
 WPSA & PISA          0.138    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                3.695    Total                4.422


                log BB                    log PMDCK
 Hydrophilic SASA    -0.933    Hydrophilic SASA    -0.881 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.663                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.032    Total                2.890

