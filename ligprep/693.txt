 Primary Metabolites & Reactive FGs:
 > Metabolism likely: alpha hydroxylation of cyclic ether
 > Reactive FG: unhindered ester
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: alpha hydroxylation of carbonyl
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acceptor carbonyl or derivative
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   520.532  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.427  (   1.0 /  12.5) 
     Solute        Total        SASA     =   697.148  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   441.211  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   242.765  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    13.172  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1386.241  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   180.696  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     6.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    12.900  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.862  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.352  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.412  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    45.597M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    13.269M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    25.788M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    17.450M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.953  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.510  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.888  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.298  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.974  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =        10  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -3.821  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        49  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        19M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -5.371  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.001  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         2  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        37  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00693.mol:
          Name                           Similarity(%)
Triamcinolone                                 80.14
Prednisolone                                  72.43
Colforsin                                     72.30
Clomocycline                                  71.08
Ampiroxicam                                   70.84

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -6.282    H-bond Acceptor     -6.754 
 Volume               9.044    SASA                13.211 
 Ac x Dn^.5/SASA      1.161    Ac x Dn^.5/SASA      2.616 
 FISA                -1.680    Rotor Bonds         -0.977 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.015    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.953    Total                3.510


                log BB                    log PMDCK
 Hydrophilic SASA    -2.176    Hydrophilic SASA    -2.488 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.362                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.974    Total                1.282

