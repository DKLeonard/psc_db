 Primary Metabolites & Reactive FGs:
 > Metabolism likely: primary amine -> C=O
 > Metabolism likely: amine dealkylation
 > Metabolism likely: amine dealkylation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   384.647  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     1.724  (   1.0 /  12.5) 
     Solute        Total        SASA     =   653.656  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   574.364  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    46.556  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    32.735  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1282.061  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    27.668  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     3.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.873  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.656  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.267  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    43.540M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    11.134M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    18.496M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     7.054M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     4.549  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.951  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.604  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     1.322  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.773  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         7  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      ++ 
   HERG K+ Channel Blockage: log IC50    =    -5.193  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       222  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       119  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -6.099  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        96  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02230.mol:
          Name                           Similarity(%)
Mefloquine                                    81.62
Dimethisterone                                78.88
Norgestrel                                    78.08
Oxetorone                                     77.86
Trifluoperazine                               77.61

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -1.461    H-bond Acceptor     -1.571 
 Volume               8.364    SASA                12.387 
 Ac x Dn^.5/SASA      0.288    Ac x Dn^.5/SASA      0.649 
 FISA                -0.322    Rotor Bonds         -0.488 
 Non-con amines      -1.054    N Protonation       -2.439 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.038    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                4.549    Total                3.951


                log BB                    log PMDCK
 Hydrophilic SASA    -0.407    Hydrophilic SASA    -0.477 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.181                         0.000 
 N Protonation        0.797    Non-con amines      -1.216 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.773    Total                2.078

