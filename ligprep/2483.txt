 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Reactive FG: acetal or analog

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   448.382  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.292  (   1.0 /  12.5) 
     Solute        Total        SASA     =   636.832  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =    92.119  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   336.461  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   208.252  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1195.485  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   196.417  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    10.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     6.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    13.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.855  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.024  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.674  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    37.257M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    14.540M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    29.186M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    24.343M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -0.903  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.439  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.066  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.753  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -3.105  (  -3.0 /   1.2)*
   No. of Primary Metabolites            =         7  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.865  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         6  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         2M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -6.027  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.002  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         2  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        10  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  2
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02483.mol:
          Name                           Similarity(%)
Idarubicin                                    71.09
Aminopterin                                   68.39
Valganciclovir                                66.53
Methotrexate                                  66.40
Methacycline                                  65.97

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.799    H-bond Donor        -2.410 
 H-bond Acceptor     -6.331    H-bond Acceptor     -6.807 
 Volume               7.799    SASA                12.068 
 Ac x Dn^.5/SASA      2.218    Ac x Dn^.5/SASA      4.998 
 FISA                -2.329    Rotor Bonds         -1.628 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.244    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               -0.903    Total                2.439


                log BB                    log PMDCK
 Hydrophilic SASA    -3.066<   Hydrophilic SASA    -3.449<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.603                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -3.105    Total                0.322

