 Primary Metabolites & Reactive FGs:
 > Reactive FG: phosphonate or relative
 > Reactive FG: anhydride or analog
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   450.448  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.945  (   1.0 /  12.5) 
     Solute        Total        SASA     =   706.108  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   468.573  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   196.974  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    29.598  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =    10.962  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1326.267  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   113.242  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    11.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     7.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.827  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.702  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.066  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    40.143M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    12.197M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    17.142M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     7.432M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     4.799  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.190  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.408  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.355  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.994  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =     0.946  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         2  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         1M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.989  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.003  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        61  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02427.mol:
          Name                           Similarity(%)
Cloricromen                                   75.98
Mebeverine                                    74.34
Acetorphan                                    73.58
Trimebutine                                   72.83
Promethestrol                                 72.45

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -3.409    H-bond Acceptor     -3.665 
 Volume               8.653    SASA                13.381 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                 0.184    Rotor Bonds         -1.791 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.076    WPSA                 0.048 
 Constant            -0.705    Constant            -3.783 
 Total                4.799    Total                4.190


                log BB                    log PMDCK
 Hydrophilic SASA    -1.921    Hydrophilic SASA    -2.019 
 WPSA                 0.027    WPSA                 0.060 
 Rotor Bonds         -0.663                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids     -1.621<
 Constant             0.564    Constant             3.771
 Total               -1.994    Total                0.191

