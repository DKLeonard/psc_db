 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Reactive FG: unhindered ester
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acetal or analog

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   576.726  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.611  (   1.0 /  12.5) 
     Solute        Total        SASA     =   834.980  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   655.627  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   171.287  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =     8.066  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1668.760  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   133.728  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     6.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    12.550  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.815  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.479  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.362  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    56.854M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    15.276M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    28.953M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    16.916M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     3.237  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -5.814  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -6.031  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.403  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.518  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         4  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.566  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       235  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       103M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.072  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         1  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        75  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02818.mol:
          Name                           Similarity(%)
Meproscillarin                                84.39
Dihydroergocryptine                           77.70
Dihydroergocornine                            76.07
Bromocriptine                                 74.97
Olmesartan                                    74.72

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -6.112    H-bond Acceptor     -6.571 
 Volume              10.887    SASA                15.823 
 Ac x Dn^.5/SASA      0.943    Ac x Dn^.5/SASA      2.125 
 FISA                -1.185    Rotor Bonds         -0.977 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.009    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                3.237    Total                5.814


                log BB                    log PMDCK
 Hydrophilic SASA    -1.720    Hydrophilic SASA    -1.756 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.362                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000<   COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.518    Total                2.015

