 Primary Metabolites & Reactive FGs:
 > Reactive FG: heteroatom in 3-ring
 > Reactive FG: heteroatom in 3-ring
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Reactive FG: unhindered ester
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: alpha hydroxylation of carbonyl
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   310.303  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =    10.359  (   1.0 /  12.5) 
     Solute        Total        SASA     =   410.242  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   274.176  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   107.478  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    28.588  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   809.801  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   106.813  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     4.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     9.200  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     1.024  (  0.75 /  0.95)*
     Solute Ionization Potential (eV)    =    10.831  (   7.9 /  10.5)*
     Solute Electron Affinity    (eV)    =    -0.074  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    23.959M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     8.382M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    17.224M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    12.741M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.195  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -0.890  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -1.844  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.656  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.361  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -1.277  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       947  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       466  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.016  (Kp in cm/hr)
   Jm, max transdermal transport rate    =    38.568  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        81  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  2
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01326.mol:
          Name                           Similarity(%)
Carboquone                                    81.28
Topiramate                                    77.44
Sulfadoxine                                   73.94
Buthiazide                                    73.34
Prednisone                                    72.76

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -4.480    H-bond Acceptor     -4.817 
 Volume               5.283    SASA                 7.774 
 Ac x Dn^.5/SASA      1.407    Ac x Dn^.5/SASA      3.170 
 FISA                -0.744    Rotor Bonds         -0.651 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.033    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.195    Total                0.890


                log BB                    log PMDCK
 Hydrophilic SASA    -0.683    Hydrophilic SASA    -1.102 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.241                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.361    Total                2.669

