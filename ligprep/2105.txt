 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   252.312  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     0.739  (   1.0 /  12.5)*
     Solute        Total        SASA     =   525.420  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =    94.484  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   109.380  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   321.556  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   896.364  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    45.094  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     6.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     1.500  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.856  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.802  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.060  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    28.937M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    10.005M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    13.034M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     6.998M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     4.164  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.013  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.249  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.370  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.794  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -5.401  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       909  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       446  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.827  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.366  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  2
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02105.mol:
          Name                           Similarity(%)
Dienestrol                                    93.73
Diethylstilbestrol                            93.63
Hexestrol                                     93.13
Clobenzorex                                   89.80
Amfetaminil                                   89.09

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -0.730    H-bond Acceptor     -0.785 
 Volume               5.848    SASA                 9.957 
 Ac x Dn^.5/SASA      0.179    Ac x Dn^.5/SASA      0.404 
 FISA                -0.757    Rotor Bonds         -0.977 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.377    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                3.611    Total                4.012


                log BB                    log PMDCK
 Hydrophilic SASA    -0.996    Hydrophilic SASA    -1.121 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.362                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.794    Total                2.650

