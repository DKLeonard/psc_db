 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   346.422  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.530  (   1.0 /  12.5) 
     Solute        Total        SASA     =   519.693  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   340.101  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   150.723  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    28.868  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   996.325  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    96.588  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     2.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     5.750  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.928  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.038  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.600  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    32.710M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.545M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    17.033M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    10.343M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.596  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.490  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.034  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.035  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.723  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -1.022  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        93  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        48  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.004  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.011  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        77  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02470.mol:
          Name                           Similarity(%)
Fluoxymesterone                               88.37
Medroxyprogesterone                           88.10
Formebolone                                   88.07
Estropipate                                   87.11
Flugestone                                    86.80

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -2.800    H-bond Acceptor     -3.011 
 Volume               6.500    SASA                 9.848 
 Ac x Dn^.5/SASA      0.694    Ac x Dn^.5/SASA      1.564 
 FISA                -0.527    Rotor Bonds         -0.326 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.034    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.596    Total                3.490


                log BB                    log PMDCK
 Hydrophilic SASA    -1.166    Hydrophilic SASA    -1.545 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.121                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids     -0.540 
 Constant             0.564    Constant             3.771
 Total               -0.723    Total                1.686

