 Primary Metabolites & Reactive FGs:
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: para hydroxylation of aryl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   244.336  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.274  (   1.0 /  12.5) 
     Solute        Total        SASA     =   470.166  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   305.616  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    49.621  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   114.928  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   818.202  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    34.646  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     0.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     5.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.900  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.668  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.075  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    27.688M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     7.155M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    11.904  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     6.895  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.462  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.289  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -1.266  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.344  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.553  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         2  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      ++ 
   HERG K+ Channel Blockage: log IC50    =    -4.427  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       836  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       451  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.092  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     1.016  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        88  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02205.mol:
          Name                           Similarity(%)
Phenindione                                   85.72
Anisindione                                   85.23
Methaqualone                                  84.79
Fluindione                                    84.60
Testolactone                                  84.48

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -2.435    H-bond Acceptor     -2.618 
 Volume               5.338    SASA                 8.910 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.343    Rotor Bonds          0.000 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.135    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.462    Total                1.289


                log BB                    log PMDCK
 Hydrophilic SASA    -0.409    Hydrophilic SASA    -0.509 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds          0.000                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.553    Total                2.654

