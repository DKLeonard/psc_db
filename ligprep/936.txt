 Primary Metabolites & Reactive FGs:
 > Reactive FG: unhindered ester
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   366.459  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     7.919  (   1.0 /  12.5) 
     Solute        Total        SASA     =   564.330  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   341.991  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    85.705  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   136.633  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1079.648  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    65.585  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     1.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     7.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.902  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.674  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.137  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    37.716M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.996M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    16.910M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     9.264M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.970  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.864  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.977  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.163  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.199  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         1  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -4.497  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       380  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       192  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.584  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.131  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        85  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00936.mol:
          Name                           Similarity(%)
Dolasetron                                    81.47
Ofloxacin                                     81.19
Hydroxyprogesterone                           81.04
Canrenone                                     80.88
Piribedil                                     80.54

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -3.409    H-bond Acceptor     -3.665 
 Volume               7.044    SASA                10.694 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.593    Rotor Bonds         -0.163 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.160    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.970    Total                1.864


                log BB                    log PMDCK
 Hydrophilic SASA    -0.702    Hydrophilic SASA    -0.878 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.060                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.199    Total                2.284

