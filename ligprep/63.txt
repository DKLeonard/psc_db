 Primary Metabolites & Reactive FGs:
 > Metabolism likely: para hydroxylation of aryl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   139.110  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.524  (   1.0 /  12.5) 
     Solute        Total        SASA     =   310.909  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =     0.000  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   197.541  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   113.369  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   466.285  ( 500.0 /2000.0)*
     Solute        vdW Polar SA (PSA)    =    93.538  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     1.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.500  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.935  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.492  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.840  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    12.947  (  13.0 /  70.0)*
   QP log P  for     hexadecane/gas      =     5.081  (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =     7.645  (   8.0 /  35.0)*
   QP log P  for     water/gas           =     7.274  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.543  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -0.963  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -1.175  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.865  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.002  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         1  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -1.312M (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        33  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        16  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.665  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.438  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        57  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  3
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00063.mol:
          Name                           Similarity(%)
Acipimox                                      92.84
Ethosuximide                                  91.87
Deferiprone                                   91.62
Methylthiouracil                              89.64
Acetaminophen                                 89.15

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -1.704    H-bond Acceptor     -1.833 
 Volume               3.042    SASA                 5.892 
 Ac x Dn^.5/SASA      0.499    Ac x Dn^.5/SASA      1.125 
 FISA                -0.851    Rotor Bonds         -0.163 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.133    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.113    Total                0.837


                log BB                    log PMDCK
 Hydrophilic SASA    -1.505    Hydrophilic SASA    -2.025 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.060                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids     -0.540 
 Constant             0.564    Constant             3.771
 Total               -1.002    Total                1.206

