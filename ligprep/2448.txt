 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   300.267  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.205  (   1.0 /  12.5) 
     Solute        Total        SASA     =   515.960  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =    92.706  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   169.631  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   253.623  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   881.595  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   104.599  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     5.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     4.500  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.862  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.709  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.320  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    28.339M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.833M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    14.949M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    10.099M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.925  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.254  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.429  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.073  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.261  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         4  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.978  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       243  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       107  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.272  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.089  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        81  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02448.mol:
          Name                           Similarity(%)
Aminaftone                                    91.46
Tolcapone                                     89.63
Warfarin                                      88.07
Zolmitriptan                                  87.42
Sulfaphenazole                                87.34

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -2.192    H-bond Acceptor     -2.356 
 Volume               5.752    SASA                 9.777 
 Ac x Dn^.5/SASA      0.547    Ac x Dn^.5/SASA      1.233 
 FISA                -1.174    Rotor Bonds         -0.814 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.297    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.925    Total                3.254


                log BB                    log PMDCK
 Hydrophilic SASA    -1.523    Hydrophilic SASA    -1.739 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.301                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.261    Total                2.032

