 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   154.252  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     1.931  (   1.0 /  12.5) 
     Solute        Total        SASA     =   399.790  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   326.589  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    41.648  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    31.552  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   653.995  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    21.150  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     2.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     1.700  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.911  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.897  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -1.033  (  -0.9 /   1.7)*
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    19.036  (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     5.183  (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =     7.720  (   8.0 /  35.0)*
   QP log P  for     water/gas           =     3.877  (   4.0 /  45.0)*
   QP log P  for     octanol/water       =     2.697  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.544  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -1.595  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.024  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.109  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -3.116M (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      3989  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      2207  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.984  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     4.012  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  4
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02343.mol:
          Name                           Similarity(%)
Phenylpropanol                                89.55
Isaxonine                                     89.01
Cyclobutyrol                                  87.58
Paroxypropione                                86.94
Sobrerol                                      86.85

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -0.828    H-bond Acceptor     -0.890 
 Volume               4.267    SASA                 7.576 
 Ac x Dn^.5/SASA      0.189    Ac x Dn^.5/SASA      0.425 
 FISA                -0.288    Rotor Bonds         -0.326 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.037    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.371    Total                2.601


                log BB                    log PMDCK
 Hydrophilic SASA    -0.334    Hydrophilic SASA    -0.427 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.121                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.109    Total                3.344

