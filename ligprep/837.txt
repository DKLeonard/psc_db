 Primary Metabolites & Reactive FGs:
 > Reactive FG: acetal or analog
 > Reactive FG: acetal or analog
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: acetal or analog
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: acetal or analog
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: primary alcohol -> acid

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   887.069  ( 130.0 / 725.0)*
     Solute        Dipole Moment (D)     =     7.104  (   1.0 /  12.5) 
     Solute        Total        SASA     =  1165.319  ( 300.0 /1000.0)*
     Solute        Hydrophobic  SASA     =   846.176  (   0.0 / 750.0)*
     Solute        Hydrophilic  SASA     =   319.143  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =     0.000  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  2414.836  ( 500.0 /2000.0)*
     Solute        vdW Polar SA (PSA)    =   242.238  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    17.000  (   0.0 /  15.0)*
     Solute as Donor -    Hydrogen Bonds =     9.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =    27.000  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.747  (  0.75 /  0.95)*
     Solute Ionization Potential (eV)    =    10.352  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -1.724  (  -0.9 /   1.7)*
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    79.572M (  13.0 /  70.0)*
   QP log P  for     hexadecane/gas      =    24.880M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    55.236M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    40.721M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.076  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.729M (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.889M (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.964M (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -4.274M (  -3.0 /   1.2)*
   No. of Primary Metabolites            =         9  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.626M (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         9M (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         3M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -5.769M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         6M (<25% is poor)
   Qual. Model for Human Oral Absorption =       lowM (>80% is high)

       A * indicates a violation of the 95% range. # stars = 15
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00837.mol:
          Name                           Similarity(%)
Troxerutin                                    50.76
Monoxerutin                                   49.08
Roxithromycin                                 48.86
Gitoformate                                   48.68
beta                                          47.87

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -2.699<   H-bond Donor        -3.614<
 H-bond Acceptor    -13.149    H-bond Acceptor    -14.137<
 Volume              15.754    SASA                22.083<
 Ac x Dn^.5/SASA      3.083    Ac x Dn^.5/SASA      6.948 
 FISA                -2.209    Rotor Bonds         -2.768<
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.000    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.076    Total                4.729


                log BB                    log PMDCK
 Hydrophilic SASA    -3.813<   Hydrophilic SASA    -3.271<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -1.024<                        0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000<   COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -4.274    Total                0.500

