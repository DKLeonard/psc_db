 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   286.456  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     1.601  (   1.0 /  12.5) 
     Solute        Total        SASA     =   562.030  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   485.237  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    32.431  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    44.362  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1020.421  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    18.232  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     2.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     0.750  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.872  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.702  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.289  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    33.823M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     8.542M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    12.301M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     3.356M (   4.0 /  45.0)*
   QP log P  for     octanol/water       =     5.174  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -5.881  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.987  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     1.168  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.159  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -3.812  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      4879  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      2744  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.769  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.006  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         1  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  2
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00977.mol:
          Name                           Similarity(%)
Ethylestrenol                                 89.65
Xibornol                                      88.75
Flumecinol                                    87.53
Pramiverine                                   87.29
Lynestrenol                                   87.27

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -0.365    H-bond Acceptor     -0.393 
 Volume               6.657    SASA                10.650 
 Ac x Dn^.5/SASA      0.059    Ac x Dn^.5/SASA      0.133 
 FISA                -0.224    Rotor Bonds         -0.326 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.052    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                5.174    Total                5.881


                log BB                    log PMDCK
 Hydrophilic SASA    -0.284    Hydrophilic SASA    -0.332 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.121                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.159    Total                3.438

