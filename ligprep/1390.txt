 Primary Metabolites & Reactive FGs:
 > Reactive FG: heteroatom in 3-ring
 > Reactive FG: unhindered ester
 > Reactive FG: activated cyclopropane
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   404.459  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     8.324  (   1.0 /  12.5) 
     Solute        Total        SASA     =   628.367  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   480.365  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   110.764  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    37.238  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1232.445  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   115.190  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     4.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     9.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.885  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.944  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.136  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    40.956M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    10.754M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    18.972M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    10.221M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.230  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.761  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.269  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.382  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.621  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         7  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -3.575  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       882  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       431  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.046  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.631  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        93  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01390.mol:
          Name                           Similarity(%)
Pivmecillinam                                 78.98
Cortisone                                     78.87
Meprednisone                                  77.01
Aranidipine                                   75.77
Eplerenone                                    74.79

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -4.383    H-bond Acceptor     -4.712 
 Volume               8.040    SASA                11.908 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.767    Rotor Bonds         -0.651 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.044    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.230    Total                2.761


                log BB                    log PMDCK
 Hydrophilic SASA    -0.943    Hydrophilic SASA    -1.135 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.241                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.621    Total                2.635

