 Primary Metabolites & Reactive FGs:
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acetal or analog
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   168.235  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.796  (   1.0 /  12.5) 
     Solute        Total        SASA     =   370.537  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   286.431  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    46.457  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    37.649  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   617.444  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    29.917  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     1.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.400  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.946  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.323  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.888  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    18.274  (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     5.219  (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =     8.600  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     5.776  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.497  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.811  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -1.509  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.328  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.157  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         2  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -2.579  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      3592  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      1970  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.147  (Kp in cm/hr)
   Jm, max transdermal transport rate    =    18.509  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)


       5 of   1712 molecules most similar to BC01589.mol:
          Name                           Similarity(%)
Hymecromone                                   91.52
Chlorzoxazone                                 88.36
Homarylamine                                  87.37
Aminorex                                      86.70
Tiliquinol                                    86.27

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -1.656    H-bond Acceptor     -1.780 
 Volume               4.028    SASA                 7.022 
 Ac x Dn^.5/SASA      0.407    Ac x Dn^.5/SASA      0.917 
 FISA                -0.322    Rotor Bonds         -0.163 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.044    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.497    Total                1.811


                log BB                    log PMDCK
 Hydrophilic SASA    -0.346    Hydrophilic SASA    -0.476 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.060                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.157    Total                3.295

