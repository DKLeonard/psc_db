 Primary Metabolites & Reactive FGs:
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   294.396  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.501  (   1.0 /  12.5) 
     Solute        Total        SASA     =   538.552  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   277.824  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    70.436  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   190.292  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   962.293  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    41.872  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     2.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.700  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.875  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.086  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.132  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    32.890M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.532M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    15.492M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     8.921M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.793  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.108  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.210  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.389  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.229  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -5.184  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       530  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       275  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.018  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.022  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        92  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02359.mol:
          Name                           Similarity(%)
Nalorphine                                    88.69
Norgestrienone                                87.81
Granisetron                                   87.64
Epimestrol                                    86.16
Oxymesterone                                  85.04

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -1.802    H-bond Acceptor     -1.937 
 Volume               6.278    SASA                10.206 
 Ac x Dn^.5/SASA      0.431    Ac x Dn^.5/SASA      0.971 
 FISA                -0.487    Rotor Bonds         -0.326 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.223    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.811    Total                3.108


                log BB                    log PMDCK
 Hydrophilic SASA    -0.613    Hydrophilic SASA    -0.722 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.121                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.229    Total                2.441

