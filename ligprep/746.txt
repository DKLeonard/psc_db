 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Reactive FG: unhindered ester
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   516.630  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     7.980  (   1.0 /  12.5) 
     Solute        Total        SASA     =   753.865  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   495.035  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   244.646  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    14.185  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1494.159  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   155.943  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     7.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     4.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    11.900  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.838  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.135  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.807  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    49.283M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    14.703M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    29.552M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    19.330M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.772  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.682  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.238  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.105  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -2.179  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =        10  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.214  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        47  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        18M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -5.306  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         1  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        54  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00746.mol:
          Name                           Similarity(%)
Prednisolone                                  82.21
Idarubicin                                    79.57
Dipyridamole                                  74.32
Sulfamazone                                   73.68
Sildenafil                                    71.46

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.200    H-bond Donor        -1.606 
 H-bond Acceptor     -5.795    H-bond Acceptor     -6.231 
 Volume               9.748    SASA                14.286 
 Ac x Dn^.5/SASA      1.400    Ac x Dn^.5/SASA      3.156 
 FISA                -1.693    Rotor Bonds         -1.140 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.017    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.772    Total                4.682


                log BB                    log PMDCK
 Hydrophilic SASA    -2.320    Hydrophilic SASA    -2.508 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.422                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -2.179    Total                1.263

