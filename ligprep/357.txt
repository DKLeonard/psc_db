 Primary Metabolites & Reactive FGs:
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   312.535  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.295  (   1.0 /  12.5) 
     Solute        Total        SASA     =   810.243  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   700.562  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   109.680  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =     0.000  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1374.371  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    51.745  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    18.000  (   0.0 /  15.0)*
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     2.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.738  (  0.75 /  0.95)*
     Solute Ionization Potential (eV)    =    11.339  (   7.9 /  10.5)*
     Solute Electron Affinity    (eV)    =    -0.885  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    37.291M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    12.383M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    13.515M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     1.879M (   4.0 /  45.0)*
   QP log P  for     octanol/water       =     6.854  (  -2.0 /   6.5)*
   QP log S  for   aqueous solubility    =    -7.439  (  -6.5 /   0.5)*
   QP log S - conformation independent   =    -4.733  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     1.070  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.864  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         1  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -3.967  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       228  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       127  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.813  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         1  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        96  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  6
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00357.mol:
          Name                           Similarity(%)
Tiadenol                                      73.94
Lonazolac                                     72.58
Fentiazac                                     71.01
Lonidamine                                    70.03
Triclabendazole                               69.39

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -0.974    H-bond Acceptor     -1.047 
 Volume               8.966    SASA                15.354 
 Ac x Dn^.5/SASA      0.109    Ac x Dn^.5/SASA      0.247 
 FISA                -0.243    Rotor Bonds         -2.930<
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.000    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                6.854    Total                7.439


                log BB                    log PMDCK
 Hydrophilic SASA    -1.343    Hydrophilic SASA    -1.124 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -1.085<                        0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000<   COOH/SO3H acids     -0.540 
 Constant             0.564    Constant             3.771
 Total               -1.864    Total                2.106

