 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Reactive FG: acetal or analog
 > Reactive FG: acetal or analog

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   610.524  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     7.373  (   1.0 /  12.5) 
     Solute        Total        SASA     =   767.045  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   183.119  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   430.879  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   153.048  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1571.925  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   271.319  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    15.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     9.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =    20.550  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.852  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.827  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.613  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    48.585M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    18.745M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    42.120M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    35.651M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -2.394  (  -2.0 /   6.5)*
   QP log S  for   aqueous solubility    =    -1.970  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.160  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.129  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -4.294  (  -3.0 /   1.2)*
   No. of Primary Metabolites            =        10  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.651  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         0  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -7.481M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         0  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  9
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00286.mol:
          Name                           Similarity(%)
Monoxerutin                                   84.54
Troxerutin                                    66.86
Lactitol                                      57.94
Lactulose                                     56.77
Thiamine                                      45.87

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -2.699<   H-bond Donor        -3.614<
 H-bond Acceptor    -10.008    H-bond Acceptor    -10.760 
 Volume              10.255    SASA                14.536 
 Ac x Dn^.5/SASA      3.565    Ac x Dn^.5/SASA      8.034 
 FISA                -2.982    Rotor Bonds         -2.442<
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.179    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               -2.394    Total                1.970


                log BB                    log PMDCK
 Hydrophilic SASA    -3.954<   Hydrophilic SASA    -4.417<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.904<                        0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -4.294    Total               -0.646

