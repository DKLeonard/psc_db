 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   264.321  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     7.308  (   1.0 /  12.5) 
     Solute        Total        SASA     =   477.050  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   300.826  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   136.328  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    39.896  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   848.315  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    81.303  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     3.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     5.450  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.908  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.023  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.165  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    26.250M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     8.040M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    14.995M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     9.702M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.395  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.727  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.543  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.173  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.719  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -3.203  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       504  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       236  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.603  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.123  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        83  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01352.mol:
          Name                           Similarity(%)
Molindone                                     84.71
Moxonidine                                    83.57
Tenonitrozole                                 83.08
Modafinil                                     82.85
Benzylsulfonamidobenzoic                      82.74

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -2.654    H-bond Acceptor     -2.854 
 Volume               5.534    SASA                 9.040 
 Ac x Dn^.5/SASA      0.717    Ac x Dn^.5/SASA      1.615 
 FISA                -0.944    Rotor Bonds         -0.488 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.047    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.395    Total                2.727


                log BB                    log PMDCK
 Hydrophilic SASA    -1.101    Hydrophilic SASA    -1.397 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.181                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.719    Total                2.373

