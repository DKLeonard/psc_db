 Primary Metabolites & Reactive FGs:
 > Metabolism likely: alpha hydroxylation of cyclic ether
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: benzylic-like H -> alcohol
 > Reactive FG: acetal or analog
 > Metabolism likely: para hydroxylation of aryl
 > Metabolism likely: para hydroxylation of aryl
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   564.542  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     8.380  (   1.0 /  12.5) 
     Solute        Total        SASA     =   816.633  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   286.233  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   388.067  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   142.333  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1549.652  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   231.231  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    13.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     8.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =    19.500  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.793  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.011  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.650  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    48.875M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    18.209M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    39.891M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    33.349M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -2.014  (  -2.0 /   6.5)*
   QP log S  for   aqueous solubility    =    -2.904  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.612  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.200  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -4.334  (  -3.0 /   1.2)*
   No. of Primary Metabolites            =        14  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.560  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         2  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -6.922  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         0  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  8
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02651.mol:
          Name                           Similarity(%)
Monoxerutin                                   64.12
Lactulose                                     62.42
Troxerutin                                    58.66
Thiamine                                      53.79
Penimepicycline                               53.66

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -2.399    H-bond Donor        -3.213 
 H-bond Acceptor     -9.496    H-bond Acceptor    -10.210 
 Volume              10.110    SASA                15.475 
 Ac x Dn^.5/SASA      2.996    Ac x Dn^.5/SASA      6.751 
 FISA                -2.686    Rotor Bonds         -2.116 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.167    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               -2.014    Total                2.904


                log BB                    log PMDCK
 Hydrophilic SASA    -4.114<   Hydrophilic SASA    -3.978<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.783                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -4.334    Total               -0.207

