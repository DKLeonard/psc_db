 Primary Metabolites & Reactive FGs:
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acetal or analog
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: acetal or analog
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: acetal or analog
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: primary alcohol -> acid

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   989.116  ( 130.0 / 725.0)*
     Solute        Dipole Moment (D)     =    13.067  (   1.0 /  12.5)*
     Solute        Total        SASA     =  1042.473  ( 300.0 /1000.0)*
     Solute        Hydrophobic  SASA     =   628.655  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   396.619  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =    17.199  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  2414.478  ( 500.0 /2000.0)*
     Solute        vdW Polar SA (PSA)    =   334.175  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    22.000  (   0.0 /  15.0)*
     Solute as Donor -    Hydrogen Bonds =    13.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =    32.900  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.835  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.574  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.872  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    76.511M (  13.0 /  70.0)*
   QP log P  for     hexadecane/gas      =    25.101M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    64.702M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    51.636M (   4.0 /  45.0)*
   QP log P  for     octanol/water       =    -1.520M (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.318M (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.212M (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.896M (  -1.5 /   1.5)*
   QP log BB for     brain/blood         =    -4.556M (  -3.0 /   1.2)*
   No. of Primary Metabolites            =        13  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -0.670M (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         0M (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -6.656M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.011M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         0M (<25% is poor)
   Qual. Model for Human Oral Absorption =       lowM (>80% is high)

       A * indicates a violation of the 95% range. # stars = 16
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00880.mol:
          Name                           Similarity(%)
Troxerutin                                    41.40
Glucosulfone                                  39.27
Gitoformate                                   38.05
Metildigoxin                                  36.01
beta                                          34.98

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -3.899<   H-bond Donor        -5.221<
 H-bond Acceptor    -16.022<   H-bond Acceptor    -17.226<
 Volume              15.752    SASA                19.755<
 Ac x Dn^.5/SASA      5.048<   Ac x Dn^.5/SASA     11.374<
 FISA                -1.714    Rotor Bonds         -3.582<
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.020    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               -1.520    Total                1.318


                log BB                    log PMDCK
 Hydrophilic SASA    -3.793<   Hydrophilic SASA    -4.065<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -1.326<                        0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids     -1.080 
 Constant             0.564    Constant             3.771
 Total               -4.556    Total               -1.375

