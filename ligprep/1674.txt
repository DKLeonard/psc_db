 Primary Metabolites & Reactive FGs:
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   360.320  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.942  (   1.0 /  12.5) 
     Solute        Total        SASA     =   593.382  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   255.745  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   165.277  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   172.360  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1040.075  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   116.370  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     6.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     6.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.837  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.733  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.722  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    33.263M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    10.691M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    17.614M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    10.959M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.251  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.970  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.067  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.028  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.372  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.997  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       268  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       119  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.382  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.016  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        84  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01674.mol:
          Name                           Similarity(%)
Pantoprazole                                  91.00
Phentetramine                                 90.13
Acenocoumarol                                 88.64
Voriconazole                                  88.27
Methysergide                                  88.25

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -2.922    H-bond Acceptor     -3.142 
 Volume               6.785    SASA                11.245 
 Ac x Dn^.5/SASA      0.634    Ac x Dn^.5/SASA      1.429 
 FISA                -1.144    Rotor Bonds         -0.977 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.202    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.251    Total                3.970


                log BB                    log PMDCK
 Hydrophilic SASA    -1.574    Hydrophilic SASA    -1.694 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.362                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.372    Total                2.077

