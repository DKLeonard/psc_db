 Primary Metabolites & Reactive FGs:
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   332.439  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.361  (   1.0 /  12.5) 
     Solute        Total        SASA     =   527.884  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   370.881  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   127.624  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    29.378  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1003.412  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    83.585  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     2.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     4.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.918  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.886  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.807  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    32.999M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.391M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    15.843M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     8.486M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     3.951  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.069  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.275  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.149  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.566  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =     0.620  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        39  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        24  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.576  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.008  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        79  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02395.mol:
          Name                           Similarity(%)
Ethisterone                                   86.99
Fluoxymesterone                               86.47
Medroxyprogesterone                           85.73
Oxymesterone                                  85.39
Oxymetholone                                  85.05

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -1.948    H-bond Acceptor     -2.094 
 Volume               6.546    SASA                10.003 
 Ac x Dn^.5/SASA      0.475    Ac x Dn^.5/SASA      1.071 
 FISA                 0.148    Rotor Bonds         -0.326 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.034    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                3.951    Total                4.069


                log BB                    log PMDCK
 Hydrophilic SASA    -1.009    Hydrophilic SASA    -1.308 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.121                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids     -1.080 
 Constant             0.564    Constant             3.771
 Total               -0.566    Total                1.382

