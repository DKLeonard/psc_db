 Primary Metabolites & Reactive FGs:
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: amine dealkylation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: amine dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   696.839  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.664  (   1.0 /  12.5) 
     Solute        Total        SASA     =  1021.627  ( 300.0 /1000.0)*
     Solute        Hydrophobic  SASA     =   841.959  (   0.0 / 750.0)*
     Solute        Hydrophilic  SASA     =     5.644  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   174.025  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  2029.788  ( 500.0 /2000.0)*
     Solute        vdW Polar SA (PSA)    =    59.032  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    11.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     9.750  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.759  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.371  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.463  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    69.677M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    18.441M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    28.196M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    10.577M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     6.900  (  -2.0 /   6.5)*
   QP log S  for   aqueous solubility    =    -6.242  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -8.777  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     1.435  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.632M (  -3.0 /   1.2) 
   No. of Primary Metabolites            =        15  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      ++ 
   HERG K+ Channel Blockage: log IC50    =    -7.716  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       544M (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       314M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.080M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         2  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        90  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  7
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01463.mol:
          Name                           Similarity(%)
Manidipine                                    69.42
Candesartan                                   69.05
Itraconazole                                  65.84
Amiodarone                                    64.63
Efonidipine                                   62.86

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -4.748    H-bond Acceptor     -5.105 
 Volume              13.242    SASA                19.360 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.039    Rotor Bonds         -1.791 
 Non-con amines      -1.054    N Protonation       -2.439 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.204    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                6.900    Total                6.242


                log BB                    log PMDCK
 Hydrophilic SASA    -0.065    Hydrophilic SASA    -0.058 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.663                         0.000 
 N Protonation        0.797    Non-con amines      -1.216 
 FOSA                 0.000<   COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.632    Total                2.497

