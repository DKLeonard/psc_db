 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   332.439  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.543  (   1.0 /  12.5) 
     Solute        Total        SASA     =   533.221  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   359.737  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   144.065  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    29.418  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1007.328  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    88.001  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     3.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     4.700  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.911  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.772  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.755  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    32.514M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.279M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    15.134M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     7.615M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     3.222  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.852  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.086  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.194  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.774  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         4  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -1.299  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       107  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        56  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.783  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.008  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        82  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02391.mol:
          Name                           Similarity(%)
Fluoxymesterone                               88.99
Nomegestrol                                   88.39
Medroxyprogesterone                           88.32
Oxymetholone                                  88.14
Indobufen                                     88.01

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -2.289    H-bond Acceptor     -2.461 
 Volume               6.572    SASA                10.105 
 Ac x Dn^.5/SASA      0.391    Ac x Dn^.5/SASA      0.881 
 FISA                -0.481    Rotor Bonds         -0.488 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.034    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                3.222    Total                3.852


                log BB                    log PMDCK
 Hydrophilic SASA    -1.156    Hydrophilic SASA    -1.477 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.181                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids     -0.540 
 Constant             0.564    Constant             3.771
 Total               -0.774    Total                1.754

