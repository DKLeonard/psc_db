 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Reactive FG: acetal or analog

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   303.268  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.200  (   1.0 /  12.5) 
     Solute        Total        SASA     =   498.741  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   133.395  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   309.964  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    55.382  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   879.000  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   170.307  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    10.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     6.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    14.150  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.890  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.905  (   7.9 /  10.5)*
     Solute Electron Affinity    (eV)    =     0.039  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    23.131M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    10.458M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    24.323M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    23.924M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -2.658  (  -2.0 /   6.5)*
   QP log S  for   aqueous solubility    =    -2.169  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -1.660  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.134  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -2.649  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -3.580  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        11  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         3  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -6.077  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.002  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         1  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        17  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  2
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00456.mol:
          Name                           Similarity(%)
Lactulose                                     74.54
Valganciclovir                                73.98
Azacitidine                                   69.19
Valaciclovir                                  68.45
Exifone                                       67.76

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.799    H-bond Donor        -2.410 
 H-bond Acceptor     -6.891    H-bond Acceptor     -7.409 
 Volume               5.735    SASA                 9.451 
 Ac x Dn^.5/SASA      3.083    Ac x Dn^.5/SASA      6.947 
 FISA                -2.145    Rotor Bonds         -1.628 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.065    WPSA                 0.000 
 Constant            -0.705    Constant            -2.783 
 Total               -2.658    Total                2.169


                log BB                    log PMDCK
 Hydrophilic SASA    -2.610<   Hydrophilic SASA    -3.177 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.603                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -2.649    Total                0.594

