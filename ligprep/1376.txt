 Primary Metabolites & Reactive FGs:
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: amine dealkylation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   343.422  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.243  (   1.0 /  12.5) 
     Solute        Total        SASA     =   586.289  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   411.361  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    48.134  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   126.795  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1089.868  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    47.923  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     6.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     5.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.874  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.525  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.103  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    34.821M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.878M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    15.243M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     7.386M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     3.337  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.964  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.915  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.375  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.180  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         8  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -4.908  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       863  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       467  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.446  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.133  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01376.mol:
          Name                           Similarity(%)
Tretoquinol                                   90.10
Poldine                                       90.03
Reboxetine                                    87.85
Encainide                                     87.70
Tiagabine                                     87.54

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -2.435    H-bond Acceptor     -2.618 
 Volume               7.110    SASA                11.110 
 Ac x Dn^.5/SASA      0.378    Ac x Dn^.5/SASA      0.852 
 FISA                -0.333    Rotor Bonds         -0.977 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.148    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                3.337    Total                2.964


                log BB                    log PMDCK
 Hydrophilic SASA    -0.421    Hydrophilic SASA    -0.493 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.362                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.180    Total                2.669

