 Primary Metabolites & Reactive FGs:
 > Metabolism likely: ether dealkylation
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   280.323  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.441  (   1.0 /  12.5) 
     Solute        Total        SASA     =   565.932  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   158.282  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    55.004  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   352.646  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   957.672  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    44.842  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     4.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.250  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.830  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.118  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.630  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    32.971M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.905M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    12.678M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     6.173M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     3.992  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.589  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.478  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.359  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.210  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -5.960  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      2980  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      1610  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -0.907  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.895  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00507.mol:
          Name                           Similarity(%)
Phenprocoumon                                 94.33
Ipriflavone                                   93.94
Proquazone                                    93.17
Diphenpyramide                                92.26
Prothipendyl                                  90.39

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -1.583    H-bond Acceptor     -1.702 
 Volume               6.248    SASA                10.724 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.381    Rotor Bonds         -0.651 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.413    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                3.992    Total                4.589


                log BB                    log PMDCK
 Hydrophilic SASA    -0.532    Hydrophilic SASA    -0.564 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.241                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.210    Total                3.207

