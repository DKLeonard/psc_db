 Primary Metabolites & Reactive FGs:
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   254.412  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.490  (   1.0 /  12.5) 
     Solute        Total        SASA     =   666.674  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   538.672  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   109.680  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    18.322  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1112.326  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    51.745  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    13.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     2.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.779  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.875  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.884  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    30.189M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.686M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    11.397M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     2.761M (   4.0 /  45.0)*
   QP log P  for     octanol/water       =     5.189  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -5.585  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.535  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.544  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.426  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -3.392  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       228  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       127  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.228  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.004  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         1  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        87  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00469.mol:
          Name                           Similarity(%)
Gemfibrozil                                   76.94
Tolfenamic                                    76.73
Tiadenol                                      76.20
Chlorambucil                                  75.64
Rosaprostol                                   74.98

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -0.974    H-bond Acceptor     -1.047 
 Volume               7.257    SASA                12.633 
 Ac x Dn^.5/SASA      0.133    Ac x Dn^.5/SASA      0.300 
 FISA                -0.243    Rotor Bonds         -2.116 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.021    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                5.189    Total                5.585


                log BB                    log PMDCK
 Hydrophilic SASA    -1.206    Hydrophilic SASA    -1.124 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.783                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids     -0.540 
 Constant             0.564    Constant             3.771
 Total               -1.426    Total                2.106

