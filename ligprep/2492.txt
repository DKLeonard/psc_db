 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Reactive FG: acetal or analog
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: acetal or analog
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: acetal or analog
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: primary alcohol -> acid

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   772.666  ( 130.0 / 725.0)*
     Solute        Dipole Moment (D)     =     8.827  (   1.0 /  12.5) 
     Solute        Total        SASA     =   951.326  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   268.612  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   471.652  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   211.061  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1972.922  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   336.253  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    22.000  (   0.0 /  15.0)*
     Solute as Donor -    Hydrogen Bonds =    12.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =    30.000  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.800  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.080  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.653  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    60.692M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    23.746M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    55.902M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    48.924M (   4.0 /  45.0)*
   QP log P  for     octanol/water       =    -4.214  (  -2.0 /   6.5)*
   QP log S  for   aqueous solubility    =    -1.056M (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.461M (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -2.093M (  -1.5 /   1.5)*
   QP log BB for     brain/blood         =    -5.680M (  -3.0 /   1.2)*
   No. of Primary Metabolites            =        13  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.780M (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         0M (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -7.356M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.003M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         0M (<25% is poor)
   Qual. Model for Human Oral Absorption =       lowM (>80% is high)

       A * indicates a violation of the 95% range. # stars = 13
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02492.mol:
          Name                           Similarity(%)
Troxerutin                                    57.13
Glucosulfone                                  56.90
Glycopin                                      49.89
Rifaximin                                     44.56
Bisbentiamine                                 41.94

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -3.599<   H-bond Donor        -4.819<
 H-bond Acceptor    -14.610<   H-bond Acceptor    -15.708<
 Volume              12.871    SASA                18.028 
 Ac x Dn^.5/SASA      4.846<   Ac x Dn^.5/SASA     10.920<
 FISA                -3.264<   Rotor Bonds         -3.582<
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.247    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               -4.214    Total                1.056


                log BB                    log PMDCK
 Hydrophilic SASA    -4.917<   Hydrophilic SASA    -4.834<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -1.326<                        0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -5.680    Total               -1.064

