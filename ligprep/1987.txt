 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acceptor carbonyl or derivative
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   381.468  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.638  (   1.0 /  12.5) 
     Solute        Total        SASA     =   643.106  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   488.908  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   109.425  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    44.773  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1220.009  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    96.985  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     9.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     7.450  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.859  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.427  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.083  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    37.319M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    10.875M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    17.433M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     9.133M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.608  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.575  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.099  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.102  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.570  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         7  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -4.760  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       226  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       109  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.577  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.027  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        84  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01987.mol:
          Name                           Similarity(%)
Roxatidine                                    84.08
Eprozinol                                     81.87
Cloricromen                                   81.59
Hydroxyzine                                   80.45
Trimethobenzamide                             79.66

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -3.628    H-bond Acceptor     -3.901 
 Volume               7.959    SASA                12.187 
 Ac x Dn^.5/SASA      0.514    Ac x Dn^.5/SASA      1.158 
 FISA                -0.757    Rotor Bonds         -1.465 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.052    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.608    Total                2.575


                log BB                    log PMDCK
 Hydrophilic SASA    -0.990    Hydrophilic SASA    -1.122 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.542                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.570    Total                2.041

