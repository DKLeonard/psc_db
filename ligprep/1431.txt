 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   258.273  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.771  (   1.0 /  12.5) 
     Solute        Total        SASA     =   513.400  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =    59.013  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   192.638  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   261.748  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   859.580  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    91.805  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     7.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.250  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.852  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.043  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.404  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    26.251M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.783M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    13.416M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     8.632M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.091  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.197  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.794  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.035  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.629  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.182  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       147  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        62  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.476  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.055  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        78  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01431.mol:
          Name                           Similarity(%)
Primaquine                                    88.60
Diethylstilbestrol                            88.35
Ftivazide                                     88.24
Tolcapone                                     88.07
Phenyramidol                                  87.87

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -1.583    H-bond Acceptor     -1.702 
 Volume               5.608    SASA                 9.729 
 Ac x Dn^.5/SASA      0.397    Ac x Dn^.5/SASA      0.895 
 FISA                -1.333    Rotor Bonds         -1.140 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.307    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.091    Total                3.197


                log BB                    log PMDCK
 Hydrophilic SASA    -1.771    Hydrophilic SASA    -1.975 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.422                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.629    Total                1.796

