 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: tertiary alcohol E1 or SN1

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   308.503  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.543  (   1.0 /  12.5) 
     Solute        Total        SASA     =   585.026  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   486.187  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    60.428  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    38.411  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1097.193  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    36.893  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     6.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     1.500  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.879  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.332  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.937  (  -0.9 /   1.7)*
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    34.270M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.654M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    14.409M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     4.883M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     4.910  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -5.100  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.637  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.935  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.319  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         2  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -3.667  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      2647  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      1417  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.922  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.029  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  2
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01056.mol:
          Name                           Similarity(%)
Methallenestril                               83.73
Benzestrol                                    83.68
Terbinafine                                   82.62
Mestilbol                                     82.60
Fluoxetine                                    82.29

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -0.730    H-bond Acceptor     -0.785 
 Volume               7.158    SASA                11.086 
 Ac x Dn^.5/SASA      0.161    Ac x Dn^.5/SASA      0.362 
 FISA                -0.418    Rotor Bonds         -0.977 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.045    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                4.910    Total                5.100


                log BB                    log PMDCK
 Hydrophilic SASA    -0.521    Hydrophilic SASA    -0.619 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.362                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.319    Total                3.151

