 Primary Metabolites & Reactive FGs:
 > Reactive FG: acetal or analog
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: acetal or analog
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: para hydroxylation of aryl
 > Reactive FG: acceptor carbonyl or derivative
 > Reactive FG: acetal or analog
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   735.694  ( 130.0 / 725.0)*
     Solute        Dipole Moment (D)     =     4.347  (   1.0 /  12.5) 
     Solute        Total        SASA     =  1035.145  ( 300.0 /1000.0)*
     Solute        Hydrophobic  SASA     =   321.608  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   426.106  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   287.430  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1998.574  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   288.077  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    23.000  (   0.0 /  15.0)*
     Solute as Donor -    Hydrogen Bonds =     9.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =    28.050  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.741  (  0.75 /  0.95)*
     Solute Ionization Potential (eV)    =     9.295  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.947  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    61.806M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    24.239M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    49.969M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    42.800M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -3.032  (  -2.0 /   6.5)*
   QP log S  for   aqueous solubility    =    -2.913  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.465  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -2.197  (  -1.5 /   1.5)*
   QP log BB for     brain/blood         =    -5.993M (  -3.0 /   1.2)*
   No. of Primary Metabolites            =        11  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -7.089  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         0M (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -6.152M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.001M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         0  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars = 14
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00452.mol:
          Name                           Similarity(%)
Troxerutin                                    75.81
Monoxerutin                                   59.75
Glycopin                                      46.99
Glucosulfone                                  45.11
Lactitol                                      41.81

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -2.699<   H-bond Donor        -3.614<
 H-bond Acceptor    -13.660<   H-bond Acceptor    -14.687<
 Volume              13.039    SASA                19.616 
 Ac x Dn^.5/SASA      3.606    Ac x Dn^.5/SASA      8.126 
 FISA                -2.949    Rotor Bonds         -3.744<
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.337    WPSA                 0.000 
 Constant            -0.705    Constant            -2.783 
 Total               -3.032    Total                2.913


                log BB                    log PMDCK
 Hydrophilic SASA    -5.170<   Hydrophilic SASA    -4.368<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -1.386<                        0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -5.993    Total               -0.597

