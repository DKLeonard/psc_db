 Primary Metabolites & Reactive FGs:
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   272.473  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     0.682  (   1.0 /  12.5)*
     Solute        Total        SASA     =   511.443  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   481.679  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =     0.000  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =    29.763  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   956.313  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =     0.000  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=     0.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     0.000  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.918  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.735  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -1.154  (  -0.9 /   1.7)*
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    32.402M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     7.615M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =     9.912M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    -0.221M (   4.0 /  45.0)*
   QP log P  for     octanol/water       =     6.065  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -7.770  (  -6.5 /   0.5)*
   QP log S - conformation independent   =    -7.770  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     1.394  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     1.157  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         2  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      ++ 
   HERG K+ Channel Blockage: log IC50    =    -3.003  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      9906  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      5899  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.415  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         1  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  7
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00316.mol:
          Name                           Similarity(%)
Ethylestrenol                                 82.66
Mitotane                                      81.72
Medazepam                                     81.24
Budipine                                      80.63
Lynestrenol                                   80.28

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor      0.000    H-bond Acceptor      0.000 
 Volume               8.705    SASA                 9.953 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                 0.000    Rotor Bonds          0.000 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.000    WPSA                 0.000 
 Constant            -2.623    Constant            -2.183 
 Total                6.082    Total                7.770


                log BB                    log PMDCK
 Hydrophilic SASA     0.000    Hydrophilic SASA     0.000 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds          0.000                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.965    COOH/SO3H acids      0.000 
 Constant             0.192    Constant             3.771
 Total                1.157    Total                3.771

