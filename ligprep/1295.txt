 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   288.299  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.011  (   1.0 /  12.5) 
     Solute        Total        SASA     =   542.316  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   148.198  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   159.938  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   234.180  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   928.057  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    95.182  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     8.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.848  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.906  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.178  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    28.086M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.802M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    12.482M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     6.360M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     3.004  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.772  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.572  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.225  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.400  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.101  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       301  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       135  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.874  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.065  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        89  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01295.mol:
          Name                           Similarity(%)
Tofenacin                                     88.05
Thonzylamine                                  87.06
Medrylamine                                   87.03
Pyrilamine                                    86.77
Tolterodine                                   86.68

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -1.461    H-bond Acceptor     -1.571 
 Volume               6.055    SASA                10.277 
 Ac x Dn^.5/SASA      0.245    Ac x Dn^.5/SASA      0.553 
 FISA                -1.107    Rotor Bonds         -1.302 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.274    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                3.002    Total                3.772


                log BB                    log PMDCK
 Hydrophilic SASA    -1.481    Hydrophilic SASA    -1.639 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.482                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.400    Total                2.131

