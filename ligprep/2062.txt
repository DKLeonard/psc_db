 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   272.300  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.135  (   1.0 /  12.5) 
     Solute        Total        SASA     =   505.854  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   171.943  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    87.002  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   246.909  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   865.891  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    57.426  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     3.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.869  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.697  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.100  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    28.931M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.082M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    14.004M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     8.430M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.942  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.779  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.199  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.196  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.386  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -4.837  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      1482  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       756  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.965  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.491  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02062.mol:
          Name                           Similarity(%)
Phentolamine                                  90.24
Epimestrol                                    89.15
Nomifensine                                   86.30
Epinastine                                    85.90
Cicletanine                                   85.80

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -1.461    H-bond Acceptor     -1.571 
 Volume               5.649    SASA                 9.586 
 Ac x Dn^.5/SASA      0.372    Ac x Dn^.5/SASA      0.838 
 FISA                -0.602    Rotor Bonds         -0.488 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.289    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.942    Total                3.779


                log BB                    log PMDCK
 Hydrophilic SASA    -0.769    Hydrophilic SASA    -0.892 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.181                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.386    Total                2.879

