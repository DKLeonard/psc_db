 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Reactive FG: acetal or analog

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   464.382  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =    11.857  (   1.0 /  12.5) 
     Solute        Total        SASA     =   657.336  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =    76.844  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   396.977  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   183.515  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1227.169  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   219.180  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    11.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     7.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =    13.750  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.843  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.148  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.950  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    37.647M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    15.297M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    32.624M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    26.619M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -1.572  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.404  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.032  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.858  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -3.821  (  -3.0 /   1.2)*
   No. of Primary Metabolites            =         8  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.943  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         1  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -7.133  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         2  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         0  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  4
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00285.mol:
          Name                           Similarity(%)
Valganciclovir                                66.25
Aminopterin                                   66.10
Methotrexate                                  64.76
Voglibose                                     63.94
Idarubicin                                    62.86

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -2.099    H-bond Donor        -2.811 
 H-bond Acceptor     -6.696    H-bond Acceptor     -7.199 
 Volume               8.006    SASA                12.457 
 Ac x Dn^.5/SASA      2.455    Ac x Dn^.5/SASA      5.532 
 FISA                -2.747    Rotor Bonds         -1.791 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.215    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               -1.572    Total                2.404


                log BB                    log PMDCK
 Hydrophilic SASA    -3.722<   Hydrophilic SASA    -4.069<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.663                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -3.821    Total               -0.298

