 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: heteroatom in 3-ring
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: alpha hydroxylation of carbonyl
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   486.690  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.222  (   1.0 /  12.5) 
     Solute        Total        SASA     =   759.321  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   635.241  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   124.050  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =     0.030  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1498.289  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    97.683  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     6.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     8.400  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.834  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.236  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.405  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    49.955M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    13.120M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    22.390M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    10.806M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     4.311  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -5.935  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.777  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.761  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.987  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -4.219  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       659  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       315M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.230  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02717.mol:
          Name                           Similarity(%)
Hydrocortisone                                79.66
Carindacillin                                 78.30
Simvastatin                                   74.64
Brovanexine                                   74.26
Buprenorphine                                 74.00

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -4.091    H-bond Acceptor     -4.398 
 Volume               9.775    SASA                14.389 
 Ac x Dn^.5/SASA      0.491    Ac x Dn^.5/SASA      1.106 
 FISA                -0.859    Rotor Bonds         -0.977 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.000    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                4.311    Total                5.935


                log BB                    log PMDCK
 Hydrophilic SASA    -1.189    Hydrophilic SASA    -1.272 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.362                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.987    Total                2.499

