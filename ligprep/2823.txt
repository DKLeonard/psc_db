 Primary Metabolites & Reactive FGs:
 > Reactive FG: heteroatom in 3-ring
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Reactive FG: unhindered ester
 > Reactive FG: unhindered ester
 > Metabolism likely: tertiary alcohol E1 or SN1

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   452.544  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.728  (   1.0 /  12.5) 
     Solute        Total        SASA     =   628.331  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   503.178  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   125.153  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =     0.000  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1270.243  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   121.566  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     5.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     6.250  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.903  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.728  (   7.9 /  10.5)*
     Solute Electron Affinity    (eV)    =    -0.737  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    41.471M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    11.181M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    18.561M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     8.600M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     3.814  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.630  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.540  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.668  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.762  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -3.106  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       644  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       307M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.346  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.005  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02823.mol:
          Name                           Similarity(%)
Nafcillin                                     75.08
Cyproterone                                   73.97
Bisacodyl                                     73.46
Norgestimate                                  72.92
Nimodipine                                    72.71

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -3.044    H-bond Acceptor     -3.272 
 Volume               8.287    SASA                11.907 
 Ac x Dn^.5/SASA      0.441    Ac x Dn^.5/SASA      0.994 
 FISA                -0.866    Rotor Bonds         -0.814 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.000    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                3.814    Total                4.630


                log BB                    log PMDCK
 Hydrophilic SASA    -1.024    Hydrophilic SASA    -1.283 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.301                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.762    Total                2.488

