 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   222.370  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     1.814  (   1.0 /  12.5) 
     Solute        Total        SASA     =   446.588  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   414.348  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    32.240  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =     0.000  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   804.448  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    18.837  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     2.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     0.750  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.937  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.344  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -2.554  (  -0.9 /   1.7)*
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    24.756M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     6.590M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =     9.182  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     2.775  (   4.0 /  45.0)*
   QP log P  for     octanol/water       =     3.730  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.728  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.096  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.556  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.198  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         1  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -2.557  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      4899  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      2756  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.922  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.498  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  3
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01459.mol:
          Name                           Similarity(%)
Bendazol                                      82.18
Ciprofibrate                                  79.78
Riluzole                                      79.72
Cloforex                                      79.69
Guanfacine                                    79.12

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -0.365    H-bond Acceptor     -0.393 
 Volume               5.248    SASA                 8.463 
 Ac x Dn^.5/SASA      0.074    Ac x Dn^.5/SASA      0.168 
 FISA                -0.223    Rotor Bonds         -0.326 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.000    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                3.730    Total                3.728


                log BB                    log PMDCK
 Hydrophilic SASA    -0.245    Hydrophilic SASA    -0.330 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.121                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.198    Total                3.440

