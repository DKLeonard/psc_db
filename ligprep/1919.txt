 Primary Metabolites & Reactive FGs:
 > Metabolism likely: alpha hydroxylation of cyclic ether
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: benzylic-like H -> alcohol
 > Reactive FG: acetal or analog
 > Metabolism likely: para hydroxylation of aryl
 > Metabolism likely: para hydroxylation of aryl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   580.541  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.495  (   1.0 /  12.5) 
     Solute        Total        SASA     =   836.307  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   262.178  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   438.619  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   135.510  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1572.183  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   254.154  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    15.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     9.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =    21.200  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.782  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.380  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     1.066  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    48.427M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    19.020M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    41.715M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    36.391M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -2.975  (  -2.0 /   6.5)*
   QP log S  for   aqueous solubility    =    -2.510  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.248  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.490  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -5.124  (  -3.0 /   1.2)*
   No. of Primary Metabolites            =        14  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.708  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         0  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -7.686  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         0  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  9
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01919.mol:
          Name                           Similarity(%)
Monoxerutin                                   73.07
Troxerutin                                    61.39
Lactitol                                      59.59
Lactulose                                     55.43
Thiamine                                      48.04

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -2.699<   H-bond Donor        -3.614<
 H-bond Acceptor    -10.324    H-bond Acceptor    -11.100<
 Volume              10.257    SASA                15.848 
 Ac x Dn^.5/SASA      3.374    Ac x Dn^.5/SASA      7.602 
 FISA                -3.036<   Rotor Bonds         -2.442<
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.159    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               -2.975    Total                2.510


                log BB                    log PMDCK
 Hydrophilic SASA    -4.784<   Hydrophilic SASA    -4.496<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.904<                        0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -5.124    Total               -0.725

