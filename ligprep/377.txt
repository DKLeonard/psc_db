 Primary Metabolites & Reactive FGs:
 > Reactive FG: unhindered ester
 > Reactive FG: unhindered ester
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   608.687  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.822  (   1.0 /  12.5) 
     Solute        Total        SASA     =   947.009  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   679.759  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    81.641  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   185.609  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1802.928  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   118.312  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     8.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    10.700  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.756  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.225  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.577  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    62.635M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    17.342M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    28.129M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    14.055M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     5.173  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -6.766  (  -6.5 /   0.5)*
   QP log S - conformation independent   =    -7.119  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.955  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.471  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         7  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -7.084  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       415  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       211M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.665M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        65  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00377.mol:
          Name                           Similarity(%)
Methoserpidine                                99.23
Syrosingopine                                 84.47
Efonidipine                                   77.01
Bromocriptine                                 75.36
Manidipine                                    73.28

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -5.211    H-bond Acceptor     -5.603 
 Volume              11.762    SASA                17.946 
 Ac x Dn^.5/SASA      0.501    Ac x Dn^.5/SASA      1.129 
 FISA                -0.565    Rotor Bonds         -1.302 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.217    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                5.173    Total                6.766


                log BB                    log PMDCK
 Hydrophilic SASA    -0.951    Hydrophilic SASA    -0.837 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.482                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000<   COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.471    Total                2.326

