 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: amine dealkylation
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acceptor carbonyl or derivative

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   407.549  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     1.327  (   1.0 /  12.5) 
     Solute        Total        SASA     =   706.904  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   494.734  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   204.342  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =     7.827  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1317.307  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   118.416  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     7.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     7.900  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.822  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.500  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.129  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    42.145M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    12.399M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    21.060M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    12.030M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.211  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.894  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.273  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.348  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.476  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.174  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        28  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        11  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -6.648  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        66  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00660.mol:
          Name                           Similarity(%)
Fludrocortisone                               85.80
Tianeptine                                    84.04
Cafedrine                                     82.46
Imidapril                                     81.33
Paramethasone                                 80.98

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -3.847    H-bond Acceptor     -4.136 
 Volume               8.594    SASA                13.396 
 Ac x Dn^.5/SASA      0.701    Ac x Dn^.5/SASA      1.580 
 FISA                -1.414    Rotor Bonds         -1.140 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.009    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.211    Total                3.894


                log BB                    log PMDCK
 Hydrophilic SASA    -2.016    Hydrophilic SASA    -2.095 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.422                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.476    Total                1.068

