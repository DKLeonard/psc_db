 Primary Metabolites & Reactive FGs:
 > Reactive FG: heteroatom in 3-ring
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: unhindered ester
 > Metabolism likely: furan epoxidation
 > Metabolism likely: alpha hydroxylation of carbonyl
 > Metabolism likely: benzylic-like H -> alcohol
 > Reactive FG: acceptor carbonyl or derivative
 > Reactive FG: acetal or analog

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   532.586  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.599  (   1.0 /  12.5) 
     Solute        Total        SASA     =   673.208  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   386.890  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   169.992  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   116.326  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1376.132  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   154.963  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     5.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     3.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    14.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.889  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.326  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.337  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    46.819M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    13.631M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    27.921M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    20.368M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.113  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.226  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.601  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.336  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.173  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -3.887  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       242  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       106M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.763  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.055  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         1  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        63  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00704.mol:
          Name                           Similarity(%)
Meproscillarin                                70.32
Prednisolone                                  68.13
Sulfamazone                                   67.81
Idarubicin                                    65.19
Cefuroxime                                    63.69

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.900    H-bond Donor        -1.205 
 H-bond Acceptor     -6.818    H-bond Acceptor     -7.330 
 Volume               8.978    SASA                12.757 
 Ac x Dn^.5/SASA      1.598    Ac x Dn^.5/SASA      3.601 
 FISA                -1.177    Rotor Bonds         -0.814 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.136    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.113    Total                3.226


                log BB                    log PMDCK
 Hydrophilic SASA    -1.435    Hydrophilic SASA    -1.742 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.301                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.173    Total                2.028

