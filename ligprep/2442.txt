 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   254.242  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.678  (   1.0 /  12.5) 
     Solute        Total        SASA     =   483.907  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =     0.000  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   164.240  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   319.667  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   803.286  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    80.452  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     2.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     4.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.864  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.132  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.849  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    27.762M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.483M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    15.134M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    10.603M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.841  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.293  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.710  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.068  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.025  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         2  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.263  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       274  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       122  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.228  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.070  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        81  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02442.mol:
          Name                           Similarity(%)
Phenytoin                                     91.31
Tizanidine                                    89.29
Anisindione                                   88.35
Valdecoxib                                    88.14
Afloqualone                                   87.79

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -1.948    H-bond Acceptor     -2.094 
 Volume               5.241    SASA                 9.170 
 Ac x Dn^.5/SASA      0.519    Ac x Dn^.5/SASA      1.169 
 FISA                -1.137    Rotor Bonds         -0.326 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.374    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.744    Total                3.333


                log BB                    log PMDCK
 Hydrophilic SASA    -1.468    Hydrophilic SASA    -1.683 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.121                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.025    Total                2.087

