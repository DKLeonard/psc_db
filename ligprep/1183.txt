 Primary Metabolites & Reactive FGs:
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acceptor carbonyl or derivative
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   374.390  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     1.549  (   1.0 /  12.5) 
     Solute        Total        SASA     =   597.890  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   344.587  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   167.713  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    85.590  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1116.450  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   120.343  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     5.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     9.450  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.871  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.137  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.954  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    36.133M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    10.664M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    18.468M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    12.551M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.317  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.964  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.329  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.380  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.213  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.036  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       254  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       112  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.829  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.060  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        78  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01183.mol:
          Name                           Similarity(%)
Meprednisone                                  82.24
Prednisone                                    81.28
Bunazosin                                     81.27
Polythiazide                                  80.49
Mefruside                                     79.75

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -4.602    H-bond Acceptor     -4.948 
 Volume               7.284    SASA                11.330 
 Ac x Dn^.5/SASA      0.701    Ac x Dn^.5/SASA      1.580 
 FISA                -1.161    Rotor Bonds         -0.814 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.100    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.317    Total                2.964


                log BB                    log PMDCK
 Hydrophilic SASA    -1.476    Hydrophilic SASA    -1.719 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.301                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.213    Total                2.052

