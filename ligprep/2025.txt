 Primary Metabolites & Reactive FGs:
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   302.326  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.379  (   1.0 /  12.5) 
     Solute        Total        SASA     =   549.025  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   260.865  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    98.343  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   189.818  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   950.078  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    66.987  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     4.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.750  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.851  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.684  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.140  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    31.112M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.510M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    14.964M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     8.741M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     3.038  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.169  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.537  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.252  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.582  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -4.843  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      1156  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       579  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.279  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.108  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02025.mol:
          Name                           Similarity(%)
Phentolamine                                  89.71
Epimestrol                                    89.69
Tinoridine                                    85.79
Dienogest                                     85.78
Gestrinone                                    85.72

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -1.826    H-bond Acceptor     -1.963 
 Volume               6.198    SASA                10.404 
 Ac x Dn^.5/SASA      0.428    Ac x Dn^.5/SASA      0.966 
 FISA                -0.681    Rotor Bonds         -0.651 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.222    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                3.038    Total                4.169


                log BB                    log PMDCK
 Hydrophilic SASA    -0.905    Hydrophilic SASA    -1.008 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.241                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.582    Total                2.763

