 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acetal or analog

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   536.661  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.591  (   1.0 /  12.5) 
     Solute        Total        SASA     =   743.863  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   488.449  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   242.367  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    13.047  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1499.650  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   152.982  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     7.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     5.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    13.950  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.852  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.432  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.306  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    49.492M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    14.920M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    31.842M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    22.766M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.984  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.053  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.583  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.162  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -2.085  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         7  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -3.992  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        49  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        19M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -5.268  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         1  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        50  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00803.mol:
          Name                           Similarity(%)
Idarubicin                                    75.63
Meproscillarin                                73.21
Prednisolone                                  69.41
Sulfamazone                                   66.92
Thiamine                                      65.54

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.500    H-bond Donor        -2.008 
 H-bond Acceptor     -6.794    H-bond Acceptor     -7.304 
 Volume               9.784    SASA                14.096 
 Ac x Dn^.5/SASA      1.860    Ac x Dn^.5/SASA      4.192 
 FISA                -1.677    Rotor Bonds         -1.140 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.015    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.984    Total                4.053


                log BB                    log PMDCK
 Hydrophilic SASA    -2.227    Hydrophilic SASA    -2.484 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.422                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -2.085    Total                1.287

