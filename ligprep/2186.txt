 Primary Metabolites & Reactive FGs:
 > Reactive FG: unhindered ester
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Reactive FG: acetal or analog

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   428.438  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.942  (   1.0 /  12.5) 
     Solute        Total        SASA     =   629.994  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   507.037  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    46.266  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    76.691  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1192.569  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    80.458  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     4.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     7.500  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.863  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.893  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.175  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    39.737M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    10.188M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    17.169M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     8.577M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     3.193  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.577  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.623  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.096  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.091  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         7  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -4.110  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      3607  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      1979  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.718  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     2.170  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02186.mol:
          Name                           Similarity(%)
Flavoxate                                     83.68
Bisoxatin                                     83.66
Nicergoline                                   83.51
Vesnarinone                                   83.36
Sulfinpyrazone                                83.20

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -3.652    H-bond Acceptor     -3.927 
 Volume               7.780    SASA                11.938 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.320    Rotor Bonds         -0.651 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.090    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                3.193    Total                3.577


                log BB                    log PMDCK
 Hydrophilic SASA    -0.414    Hydrophilic SASA    -0.474 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.241                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.091    Total                3.297

