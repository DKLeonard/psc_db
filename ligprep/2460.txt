 Primary Metabolites & Reactive FGs:
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: primary alcohol -> acid

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   196.202  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.433  (   1.0 /  12.5) 
     Solute        Total        SASA     =   419.751  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   171.081  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   142.315  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   106.355  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   676.880  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    72.243  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     6.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     3.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.950  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.888  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.760  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.408  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    18.098  (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     6.916  (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    12.271  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     9.382  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.750  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.551  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.005  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.576  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.001  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         4  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -3.848  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       442  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       205  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.192  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     3.549  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        79  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)


       5 of   1712 molecules most similar to BC02460.mol:
          Name                           Similarity(%)
Etilefrine                                    95.83
Mephenesin                                    93.87
Guaifenesin                                   92.66
Mephenesin                                    92.63
Chlorphenesin                                 89.51

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.900    H-bond Donor        -1.205 
 H-bond Acceptor     -1.924    H-bond Acceptor     -2.068 
 Volume               4.416    SASA                 7.954 
 Ac x Dn^.5/SASA      0.723    Ac x Dn^.5/SASA      1.629 
 FISA                -0.985    Rotor Bonds         -0.977 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.125    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.750    Total                1.551


                log BB                    log PMDCK
 Hydrophilic SASA    -1.203    Hydrophilic SASA    -1.459 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.362                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.001    Total                2.312

