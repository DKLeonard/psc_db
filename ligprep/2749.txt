 Primary Metabolites & Reactive FGs:
 > Reactive FG: acetal or analog
 > Reactive FG: acetal or analog
 > Metabolism likely: alpha hydroxylation of cyclic ether
 > Reactive FG: unhindered ester
 > Reactive FG: unhindered ester
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: tertiary alcohol E1 or SN1

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   869.054  ( 130.0 / 725.0)*
     Solute        Dipole Moment (D)     =     5.251  (   1.0 /  12.5) 
     Solute        Total        SASA     =  1177.852  ( 300.0 /1000.0)*
     Solute        Hydrophobic  SASA     =   832.695  (   0.0 / 750.0)*
     Solute        Hydrophilic  SASA     =   345.157  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =     0.000  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  2461.607  ( 500.0 /2000.0)*
     Solute        vdW Polar SA (PSA)    =   248.302  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    15.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     7.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =    22.500  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.749  (  0.75 /  0.95)*
     Solute Ionization Potential (eV)    =    10.476  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.822  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    82.728M (  13.0 /  70.0)*
   QP log P  for     hexadecane/gas      =    24.959M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    51.105M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    34.007M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.151  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -6.555M (  -6.5 /   0.5)*
   QP log S - conformation independent   =    -7.176M (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.007M (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -4.447M (  -3.0 /   1.2)*
   No. of Primary Metabolites            =         8  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.601M (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         5M (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         1M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -6.441M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         3  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        14M (<25% is poor)
   Qual. Model for Human Oral Absorption =       lowM (>80% is high)

       A * indicates a violation of the 95% range. # stars = 14
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02749.mol:
          Name                           Similarity(%)
beta                                          68.97
Acetyldigoxin                                 68.95
Metildigoxin                                  64.33
Dirithromycin                                 60.46
Sorbinicate                                   58.72

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -2.099    H-bond Donor        -2.811 
 H-bond Acceptor    -10.957    H-bond Acceptor    -11.781<
 Volume              16.060    SASA                22.320<
 Ac x Dn^.5/SASA      2.242    Ac x Dn^.5/SASA      5.052 
 FISA                -2.389    Rotor Bonds         -2.442<
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.000    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.151    Total                6.555


                log BB                    log PMDCK
 Hydrophilic SASA    -4.107<   Hydrophilic SASA    -3.538<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.904<                        0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000<   COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -4.447    Total                0.233

