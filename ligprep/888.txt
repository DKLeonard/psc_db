 Primary Metabolites & Reactive FGs:
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acetal or analog
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: acetal or analog
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: acetal or analog
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: acetal or analog
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: acetal or analog
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: acetal or analog
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: primary alcohol -> acid

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =  1381.518  ( 130.0 / 725.0)*
     Solute        Dipole Moment (D)     =    12.614  (   1.0 /  12.5)*
     Solute        Total        SASA     =  1456.972  ( 300.0 /1000.0)*
     Solute        Hydrophobic  SASA     =   804.005  (   0.0 / 750.0)*
     Solute        Hydrophilic  SASA     =   595.796  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =    57.171  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  3386.505  ( 500.0 /2000.0)*
     Solute        vdW Polar SA (PSA)    =   473.842  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    34.000  (   0.0 /  15.0)*
     Solute as Donor -    Hydrogen Bonds =    17.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =    50.300  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.749  (  0.75 /  0.95)*
     Solute Ionization Potential (eV)    =     9.455  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.592  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =   108.089M (  13.0 /  70.0)*
   QP log P  for     hexadecane/gas      =    36.491M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    91.333M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    74.610M (   4.0 /  45.0)*
   QP log P  for     octanol/water       =    -5.432M (  -2.0 /   6.5)*
   QP log S  for   aqueous solubility    =     0.644M (  -6.5 /   0.5)*
   QP log S - conformation independent   =    -5.628M (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -3.578M (  -1.5 /   1.5)*
   QP log BB for     brain/blood         =    -8.575M (  -3.0 /   1.2)*
   No. of Primary Metabolites            =        20  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.066M (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         0M (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -9.034M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.006M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         0M (<25% is poor)
   Qual. Model for Human Oral Absorption =       lowM (>80% is high)

       A * indicates a violation of the 95% range. # stars = 20
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00888.mol:
          Name                           Similarity(%)
Glucosulfone                                  31.08
Everolimus                                    28.50
Lactitol                                      26.49
Glycopin                                      25.19
Lactulose                                     24.92

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -5.098<   H-bond Donor        -6.827<
 H-bond Acceptor    -24.496<   H-bond Acceptor    -26.337<
 Volume              22.094<   SASA                27.610<
 Ac x Dn^.5/SASA      6.314<   Ac x Dn^.5/SASA     14.229<
 FISA                -3.608<   Rotor Bonds         -5.535<
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.067    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               -5.432    Total               -0.644


                log BB                    log PMDCK
 Hydrophilic SASA    -7.089<   Hydrophilic SASA    -6.107<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -2.049<                        0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000<   COOH/SO3H acids     -0.540 
 Constant             0.564    Constant             3.771
 Total               -8.575    Total               -2.876

