 Primary Metabolites & Reactive FGs:
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: amine dealkylation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   373.448  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.145  (   1.0 /  12.5) 
     Solute        Total        SASA     =   663.965  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   444.093  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    83.977  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   135.895  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1196.082  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    67.867  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     8.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     5.750  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.821  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.818  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.065  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    37.874M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    11.335M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    17.967M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     9.641M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     3.292  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.688  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.175  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.374  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.351  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         9  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -5.807  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       394  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       200  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.883  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.010  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        93  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02632.mol:
          Name                           Similarity(%)
Quinagolide                                   88.08
Cyclovalone                                   88.00
Tretoquinol                                   87.45
Glafenine                                     86.95
Nemonapride                                   85.98

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -2.800    H-bond Acceptor     -3.011 
 Volume               7.803    SASA                12.582 
 Ac x Dn^.5/SASA      0.543    Ac x Dn^.5/SASA      1.224 
 FISA                -0.581    Rotor Bonds         -1.302 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.159    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                3.292    Total                3.688


                log BB                    log PMDCK
 Hydrophilic SASA    -0.831    Hydrophilic SASA    -0.861 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.482                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.351    Total                2.302

