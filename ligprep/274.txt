 Primary Metabolites & Reactive FGs:
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   544.946  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     0.116  (   1.0 /  12.5)*
     Solute        Total        SASA     =  1275.753  ( 300.0 /1000.0)*
     Solute        Hydrophobic  SASA     =  1163.608  (   0.0 / 750.0)*
     Solute        Hydrophilic  SASA     =     0.000  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   112.145  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  2302.051  ( 500.0 /2000.0)*
     Solute        vdW Polar SA (PSA)    =     0.000  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    21.000  (   0.0 /  15.0)*
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     0.000  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.661  (  0.75 /  0.95)*
     Solute Ionization Potential (eV)    =     8.433  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.294  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    73.561M (  13.0 /  70.0)*
   QP log P  for     hexadecane/gas      =    22.454M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    22.856M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    -4.007M (   4.0 /  45.0)*
   QP log P  for     octanol/water       =    19.118  (  -2.0 /   6.5)*
   QP log S  for   aqueous solubility    =   -21.777  (  -6.5 /   0.5)*
   QP log S - conformation independent   =   -21.777  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     4.409  (  -1.5 /   1.5)*
   QP log BB for     brain/blood         =     2.523  (  -3.0 /   1.2)*
   No. of Primary Metabolites            =        22  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      ++ 
   HERG K+ Channel Blockage: log IC50    =    -7.554  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      9906  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      5899M (<25 poor, >500 great)
   QP log Kp for skin permeability       =     0.892  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         2  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars = 17
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00274.mol:
          Name                           Similarity(%)
Gefarnate                                     55.86
Ethchlorvynol                                 51.00
Meparfynol                                    51.00
Mechlorethamine                               51.00
Chloramphenicol                               50.02

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor      0.000    H-bond Acceptor      0.000 
 Volume              21.741    SASA                23.960<
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                 0.000    Rotor Bonds          0.000<
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.000    WPSA                 0.000 
 Constant            -2.623    Constant            -2.183 
 Total               19.118    Total               21.777


                log BB                    log PMDCK
 Hydrophilic SASA     0.000    Hydrophilic SASA     0.000 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds          0.000<                        0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 2.331<   COOH/SO3H acids      0.000 
 Constant             0.192    Constant             3.771
 Total                2.523    Total                3.771

