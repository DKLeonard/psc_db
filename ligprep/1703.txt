 Primary Metabolites & Reactive FGs:
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   288.256  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.641  (   1.0 /  12.5) 
     Solute        Total        SASA     =   490.137  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   174.472  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   116.284  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   199.381  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   841.279  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    88.870  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     4.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     4.500  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.879  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.816  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.630  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    26.850M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     8.543M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    12.939M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     8.061M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.128  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.014  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.123  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.100  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.680  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         4  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -4.436  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       781  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       379  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.576  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.741  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        91  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01703.mol:
          Name                           Similarity(%)
Rizatriptan                                   90.10
Mebendazole                                   89.72
Tolcapone                                     88.73
Benzarone                                     87.61
Flubendazole                                  87.42

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -2.192    H-bond Acceptor     -2.356 
 Volume               5.489    SASA                 9.288 
 Ac x Dn^.5/SASA      0.407    Ac x Dn^.5/SASA      0.918 
 FISA                -0.805    Rotor Bonds         -0.651 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.233    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.128    Total                3.014


                log BB                    log PMDCK
 Hydrophilic SASA    -1.003    Hydrophilic SASA    -1.192 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.241                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.680    Total                2.579

