 Primary Metabolites & Reactive FGs:
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   370.401  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.856  (   1.0 /  12.5) 
     Solute        Total        SASA     =   668.162  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   323.057  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   162.365  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   182.740  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1171.137  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   102.614  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     6.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     4.750  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.804  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.014  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.427  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    38.607M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    11.992M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    18.263M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     9.959M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     3.559  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -5.617  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.598  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.559  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.472  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         8  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.572  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       285  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       127  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.292  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        92  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02052.mol:
          Name                           Similarity(%)
Cyclovalone                                   87.23
Rosiglitazone                                 85.71
Pioglitazone                                  85.54
Nemonapride                                   85.51
Tretoquinol                                   85.25

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -2.313    H-bond Acceptor     -2.487 
 Volume               7.640    SASA                12.662 
 Ac x Dn^.5/SASA      0.446    Ac x Dn^.5/SASA      1.005 
 FISA                -1.124    Rotor Bonds         -0.977 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.214    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                3.559    Total                5.617


                log BB                    log PMDCK
 Hydrophilic SASA    -1.674    Hydrophilic SASA    -1.664 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.362                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.472    Total                2.107

