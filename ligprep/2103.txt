 Primary Metabolites & Reactive FGs:
 > Metabolism likely: alpha hydroxylation of cyclic ether
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   432.513  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     0.466  (   1.0 /  12.5)*
     Solute        Total        SASA     =   745.385  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   653.479  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =     0.000  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =    91.906  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1376.734  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    45.858  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     6.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     6.200  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.803  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.924  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.042  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    45.968M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    11.690M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    17.649M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     6.922M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     3.998  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -6.821  (  -6.5 /   0.5)*
   QP log S - conformation independent   =    -6.821  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.701  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.334  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         9  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.159  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      9906  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      5899M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -0.620  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.016  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  4
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02103.mol:
          Name                           Similarity(%)
Etoperidone                                   85.86
Troglitazone                                  83.76
Fluanisone                                    83.58
Nemonapride                                   82.74
Theofibrate                                   82.48

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -3.253    H-bond Acceptor     -5.325 
 Volume               8.625    SASA                14.329 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                 0.000    Rotor Bonds          0.000 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.000    WPSA                 0.000 
 Constant            -1.374    Constant            -2.183 
 Total                3.998    Total                6.821


                log BB                    log PMDCK
 Hydrophilic SASA    -2.835    Hydrophilic SASA     0.000 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds          0.000                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 1.309<   COOH/SO3H acids      0.000 
 Constant             0.192    Constant             3.771
 Total               -1.334    Total                3.771

