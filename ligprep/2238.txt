 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Reactive FG: acetal or analog

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   509.639  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     7.907  (   1.0 /  12.5) 
     Solute        Total        SASA     =   699.240  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   512.343  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   186.896  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =     0.000  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1399.588  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   136.187  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     7.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     7.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =    11.800  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.865  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.157  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -2.141  (  -0.9 /   1.7)*
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    45.363M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    14.030M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    32.273M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    23.004M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.740  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.582  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.439  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.130  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.124  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.436  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        41  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        17M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -6.355  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.001  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         2  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        34  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  2
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02238.mol:
          Name                           Similarity(%)
Idarubicin                                    79.32
Colforsin                                     66.38
Sildenafil                                    65.70
Vardenafil                                    63.95
Meproscillarin                                63.85

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -2.099    H-bond Donor        -2.811 
 H-bond Acceptor     -5.747    H-bond Acceptor     -6.178 
 Volume               9.131    SASA                13.251 
 Ac x Dn^.5/SASA      1.981    Ac x Dn^.5/SASA      4.463 
 FISA                -1.294    Rotor Bonds         -1.140 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.000    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.740    Total                2.582


                log BB                    log PMDCK
 Hydrophilic SASA    -1.664    Hydrophilic SASA    -1.916 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.422                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.124    Total                1.247

