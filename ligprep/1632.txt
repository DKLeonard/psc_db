 Primary Metabolites & Reactive FGs:
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: alpha hydroxylation of carbonyl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   150.220  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.825  (   1.0 /  12.5) 
     Solute        Total        SASA     =   369.089  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   294.291  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    44.203  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    30.595  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   601.880  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    26.532  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     1.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     2.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.934  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.397  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.104  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    17.583  (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     4.642  (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =     6.259  (   8.0 /  35.0)*
   QP log P  for     water/gas           =     2.865  (   4.0 /  45.0)*
   QP log P  for     octanol/water       =     1.978  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.001  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -1.191  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.212  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.165  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         2  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -2.745M (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      3773  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      2078  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.130  (Kp in cm/hr)
   Jm, max transdermal transport rate    =    11.089  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  2
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01632.mol:
          Name                           Similarity(%)
Clomethiazole                                 83.29
Pempidine                                     81.99
Isaxonine                                     80.25
Carbimazole                                   80.23
Phenylpropanol                                79.13

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -0.974    H-bond Acceptor     -1.047 
 Volume               3.927    SASA                 6.994 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.306    Rotor Bonds         -0.163 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.036    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.978    Total                2.001


                log BB                    log PMDCK
 Hydrophilic SASA    -0.338    Hydrophilic SASA    -0.453 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.060                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.165    Total                3.318

