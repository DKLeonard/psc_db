 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aniline NH -> NOH or NCOR
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: para hydroxylation of aryl
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   379.455  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.902  (   1.0 /  12.5) 
     Solute        Total        SASA     =   702.514  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   381.557  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   143.427  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   177.530  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1254.105  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    94.765  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     7.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     3.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.750  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.801  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.154  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.425  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    41.236M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    12.849M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    20.176M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    10.137M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     4.376  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -6.146  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -6.033  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.805  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.350  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =        11  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.606  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       432  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       199  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.865  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02122.mol:
          Name                           Similarity(%)
Amodiaquine                                   83.22
Bopindolol                                    82.89
Bezafibrate                                   81.76
Dimoxyline                                    80.65
Flecainide                                    80.29

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.900    H-bond Donor        -1.205 
 H-bond Acceptor     -1.826    H-bond Acceptor     -1.963 
 Volume               8.182    SASA                13.313 
 Ac x Dn^.5/SASA      0.410    Ac x Dn^.5/SASA      0.924 
 FISA                -0.993    Rotor Bonds         -1.140 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.208    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                4.376    Total                6.146


                log BB                    log PMDCK
 Hydrophilic SASA    -1.492    Hydrophilic SASA    -1.470 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.422                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.350    Total                2.301

