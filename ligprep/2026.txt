 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   354.359  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.059  (   1.0 /  12.5) 
     Solute        Total        SASA     =   651.831  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   216.514  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   199.089  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   236.229  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1115.861  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   114.915  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     7.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     3.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     4.500  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.798  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.710  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.281  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    36.264M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    12.320M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    18.987M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    11.531M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.913  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -5.064  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.392  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.321  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.942  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         7  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.924  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       128  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        53  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.684  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.001  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        82  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02026.mol:
          Name                           Similarity(%)
Tretoquinol                                   88.81
Pioglitazone                                  86.09
Ditazole                                      85.66
Amodiaquine                                   85.49
Glafenine                                     85.13

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.900    H-bond Donor        -1.205 
 H-bond Acceptor     -2.192    H-bond Acceptor     -2.356 
 Volume               7.280    SASA                12.352 
 Ac x Dn^.5/SASA      0.530    Ac x Dn^.5/SASA      1.195 
 FISA                -1.378    Rotor Bonds         -1.140 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.277    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.913    Total                5.064


                log BB                    log PMDCK
 Hydrophilic SASA    -2.083    Hydrophilic SASA    -2.041 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.422                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.942    Total                1.730

