 Primary Metabolites & Reactive FGs:
 > Metabolism likely: alpha hydroxylation of cyclic ether
 > Metabolism likely: alpha hydroxylation of cyclic ether
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   610.524  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.218  (   1.0 /  12.5) 
     Solute        Total        SASA     =   848.571  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   153.796  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   518.995  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   175.780  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1617.615  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   295.931  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    14.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =    11.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =    21.500  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.785  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.041  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.979  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    51.272M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    20.706M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    46.361M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    40.687M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -3.579  (  -2.0 /   6.5)*
   QP log S  for   aqueous solubility    =    -2.743  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.696  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.419  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -5.890  (  -3.0 /   1.2)*
   No. of Primary Metabolites            =        16  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.844  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         0  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -9.121M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         0  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  9
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01737.mol:
          Name                           Similarity(%)
Monoxerutin                                   69.57
Troxerutin                                    55.89
Lactitol                                      50.91
Lactulose                                     49.70
Penimepicycline                               47.35

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -3.299<   H-bond Donor        -4.418<
 H-bond Acceptor    -10.470    H-bond Acceptor    -11.257<
 Volume              10.553    SASA                16.080 
 Ac x Dn^.5/SASA      3.728    Ac x Dn^.5/SASA      8.400 
 FISA                -3.592<   Rotor Bonds         -2.279 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.206    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               -3.579    Total                2.743


                log BB                    log PMDCK
 Hydrophilic SASA    -5.610<   Hydrophilic SASA    -5.320<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.844                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -5.890    Total               -1.549

