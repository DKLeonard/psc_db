 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   260.289  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.929  (   1.0 /  12.5) 
     Solute        Total        SASA     =   495.926  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   246.548  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   172.385  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    76.994  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   848.430  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    90.201  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     5.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.250  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.874  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.336  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.963  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    25.325M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     8.343M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    13.092M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     7.636M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.956  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.223  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.714  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.032  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.243  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -3.939  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       229  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       100  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.945  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.018  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        81  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01484.mol:
          Name                           Similarity(%)
Tolcapone                                     89.73
Salizid                                       88.89
Phenyramidol                                  88.58
Pronethalol                                   88.20
Amfenac                                       87.77

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -1.583    H-bond Acceptor     -1.702 
 Volume               5.535    SASA                 9.398 
 Ac x Dn^.5/SASA      0.411    Ac x Dn^.5/SASA      0.926 
 FISA                -1.193    Rotor Bonds         -0.814 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.090    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.956    Total                3.223


                log BB                    log PMDCK
 Hydrophilic SASA    -1.505    Hydrophilic SASA    -1.767 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.301                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.243    Total                2.004

