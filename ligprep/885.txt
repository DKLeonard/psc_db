 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acetal or analog
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   632.833  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.305  (   1.0 /  12.5) 
     Solute        Total        SASA     =   890.489  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   599.741  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   272.473  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    18.275  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1810.618  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   167.953  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     7.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     4.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    11.500  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.807  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.487  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.592  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    61.987M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    17.763M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    33.431M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    19.482M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     4.620  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -6.906  (  -6.5 /   0.5)*
   QP log S - conformation independent   =    -7.304  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.560  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -2.649M (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -1.131  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         1M (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -5.805M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         1  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        45  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00885.mol:
          Name                           Similarity(%)
Bromocriptine                                 70.31
Meproscillarin                                70.28
Methoserpidine                                69.57
Dihydroergocryptine                           66.75
Syrosingopine                                 64.90

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.200    H-bond Donor        -1.606 
 H-bond Acceptor     -5.601    H-bond Acceptor     -6.021 
 Volume              11.812    SASA                16.875 
 Ac x Dn^.5/SASA      1.146    Ac x Dn^.5/SASA      2.582 
 FISA                -0.854    Rotor Bonds         -1.140 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.021    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                4.620    Total                6.906


                log BB                    log PMDCK
 Hydrophilic SASA    -2.791<   Hydrophilic SASA    -2.793 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.422                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids     -1.080 
 Constant             0.564    Constant             3.771
 Total               -2.649    Total               -0.102

