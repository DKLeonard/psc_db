 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Reactive FG: acceptor carbonyl or derivative

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   462.625  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.074  (   1.0 /  12.5) 
     Solute        Total        SASA     =   720.874  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   490.170  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   205.032  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    25.672  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1421.744  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   120.791  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    10.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     5.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     8.600  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.848  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.948  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.002  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    44.568M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    14.090M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    26.966M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    16.611M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.677  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.405  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.140  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.253  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.939  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         9  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.110  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       112  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        46M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.248  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.001  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        79  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00758.mol:
          Name                           Similarity(%)
"Estriol                                      75.65
Cisapride                                     74.48
Trimazosin                                    73.60
Ebrotidine                                    73.35
Mycophenolate                                 73.32

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.500    H-bond Donor        -2.008 
 H-bond Acceptor     -4.188    H-bond Acceptor     -4.503 
 Volume               9.275    SASA                13.661 
 Ac x Dn^.5/SASA      1.183    Ac x Dn^.5/SASA      2.667 
 FISA                -1.419    Rotor Bonds         -1.628 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.030    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.677    Total                4.405


                log BB                    log PMDCK
 Hydrophilic SASA    -1.900    Hydrophilic SASA    -2.102 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.603                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.939    Total                1.669

