 Primary Metabolites & Reactive FGs:
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   172.229  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.622  (   1.0 /  12.5) 
     Solute        Total        SASA     =   401.422  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   199.368  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    19.497  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   182.557  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   645.643  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    15.576  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     0.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     1.500  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.900  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.219  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.086  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    21.428  (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     5.676  (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =     7.371  (   8.0 /  35.0)*
   QP log P  for     water/gas           =     3.537  (   4.0 /  45.0)*
   QP log P  for     octanol/water       =     2.856  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.039  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.555  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.091  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.403  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         2  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -4.021  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      6471  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      3723  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.236  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     9.154  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  3

       5 of   1712 molecules most similar to BC02133.mol:
          Name                           Similarity(%)
Levamisole                                    94.55
Methsuximide                                  87.61
Tacrine                                       86.30
Phensuximide                                  86.11
Fadrozole                                     86.06

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -0.730    H-bond Acceptor     -0.785 
 Volume               4.212    SASA                 7.607 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.135    Rotor Bonds          0.000 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.214    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.856    Total                3.039


                log BB                    log PMDCK
 Hydrophilic SASA    -0.160    Hydrophilic SASA    -0.200 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds          0.000                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.403    Total                3.571

