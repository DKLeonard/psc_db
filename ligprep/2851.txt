 Primary Metabolites & Reactive FGs:
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acceptor carbonyl or derivative
 > Reactive FG: acceptor carbonyl or derivative

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   248.278  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.839  (   1.0 /  12.5) 
     Solute        Total        SASA     =   561.762  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   228.102  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   228.872  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   104.789  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   919.946  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   103.073  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    11.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     4.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.814  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.765  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     1.524  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    24.600M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.412M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    13.022  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     7.783  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.766  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.181  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.637  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.504  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -2.401  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         2  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -1.421  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         4  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         2  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.312  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.008  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        54  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02851.mol:
          Name                           Similarity(%)
Butoctamide                                   75.78
Chloralodol                                   75.39
Tybamate                                      75.15
Unithiol                                      75.01
Tiadenol                                      74.99

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -1.948    H-bond Acceptor     -2.094 
 Volume               6.002    SASA                10.645 
 Ac x Dn^.5/SASA      0.447    Ac x Dn^.5/SASA      1.007 
 FISA                -0.553    Rotor Bonds         -1.791 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.123    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.766    Total                3.181


                log BB                    log PMDCK
 Hydrophilic SASA    -2.301    Hydrophilic SASA    -2.346 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.663                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids     -1.080 
 Constant             0.564    Constant             3.771
 Total               -2.401    Total                0.344

