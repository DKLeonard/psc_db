 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   262.305  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.882  (   1.0 /  12.5) 
     Solute        Total        SASA     =   465.859  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   237.030  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   137.850  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    90.979  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   824.453  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    83.883  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     2.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     5.750  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.913  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.542  (   7.9 /  10.5)*
     Solute Electron Affinity    (eV)    =     0.578  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    26.425M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     7.985M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    13.167M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     9.084M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.281  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.542  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.634  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.261  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.660  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         2  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -3.378  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       488  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       227  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.548  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.214  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        83  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01275.mol:
          Name                           Similarity(%)
Azathioprine                                  89.26
Fenspiride                                    88.47
Emorfazone                                    87.86
Thalidomide                                   86.40
Toloxatone                                    85.75

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -2.800    H-bond Acceptor     -3.011 
 Volume               5.379    SASA                 8.828 
 Ac x Dn^.5/SASA      0.548    Ac x Dn^.5/SASA      1.234 
 FISA                -0.954    Rotor Bonds         -0.326 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.107    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.274    Total                2.541


                log BB                    log PMDCK
 Hydrophilic SASA    -1.103    Hydrophilic SASA    -1.413 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.121                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.660    Total                2.358

