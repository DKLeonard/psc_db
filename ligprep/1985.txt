 Primary Metabolites & Reactive FGs:
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   222.243  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     0.583  (   1.0 /  12.5)*
     Solute        Total        SASA     =   448.618  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =    88.122  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    90.957  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   269.538  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   747.064  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    54.365  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     0.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     4.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.888  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.868  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     1.458  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    26.318M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     7.743M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    10.641  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     7.191  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.907  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.624  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.776  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.283  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.206  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         1  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -4.637  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      1359  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       689  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.246  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     2.995  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        94  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01985.mol:
          Name                           Similarity(%)
Phenindione                                   95.36
Fluindione                                    95.04
Isobromindione                                90.00
Anagrelide                                    89.87
Methaqualone                                  89.15

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -1.948    H-bond Acceptor     -2.094 
 Volume               4.874    SASA                 8.501 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.630    Rotor Bonds          0.000 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.316    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.907    Total                2.624


                log BB                    log PMDCK
 Hydrophilic SASA    -0.770    Hydrophilic SASA    -0.932 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds          0.000                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.206    Total                2.838

