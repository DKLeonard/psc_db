 Primary Metabolites & Reactive FGs:
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   378.424  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.338  (   1.0 /  12.5) 
     Solute        Total        SASA     =   682.812  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   171.787  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   123.253  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   387.772  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1212.157  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    85.313  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    10.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     4.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.805  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.042  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.032  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    39.639M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    13.554M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    18.387M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     9.502M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     4.624  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -5.459  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -6.288  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.632  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.307  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         7  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -6.533  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       671  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       321  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.465  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.045  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01658.mol:
          Name                           Similarity(%)
Cyclovalone                                   85.15
Drotaverine                                   83.53
Carvedilol                                    82.08
Fluvastatin                                   81.71
Bulaquine                                     81.60

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -1.948    H-bond Acceptor     -2.094 
 Volume               7.908    SASA                12.939 
 Ac x Dn^.5/SASA      0.368    Ac x Dn^.5/SASA      0.828 
 FISA                -0.853    Rotor Bonds         -1.628 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.454    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                4.624    Total                5.459


                log BB                    log PMDCK
 Hydrophilic SASA    -1.267    Hydrophilic SASA    -1.263 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.603                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.307    Total                2.507

