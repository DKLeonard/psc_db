 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Reactive FG: acceptor carbonyl or derivative

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   464.684  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.848  (   1.0 /  12.5) 
     Solute        Total        SASA     =   761.028  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   593.961  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   167.067  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =     0.000  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1477.873  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   101.900  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     9.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     4.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     8.800  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.824  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.449  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.740  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    47.212M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    13.958M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    26.340M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    15.228M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     3.321  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -5.271  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.947  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.436  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.617  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.372  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       257  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       114M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.734  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        90  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02542.mol:
          Name                           Similarity(%)
"Estriol                                      81.32
Trimazosin                                    77.62
Pipotiazine                                   77.55
Hydrocortisone                                77.39
Cisapride                                     76.98

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.200    H-bond Donor        -1.606 
 H-bond Acceptor     -4.286    H-bond Acceptor     -4.608 
 Volume               9.642    SASA                14.421 
 Ac x Dn^.5/SASA      1.026    Ac x Dn^.5/SASA      2.312 
 FISA                -1.156    Rotor Bonds         -1.465 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.000    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                3.321    Total                5.271


                log BB                    log PMDCK
 Hydrophilic SASA    -1.638    Hydrophilic SASA    -1.712 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.542                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.617    Total                2.058

