 Primary Metabolites & Reactive FGs:
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   218.338  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.299  (   1.0 /  12.5) 
     Solute        Total        SASA     =   472.284  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   352.185  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    56.885  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    63.213  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   825.256  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    29.566  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     1.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     2.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.901  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.996  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.204  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    26.834M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     6.917M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =     9.716  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     3.422  (   4.0 /  45.0)*
   QP log P  for     octanol/water       =     3.385  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.957  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.928  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.436  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.036  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         4  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -3.437  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      2860  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      1540  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.249  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.136  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01529.mol:
          Name                           Similarity(%)
Epirizole                                     88.07
Cicloxilic                                    86.10
Fencamfamin                                   85.78
Tilbroquinol                                  84.80
Anethole                                      84.60

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -0.974    H-bond Acceptor     -1.047 
 Volume               5.384    SASA                 8.950 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.394    Rotor Bonds         -0.163 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.074    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                3.385    Total                3.957


                log BB                    log PMDCK
 Hydrophilic SASA    -0.467    Hydrophilic SASA    -0.583 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.060                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.036    Total                3.188

