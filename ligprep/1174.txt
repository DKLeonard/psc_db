 Primary Metabolites & Reactive FGs:
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   335.442  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.068  (   1.0 /  12.5) 
     Solute        Total        SASA     =   623.122  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   492.378  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    99.595  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    31.150  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1119.532  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    61.581  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     7.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     5.950  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.837  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.740  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.202  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    34.452M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.991M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    16.918M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     9.354M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.521  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.097  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.978  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.195  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.408  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -5.048  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       280  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       138  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.636  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.006  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        86  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01174.mol:
          Name                           Similarity(%)
Almotriptan                                   86.30
Quinagolide                                   85.98
Methysergide                                  85.05
Tretoquinol                                   84.16
Naratriptan                                   83.71

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -2.898    H-bond Acceptor     -3.115 
 Volume               7.304    SASA                11.808 
 Ac x Dn^.5/SASA      0.599    Ac x Dn^.5/SASA      1.350 
 FISA                -0.689    Rotor Bonds         -1.140 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.036    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.521    Total                3.097


                log BB                    log PMDCK
 Hydrophilic SASA    -0.948    Hydrophilic SASA    -1.021 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.422                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.408    Total                2.142

