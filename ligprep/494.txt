 Primary Metabolites & Reactive FGs:
 > Reactive FG: unhindered ester
 > Metabolism likely: sulfoxide -> sulfone
 > Metabolism likely: alpha hydroxylation of cyclic ether
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   435.479  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     1.611  (   1.0 /  12.5) 
     Solute        Total        SASA     =   659.888  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   228.628  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   357.641  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =    15.953  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =    57.666  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1186.843  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   208.844  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    15.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     5.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    18.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.822  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.251  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     1.147  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    31.864M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    13.419M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    28.031M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    28.112M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -2.243  (  -2.0 /   6.5)*
   QP log S  for   aqueous solubility    =     0.104  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -1.246  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.828  (  -1.5 /   1.5)*
   QP log BB for     brain/blood         =    -3.732  (  -3.0 /   1.2)*
   No. of Primary Metabolites            =         8  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -2.688  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         0  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -6.614  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.135  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         1  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         0  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  5
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00494.mol:
          Name                           Similarity(%)
Lactitol                                      69.73
Lactulose                                     59.88
Limaprost                                     58.76
Voglibose                                     57.33
Valganciclovir                                56.57

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.500    H-bond Donor        -2.008 
 H-bond Acceptor     -8.766    H-bond Acceptor     -9.425 
 Volume               7.743    SASA                12.505 
 Ac x Dn^.5/SASA      2.706    Ac x Dn^.5/SASA      6.097 
 FISA                -1.960    Rotor Bonds         -2.442<
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.239    WPSA                 0.251 
 Constant            -0.705    Constant            -5.083 
 Total               -2.243    Total               -0.104


                log BB                    log PMDCK
 Hydrophilic SASA    -3.533<   Hydrophilic SASA    -3.666<
 WPSA                 0.142    WPSA                 0.316 
 Rotor Bonds         -0.904<                        0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids     -0.540 
 Constant             0.564    Constant             3.771
 Total               -3.732    Total               -0.119

