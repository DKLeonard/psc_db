 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Reactive FG: acetal or analog
 > Reactive FG: acetal or analog

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   594.525  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     7.029  (   1.0 /  12.5) 
     Solute        Total        SASA     =   865.193  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   218.990  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   436.653  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   209.551  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1600.238  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   254.335  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    14.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     8.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =    19.800  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.765  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.968  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.784  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    50.899M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    19.691M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    40.749M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    34.328M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -2.212  (  -2.0 /   6.5)*
   QP log S  for   aqueous solubility    =    -3.224  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.208  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.298  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -5.258  (  -3.0 /   1.2)*
   No. of Primary Metabolites            =         9  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -6.338  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         0  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -7.485  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         0  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  8
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02487.mol:
          Name                           Similarity(%)
Monoxerutin                                   75.03
Troxerutin                                    63.12
Lactulose                                     62.51
Lactitol                                      56.67
Thiamine                                      49.70

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -2.399    H-bond Donor        -3.213 
 H-bond Acceptor     -9.643    H-bond Acceptor    -10.367 
 Volume              10.440    SASA                16.395 
 Ac x Dn^.5/SASA      2.871    Ac x Dn^.5/SASA      6.470 
 FISA                -3.022<   Rotor Bonds         -2.279 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.245    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               -2.212    Total                3.224


                log BB                    log PMDCK
 Hydrophilic SASA    -4.978<   Hydrophilic SASA    -4.476<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.844                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -5.258    Total               -0.705

