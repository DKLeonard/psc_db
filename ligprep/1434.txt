 Primary Metabolites & Reactive FGs:
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: amine dealkylation
 > Metabolism likely: amine dealkylation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   622.760  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.391  (   1.0 /  12.5) 
     Solute        Total        SASA     =   887.636  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   619.569  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    38.394  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   229.673  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1822.477  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    55.686  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    12.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     8.250  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.813  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.492  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.100  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    61.270M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    16.972M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    26.179M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    10.622M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     6.229  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.853  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -8.024  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     1.411  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.250M (  -3.0 /   1.2) 
   No. of Primary Metabolites            =        13  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -7.107  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       266  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       144M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.391M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         2  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        81  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01434.mol:
          Name                           Similarity(%)
Candesartan                                   80.26
Manidipine                                    77.48
Prednimustine                                 67.03
Lidoflazine                                   66.47
Mibefradil                                    66.38

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -4.018    H-bond Acceptor     -4.320 
 Volume              11.890    SASA                16.821 
 Ac x Dn^.5/SASA      0.412    Ac x Dn^.5/SASA      0.929 
 FISA                -0.266    Rotor Bonds         -1.954 
 Non-con amines      -1.054    N Protonation       -2.439 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.269    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                6.229    Total                4.853


                log BB                    log PMDCK
 Hydrophilic SASA    -0.387    Hydrophilic SASA    -0.394 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.723                         0.000 
 N Protonation        0.797    Non-con amines      -1.216 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.250    Total                2.161

