 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Reactive FG: acceptor carbonyl or derivative
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   262.305  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     7.032  (   1.0 /  12.5) 
     Solute        Total        SASA     =   483.622  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   282.059  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   142.913  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    58.650  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   846.228  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    87.663  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     2.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     6.700  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.895  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.555  (   7.9 /  10.5)*
     Solute Electron Affinity    (eV)    =     0.289  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    26.988M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     7.881M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    14.471M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     9.877M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.947  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.531  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.038  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.386  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.747  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -3.516  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       437  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       202  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.755  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.136  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        80  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01296.mol:
          Name                           Similarity(%)
Etozolin                                      83.40
Cinoxacin                                     82.81
Todralazine                                   81.24
Nitazoxanide                                  81.13
Azathioprine                                  80.99

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -3.263    H-bond Acceptor     -3.508 
 Volume               5.521    SASA                 9.165 
 Ac x Dn^.5/SASA      0.615    Ac x Dn^.5/SASA      1.385 
 FISA                -0.989    Rotor Bonds         -0.326 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.069    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.947    Total                2.531


                log BB                    log PMDCK
 Hydrophilic SASA    -1.190    Hydrophilic SASA    -1.465 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.121                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.747    Total                2.306

