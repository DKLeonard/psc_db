 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: unhindered ester
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   306.358  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     7.805  (   1.0 /  12.5) 
     Solute        Total        SASA     =   537.842  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   367.849  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   123.912  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    46.082  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   984.285  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    93.605  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     3.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     6.700  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.890  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.927  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.378  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    31.751M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     8.937M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    16.067M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     9.572M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.903  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.256  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.713  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.089  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.661  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         8  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -3.536  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       661  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       316  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.353  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.075  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        89  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01256.mol:
          Name                           Similarity(%)
Tolazamide                                    88.30
Gliclazide                                    82.73
Azapropazone                                  82.41
Acetohexamide                                 81.45
EPAB                                          80.39

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -3.263    H-bond Acceptor     -3.508 
 Volume               6.421    SASA                10.192 
 Ac x Dn^.5/SASA      0.553    Ac x Dn^.5/SASA      1.245 
 FISA                -0.858    Rotor Bonds         -0.488 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.054    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.903    Total                3.256


                log BB                    log PMDCK
 Hydrophilic SASA    -1.044    Hydrophilic SASA    -1.270 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.181                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.661    Total                2.501

