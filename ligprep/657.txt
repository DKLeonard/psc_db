 Primary Metabolites & Reactive FGs:
 > Reactive FG: unhindered ester
 > Reactive FG: unhindered ester
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: ether dealkylation
 > Metabolism likely: amine dealkylation

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   631.719  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.454  (   1.0 /  12.5) 
     Solute        Total        SASA     =   825.644  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   523.787  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   142.497  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   159.361  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1702.408  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   140.967  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    11.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     3.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    16.950  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.835  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.887  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.205  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    56.435M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    16.536M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    32.766M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    22.422M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.498  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.327  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.433  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.453  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.063M (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.686  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       110M (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        50M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.591M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.076M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         2  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        46  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00657.mol:
          Name                           Similarity(%)
Nicofuranose                                  69.99
Sulfamazone                                   67.44
Penimepicycline                               67.10
Nicomol                                       65.54
Clarithromycin                                64.59

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.900    H-bond Donor        -1.205 
 H-bond Acceptor     -8.255    H-bond Acceptor     -8.875 
 Volume              11.107    SASA                15.646 
 Ac x Dn^.5/SASA      1.577    Ac x Dn^.5/SASA      3.554 
 FISA                -0.986    Rotor Bonds         -1.791 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.187    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.498    Total                2.327


                log BB                    log PMDCK
 Hydrophilic SASA    -1.362    Hydrophilic SASA    -1.461 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.663                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.063    Total                1.702

