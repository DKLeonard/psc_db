 Primary Metabolites & Reactive FGs:
 > Reactive FG: heteroatom in 3-ring
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acetal or analog
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   482.529  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.801  (   1.0 /  12.5) 
     Solute        Total        SASA     =   696.331  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   341.945  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   126.372  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   228.014  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1348.928  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   110.043  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     5.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     8.150  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.848  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.693  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.441  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    46.797M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    13.196M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    20.525M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    10.730M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     3.519  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.331  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -6.021  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.142  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.910  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -5.081  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       627  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       298M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.565  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.061  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00969.mol:
          Name                           Similarity(%)
Triacetyldiphenolisatin                       76.34
Carindacillin                                 75.38
Hydrocortisone                                74.39
Feclobuzone                                   74.25
Buspirone                                     73.64

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -3.969    H-bond Acceptor     -4.267 
 Volume               8.800    SASA                13.195 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.875    Rotor Bonds         -0.814 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.267    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                3.519    Total                4.331


                log BB                    log PMDCK
 Hydrophilic SASA    -1.172    Hydrophilic SASA    -1.295 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.301                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.910    Total                2.475

