 Primary Metabolites & Reactive FGs:
 > Metabolism likely: alpha hydroxylation of cyclic ether
 > Metabolism likely: alpha hydroxylation of cyclic ether
 > Metabolism likely: ether dealkylation
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Reactive FG: acetal or analog
 > Reactive FG: acetal or analog

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   518.516  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     0.835  (   1.0 /  12.5)*
     Solute        Total        SASA     =   746.159  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   390.352  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   161.124  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   194.683  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1421.924  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   141.554  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     8.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     4.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    14.900  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.820  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.946  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.060  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    47.474M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    14.812M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    29.687M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    22.705M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.000  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.639  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.720  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.586  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.518  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         9  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.380  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       293  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       131M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.035  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.110  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         2  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        51  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  2
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02303.mol:
          Name                           Similarity(%)
Idarubicin                                    73.70
Prednisolone                                  71.36
Meproscillarin                                70.97
Colforsin                                     67.20
Sulfamazone                                   66.71

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.200    H-bond Donor        -1.606 
 H-bond Acceptor     -7.256    H-bond Acceptor     -7.802 
 Volume               9.277    SASA                14.140 
 Ac x Dn^.5/SASA      1.772    Ac x Dn^.5/SASA      3.992 
 FISA                -1.115    Rotor Bonds         -1.302 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.228    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.000    Total                3.639


                log BB                    log PMDCK
 Hydrophilic SASA    -1.599    Hydrophilic SASA    -1.652 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.482                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.518    Total                2.119

