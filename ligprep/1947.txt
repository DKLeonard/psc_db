 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Reactive FG: acetal or analog
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   402.357  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.735  (   1.0 /  12.5) 
     Solute        Total        SASA     =   618.112  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   194.705  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   285.723  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   137.684  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1109.734  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   171.582  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     8.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     3.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    10.100  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.839  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.288  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     1.490  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    34.436M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    12.192M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    21.731M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    16.664M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.156  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.964  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.261  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.529  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -2.627  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.800  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        19  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         6  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -5.532  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.001  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        51  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01947.mol:
          Name                           Similarity(%)
Methacycline                                  82.55
Doxycycline                                   79.35
Triamcinolone                                 79.01
Sulfasalazine                                 76.52
Mosapride                                     76.48

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.900    H-bond Donor        -1.205 
 H-bond Acceptor     -4.919    H-bond Acceptor     -5.288 
 Volume               7.240    SASA                11.713 
 Ac x Dn^.5/SASA      1.255    Ac x Dn^.5/SASA      2.829 
 FISA                -1.977    Rotor Bonds         -1.302 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.161    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.156    Total                2.964


                log BB                    log PMDCK
 Hydrophilic SASA    -2.708<   Hydrophilic SASA    -2.929 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.482                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -2.627    Total                0.842

