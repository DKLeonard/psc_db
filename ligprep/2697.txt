 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   456.707  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.391  (   1.0 /  12.5) 
     Solute        Total        SASA     =   694.532  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   579.143  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    97.113  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    18.276  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1401.817  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    62.976  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     2.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.700  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.872  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.532  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.977  (  -0.9 /   1.7)*
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    48.837M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    12.407M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    21.070M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     8.308M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     6.238  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -7.066  (  -6.5 /   0.5)*
   QP log S - conformation independent   =    -6.947  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     1.409  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.408  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -1.880  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       301  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       171M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.053  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         1  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        95  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  2
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02697.mol:
          Name                           Similarity(%)
Acetoxolone                                   82.04
Dutasteride                                   81.39
Mifepristone                                  80.87
Quingestanol                                  78.33
Pentagestrone                                 78.18

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -1.802    H-bond Acceptor     -1.937 
 Volume               9.145    SASA                13.161 
 Ac x Dn^.5/SASA      0.334    Ac x Dn^.5/SASA      0.753 
 FISA                -0.156    Rotor Bonds         -0.326 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.021    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                6.238    Total                7.066


                log BB                    log PMDCK
 Hydrophilic SASA    -0.851    Hydrophilic SASA    -0.995 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.121                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids     -0.540 
 Constant             0.564    Constant             3.771
 Total               -0.408    Total                2.235

