 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: primary alcohol -> acid

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   270.241  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.950  (   1.0 /  12.5) 
     Solute        Total        SASA     =   478.848  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =    50.907  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   220.656  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   207.285  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   807.671  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   115.799  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     4.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     5.200  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.876  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.449  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     1.435  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    25.580M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     8.979M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    13.016M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     9.487M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.929  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.601  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.505  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.312  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.595  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.529  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        80  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        32  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.472  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.023  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        66  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01911.mol:
          Name                           Similarity(%)
Azathioprine                                  92.05
Rizatriptan                                   90.60
Tolcapone                                     88.29
Sulfamethazine                                87.64
Benznidazole                                  87.51

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -2.532    H-bond Acceptor     -2.723 
 Volume               5.269    SASA                 9.074 
 Ac x Dn^.5/SASA      0.482    Ac x Dn^.5/SASA      1.086 
 FISA                -1.527    Rotor Bonds         -0.651 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.243    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.929    Total                2.601


                log BB                    log PMDCK
 Hydrophilic SASA    -1.917    Hydrophilic SASA    -2.262 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.241                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.595    Total                1.509

