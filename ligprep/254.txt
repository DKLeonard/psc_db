 Primary Metabolites & Reactive FGs:
 > Metabolism likely: amine dealkylation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Reactive FG: acetal or analog
 > Reactive FG: acetal or analog

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   353.374  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.530  (   1.0 /  12.5) 
     Solute        Total        SASA     =   519.630  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   309.941  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    31.803  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   177.885  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   978.674  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    59.521  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     0.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     7.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.917  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.839  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.199  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    34.711M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.083M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    15.072M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     9.291M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.732  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.179  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.973  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.413  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.710  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         4  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      ++ 
   HERG K+ Channel Blockage: log IC50    =    -4.443  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      1233  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       686  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.542  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     6.717  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        92  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00254.mol:
          Name                           Similarity(%)
Ketazolam                                     83.14
Piribedil                                     80.05
Acetyldihydrocodeine                          78.42
Ofloxacin                                     77.92
Rufloxacin                                    76.97

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -3.409    H-bond Acceptor     -3.665 
 Volume               6.385    SASA                 9.847 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.220    Rotor Bonds          0.000 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.208    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.732    Total                1.179


                log BB                    log PMDCK
 Hydrophilic SASA    -0.252    Hydrophilic SASA    -0.326 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds          0.000                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.710    Total                2.837

