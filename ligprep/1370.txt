 Primary Metabolites & Reactive FGs:
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: amine dealkylation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   341.406  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.159  (   1.0 /  12.5) 
     Solute        Total        SASA     =   588.277  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   445.965  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    27.082  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   115.230  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1060.440  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    41.605  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     4.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     5.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.855  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.423  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.222  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    34.817M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.324M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    14.946M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     7.610M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     3.276  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.324  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.869  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.373  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.474  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         8  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      ++ 
   HERG K+ Channel Blockage: log IC50    =    -5.156  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      1367  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       767  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.291  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.083  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01370.mol:
          Name                           Similarity(%)
Poldine                                       88.32
Niaprazine                                    87.24
Flutazolam                                    86.20
Reboxetine                                    86.14
Mofezolac                                     86.12

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -2.435    H-bond Acceptor     -2.618 
 Volume               6.918    SASA                11.148 
 Ac x Dn^.5/SASA      0.377    Ac x Dn^.5/SASA      0.850 
 FISA                -0.187    Rotor Bonds         -0.651 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.135    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                3.276    Total                3.324


                log BB                    log PMDCK
 Hydrophilic SASA    -0.247    Hydrophilic SASA    -0.278 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.241                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.474    Total                2.885

