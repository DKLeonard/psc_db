 Primary Metabolites & Reactive FGs:
 > Metabolism likely: para hydroxylation of aryl
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: para hydroxylation of aryl
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: para hydroxylation of aryl
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: para hydroxylation of aryl
 > Reactive FG: acetal or analog
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: acetal or analog
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: primary alcohol -> acid

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   862.750  ( 130.0 / 725.0)*
     Solute        Dipole Moment (D)     =     6.107  (   1.0 /  12.5) 
     Solute        Total        SASA     =   978.170  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   165.308  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   574.456  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   238.406  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  2075.769  ( 500.0 /2000.0)*
     Solute        vdW Polar SA (PSA)    =   378.240  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    18.000  (   0.0 /  15.0)*
     Solute as Donor -    Hydrogen Bonds =    10.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =    26.000  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.805  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.383  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     1.220  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    67.637M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    24.989M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    53.749M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    43.887M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -1.760  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.596M (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -7.233M (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.599M (  -1.5 /   1.5)*
   QP log BB for     brain/blood         =    -6.438M (  -3.0 /   1.2)*
   No. of Primary Metabolites            =        16  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -2.217M (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         0M (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -9.539M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         0M (<25% is poor)
   Qual. Model for Human Oral Absorption =       lowM (>80% is high)

       A * indicates a violation of the 95% range. # stars = 12
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01984.mol:
          Name                           Similarity(%)
Monoxerutin                                   57.57
Troxerutin                                    57.25
Gitoformate                                   43.24
Glycopin                                      41.85
Glucosulfone                                  41.21

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -2.999<   H-bond Donor        -4.016<
 H-bond Acceptor    -12.662    H-bond Acceptor    -13.614<
 Volume              13.542    SASA                18.536 
 Ac x Dn^.5/SASA      3.729    Ac x Dn^.5/SASA      8.402 
 FISA                -2.944<   Rotor Bonds         -2.930<
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.279    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               -1.760    Total                2.596


                log BB                    log PMDCK
 Hydrophilic SASA    -5.917<   Hydrophilic SASA    -5.888<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -1.085<                        0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids     -1.080 
 Constant             0.564    Constant             3.771
 Total               -6.438    Total               -3.198

