 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   252.266  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.485  (   1.0 /  12.5) 
     Solute        Total        SASA     =   492.464  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   268.759  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   193.216  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    30.488  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   838.329  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   113.038  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     7.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.250  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.873  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.464  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.649  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    23.192M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     7.602M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =     9.199M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     4.083M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.880  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.708  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.282  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.115  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.548  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -3.731  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       145  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        61  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.301  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.025  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        77  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02838.mol:
          Name                           Similarity(%)
Alibendol                                     88.46
Carmofur                                      87.92
Isoaminile                                    87.67
Bucetin                                       85.67
Tolbutamide                                   85.52

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -1.583    H-bond Acceptor     -1.702 
 Volume               5.469    SASA                 9.332 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -1.337    Rotor Bonds         -1.140 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.036    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.880    Total                2.708


                log BB                    log PMDCK
 Hydrophilic SASA    -1.690    Hydrophilic SASA    -1.980 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.422                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.548    Total                1.790

