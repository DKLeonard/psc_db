 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   136.150  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     0.654  (   1.0 /  12.5)*
     Solute        Total        SASA     =   340.458  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =    64.604  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   102.004  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   173.850  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   521.717  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    44.242  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     3.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     1.500  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.921  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.723  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.215  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    14.459  (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     5.515  (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =     7.902  (   8.0 /  35.0)*
   QP log P  for     water/gas           =     6.160  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.408  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -0.384  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -1.598  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.496  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.420  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         2  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -3.646M (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      1068  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       531  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.499  (Kp in cm/hr)
   Jm, max transdermal transport rate    =    26.323  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        89  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  3
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00347.mol:
          Name                           Similarity(%)
Phenylpropanol                                94.33
Paroxypropione                                93.44
Phentermine                                   93.00
Gimestat                                      92.79
Metacetamol                                   92.53

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -0.730    H-bond Acceptor     -0.785 
 Volume               3.404    SASA                 6.452 
 Ac x Dn^.5/SASA      0.276    Ac x Dn^.5/SASA      0.623 
 FISA                -0.706    Rotor Bonds         -0.488 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.204    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.142    Total                1.215


                log BB                    log PMDCK
 Hydrophilic SASA    -0.802    Hydrophilic SASA    -1.046 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.181                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.420    Total                2.725

