 Primary Metabolites & Reactive FGs:
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: amine dealkylation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   249.352  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     0.483  (   1.0 /  12.5)*
     Solute        Total        SASA     =   506.919  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   402.263  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    43.104  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    61.552  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   885.765  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    33.253  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     4.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.500  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.880  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.736  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.180  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    27.314M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     7.494M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    11.472  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     5.681  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.622  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.409  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.240  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.198  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.350  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -4.495  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       963  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       526  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.776  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.163  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        96  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01389.mol:
          Name                           Similarity(%)
Tramadol                                      89.25
Budralazine                                   87.86
Albendazole                                   87.81
Loxoprofen                                    87.13
Tolperisone                                   87.02

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -1.704    H-bond Acceptor     -1.833 
 Volume               5.779    SASA                 9.606 
 Ac x Dn^.5/SASA      0.306    Ac x Dn^.5/SASA      0.690 
 FISA                -0.298    Rotor Bonds         -0.651 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.072    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.622    Total                2.409


                log BB                    log PMDCK
 Hydrophilic SASA    -0.371    Hydrophilic SASA    -0.442 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.241                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.350    Total                2.721

