 Primary Metabolites & Reactive FGs:
 > Reactive FG: acetal or analog
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   348.395  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.747  (   1.0 /  12.5) 
     Solute        Total        SASA     =   548.468  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   401.926  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   107.991  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    38.551  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1021.349  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    91.174  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     2.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     6.250  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.894  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.371  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.729  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    33.804M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     8.905M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    14.653M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     7.751M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.212  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.013  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.898  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.174  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.457  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         2  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -3.427  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       937  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       461  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.182  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.223  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        93  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01236.mol:
          Name                           Similarity(%)
Nifenazone                                    86.39
Quinfamide                                    86.26
Repirinast                                    85.89
Hydroxyprogesterone                           85.63
Flunitrazepam                                 85.24

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -3.044    H-bond Acceptor     -3.272 
 Volume               6.663    SASA                10.393 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.747    Rotor Bonds         -0.326 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.045    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.212    Total                3.013


                log BB                    log PMDCK
 Hydrophilic SASA    -0.900    Hydrophilic SASA    -1.107 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.121                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.457    Total                2.664

