 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Reactive FG: unhindered ester
 > Metabolism likely: amine dealkylation
 > Metabolism likely: para hydroxylation of aryl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   630.734  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.995  (   1.0 /  12.5) 
     Solute        Total        SASA     =   895.147  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   581.659  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   178.386  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   135.103  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1772.040  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   153.628  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    12.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     4.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    13.600  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.791  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.802  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.667  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    58.348M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    17.728M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    33.035M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    20.613M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.778  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.317  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.678  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.194  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.661M (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -6.282  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        50  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        21M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -5.242M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         2  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        48  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00644.mol:
          Name                           Similarity(%)
Saquinavir                                    76.09
Indinavir                                     75.11
Bosentan                                      69.88
Penimepicycline                               69.21
Nelfinavir                                    67.74

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.200    H-bond Donor        -1.606 
 H-bond Acceptor     -6.623    H-bond Acceptor     -7.121 
 Volume              11.561    SASA                16.963 
 Ac x Dn^.5/SASA      1.348    Ac x Dn^.5/SASA      3.037 
 FISA                -1.235    Rotor Bonds         -1.954 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.158    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.778    Total                4.317


                log BB                    log PMDCK
 Hydrophilic SASA    -1.900    Hydrophilic SASA    -1.828 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.723                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.661    Total                1.334

