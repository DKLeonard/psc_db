 Primary Metabolites & Reactive FGs:
 > Reactive FG: unhindered ester
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   390.475  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.893  (   1.0 /  12.5) 
     Solute        Total        SASA     =   636.002  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   473.430  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   140.630  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    21.942  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1201.446  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   107.663  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     5.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     3.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     7.200  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.859  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.779  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.466  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    38.927M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    11.223M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    21.040M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    12.738M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.649  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.441  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.083  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.264  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.007  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         7  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -3.912  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       459  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       213  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.554  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.004  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        90  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02833.mol:
          Name                           Similarity(%)
Paramethasone                                 80.25
Cloprednol                                    78.56
Dexamethasone                                 77.96
Betamethasone                                 77.87
Fluprednisolone                               77.83

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.900    H-bond Donor        -1.205 
 H-bond Acceptor     -3.506    H-bond Acceptor     -3.770 
 Volume               7.838    SASA                12.052 
 Ac x Dn^.5/SASA      0.870    Ac x Dn^.5/SASA      1.960 
 FISA                -0.973    Rotor Bonds         -0.814 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.026    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.649    Total                4.441


                log BB                    log PMDCK
 Hydrophilic SASA    -1.270    Hydrophilic SASA    -1.441 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.301                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.007    Total                2.329

