 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   250.337  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.809  (   1.0 /  12.5) 
     Solute        Total        SASA     =   466.772  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   298.520  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   141.954  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    26.298  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   834.055  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    80.323  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     3.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.750  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.918  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.449  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.439  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    25.550M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     7.460M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    10.325  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     5.019  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.959  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.611  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.731  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.091  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.740  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         2  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -2.984  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       446  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       206  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.755  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.108  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        86  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01535.mol:
          Name                           Similarity(%)
Nitrefazole                                   89.88
Aniracetam                                    89.35
Ibudilast                                     87.65
Meperidine                                    87.06
Enoximone                                     87.04

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -1.826    H-bond Acceptor     -1.963 
 Volume               5.441    SASA                 8.845 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.982    Rotor Bonds         -0.488 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.031    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.959    Total                2.611


                log BB                    log PMDCK
 Hydrophilic SASA    -1.123    Hydrophilic SASA    -1.455 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.181                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.740    Total                2.316

