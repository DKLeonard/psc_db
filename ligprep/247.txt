 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Reactive FG: acetal or analog
 > Reactive FG: acceptor carbonyl or derivative

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   326.302  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.680  (   1.0 /  12.5) 
     Solute        Total        SASA     =   570.144  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   122.477  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   291.699  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   155.968  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   981.656  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   149.943  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    10.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     5.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    11.250  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.838  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.204  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.801  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    28.200M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    11.582M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    23.309M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    20.334M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -0.642  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.905  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.041  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.137  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -2.810  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         4  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -3.092  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         4  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         1  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -5.386  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.017  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        35  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00247.mol:
          Name                           Similarity(%)
Valaciclovir                                  79.38
Xamoterol                                     73.73
Sorivudine                                    72.75
Acetosulfone                                  72.11
Valganciclovir                                71.48

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.500    H-bond Donor        -2.008 
 H-bond Acceptor     -5.479    H-bond Acceptor     -5.890 
 Volume               6.404    SASA                10.804 
 Ac x Dn^.5/SASA      1.957    Ac x Dn^.5/SASA      4.410 
 FISA                -1.503    Rotor Bonds         -1.628 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.183    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               -0.642    Total                1.905


                log BB                    log PMDCK
 Hydrophilic SASA    -2.771<   Hydrophilic SASA    -2.990 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.603                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids     -0.540 
 Constant             0.564    Constant             3.771
 Total               -2.810    Total                0.241

