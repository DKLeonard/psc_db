 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: aromatic OH oxidation
 > Reactive FG: acetal or analog
 > Reactive FG: acetal or analog
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   578.526  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.051  (   1.0 /  12.5) 
     Solute        Total        SASA     =   793.450  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   266.299  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   407.851  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   119.299  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1533.190  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   247.047  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    13.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     7.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =    20.550  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.810  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.314  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     1.322  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    47.996M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    17.719M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    38.362M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    32.840M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -2.453  (  -2.0 /   6.5)*
   QP log S  for   aqueous solubility    =    -2.415  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.601  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.301  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -4.360  (  -3.0 /   1.2)*
   No. of Primary Metabolites            =         9  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.169  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         1  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -7.368  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         0  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  8
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02653.mol:
          Name                           Similarity(%)
Monoxerutin                                   67.49
Lactulose                                     62.02
Troxerutin                                    57.81
Lactitol                                      52.18
Penimepicycline                               52.06

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -2.099    H-bond Donor        -2.811 
 H-bond Acceptor    -10.008    H-bond Acceptor    -10.760 
 Volume              10.003    SASA                15.036 
 Ac x Dn^.5/SASA      3.040    Ac x Dn^.5/SASA      6.850 
 FISA                -2.823    Rotor Bonds         -2.116 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.140    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               -2.453    Total                2.415


                log BB                    log PMDCK
 Hydrophilic SASA    -4.140<   Hydrophilic SASA    -4.180<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.783                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -4.360    Total               -0.410

