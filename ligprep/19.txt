 Primary Metabolites & Reactive FGs:
 > Reactive FG: acceptor carbonyl or derivative

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   148.161  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.007  (   1.0 /  12.5) 
     Solute        Total        SASA     =   365.751  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =    23.621  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   114.689  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   227.440  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   564.779  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    51.618  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     3.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     2.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.903  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.460  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.881  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    16.694  (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     6.054  (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =     7.826  (   8.0 /  35.0)*
   QP log P  for     water/gas           =     5.699  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.902  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.631  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -1.776  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.514  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.554  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         0  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -2.372M (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       205  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       113  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.544  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     7.399  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        79  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  2
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00019.mol:
          Name                           Similarity(%)
Phenylpropanol                                89.15
Sodium                                        88.96
Paroxypropione                                88.31
Clofibric                                     87.18
Isaxonine                                     86.51

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -0.974    H-bond Acceptor     -1.047 
 Volume               3.685    SASA                 6.931 
 Ac x Dn^.5/SASA      0.243    Ac x Dn^.5/SASA      0.547 
 FISA                -0.278    Rotor Bonds         -0.488 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.266    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.937    Total                1.758


                log BB                    log PMDCK
 Hydrophilic SASA    -0.937    Hydrophilic SASA    -1.176 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.181                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids     -0.540 
 Constant             0.564    Constant             3.771
 Total               -0.554    Total                2.055

