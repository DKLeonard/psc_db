 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: ether dealkylation
 > Metabolism likely: amine dealkylation
 > Reactive FG: acetal or analog

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   479.612  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.836  (   1.0 /  12.5) 
     Solute        Total        SASA     =   705.157  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   682.962  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    22.195  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =     0.000  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1374.024  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    64.424  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     7.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    12.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.848  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.841  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -2.150  (  -0.9 /   1.7)*
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    44.340M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    11.300M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    22.123M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    13.307M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.190  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.237  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.140  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.347  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.334  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -4.724  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      1521  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       861M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.319  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     1.333  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        97  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00635.mol:
          Name                           Similarity(%)
Sildenafil                                    89.68
Vardenafil                                    87.10
Cinnoxicam                                    82.45
Tritoqualine                                  80.20
Deflazacort                                   76.36

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -5.844    H-bond Acceptor     -6.283 
 Volume               8.964    SASA                13.363 
 Ac x Dn^.5/SASA      0.755    Ac x Dn^.5/SASA      1.701 
 FISA                -0.154    Rotor Bonds         -1.140 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.000    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.190    Total                2.237


                log BB                    log PMDCK
 Hydrophilic SASA    -0.206    Hydrophilic SASA    -0.227 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.422                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000<   COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.334    Total                2.935

