 Primary Metabolites & Reactive FGs:
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol
 > Reactive FG: acceptor carbonyl or derivative
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   354.402  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.125  (   1.0 /  12.5) 
     Solute        Total        SASA     =   694.398  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   304.177  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   160.860  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   229.361  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1198.467  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    92.961  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    10.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     4.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.786  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.772  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.493  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    37.578M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    12.608M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    17.615M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     8.708M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     4.083  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -5.665  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.443  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.586  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.776  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         7  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -6.092  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       295  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       132  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.716  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.001  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        95  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02620.mol:
          Name                           Similarity(%)
Bevantolol                                    89.29
Fenalcomine                                   86.18
Seratrodast                                   83.19
Tiocarlide                                    82.83
Cinnamaverine                                 82.83

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -1.948    H-bond Acceptor     -2.094 
 Volume               7.819    SASA                13.159 
 Ac x Dn^.5/SASA      0.361    Ac x Dn^.5/SASA      0.814 
 FISA                -1.113    Rotor Bonds         -1.628 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.269    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                4.083    Total                5.665


                log BB                    log PMDCK
 Hydrophilic SASA    -1.737    Hydrophilic SASA    -1.649 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.603                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.776    Total                2.122

