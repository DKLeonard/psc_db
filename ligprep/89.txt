 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   240.215  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     0.941  (   1.0 /  12.5)*
     Solute        Total        SASA     =   436.921  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =     0.000  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   174.945  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   261.975  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   727.677  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    94.704  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     2.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     4.500  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.895  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.413  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     1.448  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    24.185M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     8.371M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    11.983  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     9.198  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.104  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.443  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.180  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.296  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.012  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         2  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.473  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       217  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        94  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.629  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.204  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        75  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00089.mol:
          Name                           Similarity(%)
Azathioprine                                  91.35
Nitrefazole                                   91.01
Tizanidine                                    89.18
Fenozolone                                    88.83
Anisindione                                   88.43

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -2.192    H-bond Acceptor     -2.356 
 Volume               4.747    SASA                 8.280 
 Ac x Dn^.5/SASA      0.457    Ac x Dn^.5/SASA      1.030 
 FISA                -1.211    Rotor Bonds         -0.326 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.307    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.104    Total                2.443


                log BB                    log PMDCK
 Hydrophilic SASA    -1.455    Hydrophilic SASA    -1.793 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.121                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.012    Total                1.978

