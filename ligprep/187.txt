 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   302.240  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.387  (   1.0 /  12.5) 
     Solute        Total        SASA     =   529.400  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =     0.000  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   274.772  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   254.627  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   885.467  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   139.124  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     5.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     4.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     5.250  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.842  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.671  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.777  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    28.504M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    10.987M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    18.807M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    14.521M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.592  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.063  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.043  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.307  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -2.320  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.297  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        24  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         9  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -5.207  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.002  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        55  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00187.mol:
          Name                           Similarity(%)
Papaveroline                                  90.09
Aminaftone                                    88.44
Azosemide                                     87.20
Trimethoprim                                  85.71
Chlorthalidone                                83.58

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.200    H-bond Donor        -1.606 
 H-bond Acceptor     -2.557    H-bond Acceptor     -2.749 
 Volume               5.777    SASA                10.032 
 Ac x Dn^.5/SASA      0.880    Ac x Dn^.5/SASA      1.983 
 FISA                -1.902    Rotor Bonds         -0.814 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.298    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.592    Total                3.063


                log BB                    log PMDCK
 Hydrophilic SASA    -2.582<   Hydrophilic SASA    -2.816 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.301                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -2.320    Total                0.954

