 Primary Metabolites & Reactive FGs:
 > Reactive FG: unhindered ester
 > Reactive FG: unhindered ester
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   356.418  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.433  (   1.0 /  12.5) 
     Solute        Total        SASA     =   655.476  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   436.000  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    84.237  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   135.239  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1183.299  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    75.173  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     4.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     5.250  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.825  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.049  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.850  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    39.925M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    10.756M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    16.082M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     7.078M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     3.932  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -5.238  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.857  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.479  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.502  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         1  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -5.002  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      1574  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       807  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.212  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.013  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01152.mol:
          Name                           Similarity(%)
Nitrendipine                                  90.63
Repirinast                                    88.33
Ethyl                                         87.10
Nifedipine                                    86.71
Nisoldipine                                   86.70

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -2.557    H-bond Acceptor     -2.749 
 Volume               7.720    SASA                12.421 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.583    Rotor Bonds         -0.651 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.158    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                4.034    Total                5.238


                log BB                    log PMDCK
 Hydrophilic SASA    -0.824    Hydrophilic SASA    -0.863 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.241                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.502    Total                2.907

