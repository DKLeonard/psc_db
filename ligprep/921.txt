 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   206.198  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.525  (   1.0 /  12.5) 
     Solute        Total        SASA     =   407.765  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   126.350  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   131.132  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   150.283  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   665.943  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    76.617  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     3.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.904  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.281  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.420  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    20.006M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     6.713M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =     9.694  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     6.372  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.474  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.219  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.811  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.244  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.686  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -3.795  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       565  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       267  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.119  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.947  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        85  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00921.mol:
          Name                           Similarity(%)
Nitroxoline                                   94.76
Amezinium                                     92.68
Metaxalone                                    91.87
Fenozolone                                    90.01
Ethotoin                                      89.81

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -1.461    H-bond Acceptor     -1.571 
 Volume               4.345    SASA                 7.727 
 Ac x Dn^.5/SASA      0.326    Ac x Dn^.5/SASA      0.735 
 FISA                -0.908    Rotor Bonds         -0.488 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.176    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.474    Total                2.219


                log BB                    log PMDCK
 Hydrophilic SASA    -1.069    Hydrophilic SASA    -1.344 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.181                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.686    Total                2.427

