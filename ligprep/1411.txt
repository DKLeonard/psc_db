 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: ether dealkylation
 > Metabolism likely: amine dealkylation
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   299.369  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     1.920  (   1.0 /  12.5) 
     Solute        Total        SASA     =   509.456  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   357.012  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    35.530  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   116.914  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   915.691  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    39.060  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     2.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     5.200  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.895  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.721  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.200  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    30.324M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     8.201M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    13.667M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     8.127M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.369  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.895  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.751  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.058  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.546  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      ++ 
   HERG K+ Channel Blockage: log IC50    =    -4.512  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      1137  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       628  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.633  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.418  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        90  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01411.mol:
          Name                           Similarity(%)
Granisetron                                   92.13
Nalorphine                                    91.53
Metopon                                       90.67
Vinburnine                                    89.39
Molindone                                     88.77

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -2.532    H-bond Acceptor     -2.723 
 Volume               5.974    SASA                 9.654 
 Ac x Dn^.5/SASA      0.453    Ac x Dn^.5/SASA      1.020 
 FISA                -0.246    Rotor Bonds         -0.326 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.137    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.254    Total                2.222


                log BB                    log PMDCK
 Hydrophilic SASA    -0.296    Hydrophilic SASA    -0.364 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.121                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.546    Total                2.799

BC01412.mol                             
 Has an odd number of electrons or is charged 183 - molecule not processed
