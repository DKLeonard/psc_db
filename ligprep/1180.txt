 Primary Metabolites & Reactive FGs:
 > Reactive FG: heteroatom in 3-ring
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   280.320  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.982  (   1.0 /  12.5) 
     Solute        Total        SASA     =   433.715  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   289.129  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   121.417  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    23.169  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   809.745  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    90.634  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     3.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     8.400  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.969  (  0.75 /  0.95)*
     Solute Ionization Potential (eV)    =    10.506  (   7.9 /  10.5)*
     Solute Electron Affinity    (eV)    =     0.068  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    24.547M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     7.925M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    15.984M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    12.288M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.289  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.484  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -1.589  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.587  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.480  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -2.149  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       699  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       335  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.388  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     3.767  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        80  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  2
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01180.mol:
          Name                           Similarity(%)
Fropenem                                      81.80
Sulfasomidine                                 79.20
Sulfamoxole                                   78.74
Sulfamethazine                                78.74
Acetylsulfisoxazole                           78.36

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -4.091    H-bond Acceptor     -4.398 
 Volume               5.283    SASA                 8.219 
 Ac x Dn^.5/SASA      1.215    Ac x Dn^.5/SASA      2.738 
 FISA                -0.840    Rotor Bonds         -0.488 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.027    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.289    Total                1.484


                log BB                    log PMDCK
 Hydrophilic SASA    -0.863    Hydrophilic SASA    -1.245 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.181                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.480    Total                2.526

