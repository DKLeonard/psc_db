 Primary Metabolites & Reactive FGs:
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: amine dealkylation
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: para hydroxylation of aryl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   385.459  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     7.048  (   1.0 /  12.5) 
     Solute        Total        SASA     =   606.124  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   478.279  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    63.540  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    64.306  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1154.700  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    59.229  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     4.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     7.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.878  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.149  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.517  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    38.103M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.915M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    16.465M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     8.060M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.528  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.167  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.534  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.072  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.172  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         8  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -4.536  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       616  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       324  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.143  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.189  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        92  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02634.mol:
          Name                           Similarity(%)
Sulforidazine                                 85.42
Fedrilate                                     84.27
Mesoridazine                                  84.13
Flutazolam                                    83.56
Fonazine                                      82.78

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -3.409    H-bond Acceptor     -3.665 
 Volume               7.533    SASA                11.486 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.440    Rotor Bonds         -0.651 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.075    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.528    Total                2.167


                log BB                    log PMDCK
 Hydrophilic SASA    -0.549    Hydrophilic SASA    -0.651 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.241                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.172    Total                2.512

