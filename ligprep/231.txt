 Primary Metabolites & Reactive FGs:
 > Metabolism likely: thiophene S -> S=O
 > Reactive FG: phosphonate or relative
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   223.183  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.889  (   1.0 /  12.5) 
     Solute        Total        SASA     =   398.203  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   129.677  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   155.471  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    62.095  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =    50.960  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   653.561  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    82.713  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     6.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     6.500  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.915  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.757  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.725  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    16.742M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     6.812M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    11.941  (   8.0 /  35.0) 
   QP log P  for     water/gas           =    10.183  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.040  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.109  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -1.696  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.169  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.912  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =     0.406  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        21  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        23  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.590  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     4.459  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        57  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00231.mol:
          Name                           Similarity(%)
Cinametic                                     83.60
Mephenesin                                    83.35
Mephenesin                                    83.31
Erdosteine                                    83.14
Guaifenesin                                   82.78

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -3.165    H-bond Acceptor     -3.403 
 Volume               4.264    SASA                 7.546 
 Ac x Dn^.5/SASA      1.024    Ac x Dn^.5/SASA      2.308 
 FISA                -0.045    Rotor Bonds         -0.977 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.267    WPSA                 0.222 
 Constant            -0.705    Constant            -3.783 
 Total                1.040    Total                1.109


                log BB                    log PMDCK
 Hydrophilic SASA    -1.239    Hydrophilic SASA    -1.594 
 WPSA                 0.125    WPSA                 0.279 
 Rotor Bonds         -0.362                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids     -1.080 
 Constant             0.564    Constant             3.771
 Total               -0.912    Total                1.376

