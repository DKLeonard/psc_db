 Primary Metabolites & Reactive FGs:
 > Reactive FG: unhindered ester
 > Metabolism likely: alpha hydroxylation of cyclic ether
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   425.425  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     8.009  (   1.0 /  12.5) 
     Solute        Total        SASA     =   622.857  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   114.922  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   367.823  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   110.493  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =    29.619  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1126.988  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   210.190  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    14.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     6.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    14.750  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.841  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.158  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     1.249  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    31.014M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    13.634M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    28.336M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    24.710M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -1.550  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.536  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.509  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.404  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -3.676  (  -3.0 /   1.2)*
   No. of Primary Metabolites            =         7  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -2.827  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         0  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -6.565  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.003  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         2  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         0  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  3
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00497.mol:
          Name                           Similarity(%)
Lisinopril                                    67.94
Lactulose                                     67.84
Valganciclovir                                67.60
Sulfoxone                                     67.36
Thiamine                                      66.92

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.799    H-bond Donor        -2.410 
 H-bond Acceptor     -7.183    H-bond Acceptor     -7.723 
 Volume               7.352    SASA                11.803 
 Ac x Dn^.5/SASA      2.573    Ac x Dn^.5/SASA      5.798 
 FISA                -2.030    Rotor Bonds         -2.279 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.242    WPSA                 0.129 
 Constant            -0.705    Constant            -3.783 
 Total               -1.550    Total                1.536


                log BB                    log PMDCK
 Hydrophilic SASA    -3.468<   Hydrophilic SASA    -3.770<
 WPSA                 0.073    WPSA                 0.162 
 Rotor Bonds         -0.844                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids     -0.540 
 Constant             0.564    Constant             3.771
 Total               -3.676    Total               -0.377

