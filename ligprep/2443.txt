 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   274.273  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.547  (   1.0 /  12.5) 
     Solute        Total        SASA     =   504.721  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =    61.879  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   191.631  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   251.211  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   851.452  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    93.342  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     4.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     4.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     4.700  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.861  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.028  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.076  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    27.752M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    10.119M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    17.718M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    13.552M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.156  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.925  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.499  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.269  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.402  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.983  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       150  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        64  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.782  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.054  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        73  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02443.mol:
          Name                           Similarity(%)
Papaveroline                                  88.03
Aminaftone                                    82.11
Endralazine                                   81.77
Tolcapone                                     81.56
Trimethoprim                                  81.35

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.200    H-bond Donor        -1.606 
 H-bond Acceptor     -2.289    H-bond Acceptor     -2.461 
 Volume               5.555    SASA                 9.564 
 Ac x Dn^.5/SASA      0.826    Ac x Dn^.5/SASA      1.862 
 FISA                -1.326    Rotor Bonds         -0.651 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.294    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.156    Total                2.925


                log BB                    log PMDCK
 Hydrophilic SASA    -1.724    Hydrophilic SASA    -1.964 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.241                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.402    Total                1.807

