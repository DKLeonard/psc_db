 Primary Metabolites & Reactive FGs:
 > Reactive FG: heteroatom in 3-ring
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: low IP - easily oxidized

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   600.880  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.866  (   1.0 /  12.5) 
     Solute        Total        SASA     =  1124.234  ( 300.0 /1000.0)*
     Solute        Hydrophobic  SASA     =   878.886  (   0.0 / 750.0)*
     Solute        Hydrophilic  SASA     =   125.480  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   119.869  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  2141.322  ( 500.0 /2000.0)*
     Solute        vdW Polar SA (PSA)    =    72.913  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    21.000  (   0.0 /  15.0)*
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     5.150  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.715  (  0.75 /  0.95)*
     Solute Ionization Potential (eV)    =     7.970  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.955  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    67.202M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    21.077M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    27.454M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     7.463M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     9.717  (  -2.0 /   6.5)*
   QP log S  for   aqueous solubility    =   -11.250  (  -6.5 /   0.5)*
   QP log S - conformation independent   =    -9.217  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     2.476  (  -1.5 /   1.5)*
   QP log BB for     brain/blood         =    -2.340  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         8  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -6.721  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       639  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       305M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.394M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         2  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  9
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02506.mol:
          Name                           Similarity(%)
Montelukast                                   62.90
Azelnidipine                                  61.86
Tocopherol                                    61.15
Indometacin                                   58.29
Tocopherol                                    58.02

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -2.508    H-bond Acceptor     -2.697 
 Volume              13.970    SASA                21.304<
 Ac x Dn^.5/SASA      0.287    Ac x Dn^.5/SASA      0.648 
 FISA                -0.868    Rotor Bonds         -3.419<
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.140    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                9.717    Total               11.250


                log BB                    log PMDCK
 Hydrophilic SASA    -1.638    Hydrophilic SASA    -1.286 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -1.266<                        0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000<   COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -2.340    Total                2.485

