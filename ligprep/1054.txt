 Primary Metabolites & Reactive FGs:
 > Reactive FG: unhindered ester
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Reactive FG: unhindered ester
 > Metabolism likely: secondary alcohol -> ketone

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   454.559  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.848  (   1.0 /  12.5) 
     Solute        Total        SASA     =   646.553  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   499.774  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   146.779  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =     0.000  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1289.364  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   131.138  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     6.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     4.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     7.950  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.886  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.713  (   7.9 /  10.5)*
     Solute Electron Affinity    (eV)    =    -0.987  (  -0.9 /   1.7)*
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    41.594M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    12.143M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    24.122M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    14.639M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.711  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.182  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.815  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.307  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.044  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         4  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -3.375  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       401  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       184M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.649  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.007  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        89  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  2
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01054.mol:
          Name                           Similarity(%)
Paramethasone                                 82.36
Trimazosin                                    81.62
Triamcinolone                                 78.42
Fludrocortisone                               77.76
Paramethasone                                 76.81

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.200    H-bond Donor        -1.606 
 H-bond Acceptor     -3.872    H-bond Acceptor     -4.163 
 Volume               8.412    SASA                12.252 
 Ac x Dn^.5/SASA      1.091    Ac x Dn^.5/SASA      2.458 
 FISA                -1.016    Rotor Bonds         -0.977 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.000    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.711    Total                4.182


                log BB                    log PMDCK
 Hydrophilic SASA    -1.246    Hydrophilic SASA    -1.504 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.362                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.044    Total                2.266

