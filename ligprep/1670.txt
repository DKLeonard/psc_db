 Primary Metabolites & Reactive FGs:
 > Metabolism likely: alpha hydroxylation of cyclic ether
 > Metabolism likely: alpha hydroxylation of cyclic ether
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   580.498  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     9.737  (   1.0 /  12.5) 
     Solute        Total        SASA     =   783.077  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   196.279  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   437.737  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   149.062  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1496.453  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   267.869  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    12.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =    10.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =    19.800  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.808  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.034  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.781  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    47.452M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    18.509M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    43.053M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    37.097M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -2.892  (  -2.0 /   6.5)*
   QP log S  for   aqueous solubility    =    -2.712  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.833  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.223  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -4.630  (  -3.0 /   1.2)*
   No. of Primary Metabolites            =        15  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.350  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         0  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -7.910  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         0  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  8
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01670.mol:
          Name                           Similarity(%)
Monoxerutin                                   66.30
Troxerutin                                    56.61
Lactulose                                     56.02
Penimepicycline                               54.25
Lactitol                                      51.18

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -2.999<   H-bond Donor        -4.016<
 H-bond Acceptor     -9.643    H-bond Acceptor    -10.367 
 Volume               9.763    SASA                14.839 
 Ac x Dn^.5/SASA      3.547    Ac x Dn^.5/SASA      7.993 
 FISA                -3.030<   Rotor Bonds         -1.954 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.175    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               -2.892    Total                2.712


                log BB                    log PMDCK
 Hydrophilic SASA    -4.470<   Hydrophilic SASA    -4.487<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.723                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -4.630    Total               -0.716

