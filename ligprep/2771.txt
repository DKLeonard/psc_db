 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   414.713  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.392  (   1.0 /  12.5) 
     Solute        Total        SASA     =   766.449  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   687.552  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    48.932  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    29.965  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1473.694  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    22.287  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     7.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     1.700  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.817  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.569  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.986  (  -0.9 /   1.7)*
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    48.615M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    12.645M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    17.866M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     3.695M (   4.0 /  45.0)*
   QP log P  for     octanol/water       =     7.576  (  -2.0 /   6.5)*
   QP log S  for   aqueous solubility    =    -8.532  (  -6.5 /   0.5)*
   QP log S - conformation independent   =    -7.033  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     2.056  (  -1.5 /   1.5)*
   QP log BB for     brain/blood         =    -0.347  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -4.634  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      3403  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      1858  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.644  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         1  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  6
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02771.mol:
          Name                           Similarity(%)
Norethindrone                                 85.63
Chlorotrianisene                              83.05
Estradiol                                     82.87
Toremifene                                    80.91
Testosterone                                  79.94

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -0.828    H-bond Acceptor     -0.890 
 Volume               9.614    SASA                14.524 
 Ac x Dn^.5/SASA      0.098    Ac x Dn^.5/SASA      0.222 
 FISA                -0.339    Rotor Bonds         -1.140 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.035    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                7.576    Total                8.532


                log BB                    log PMDCK
 Hydrophilic SASA    -0.489    Hydrophilic SASA    -0.502 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.422                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000<   COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.347    Total                3.269

