 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Reactive FG: acetal or analog

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   448.382  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =    11.589  (   1.0 /  12.5) 
     Solute        Total        SASA     =   648.227  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   104.127  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   338.086  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   206.013  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1196.021  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   194.029  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     9.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     6.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    12.050  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.841  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.062  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.907  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    37.899M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    14.529M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    30.099M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    23.652M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -0.649  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.868  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.380  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.649  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -3.169  (  -3.0 /   1.2)*
   No. of Primary Metabolites            =         7  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.093  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         6  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         2M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -6.161  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         2  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        11  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  2
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00122.mol:
          Name                           Similarity(%)
Idarubicin                                    74.43
Methacycline                                  70.31
Aminopterin                                   68.73
Methotrexate                                  66.74
Cefatrizine                                   66.41

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.799    H-bond Donor        -2.410 
 H-bond Acceptor     -5.868    H-bond Acceptor     -6.309 
 Volume               7.803    SASA                12.284 
 Ac x Dn^.5/SASA      2.020    Ac x Dn^.5/SASA      4.552 
 FISA                -2.340    Rotor Bonds         -1.465 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.241    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               -0.649    Total                2.868


                log BB                    log PMDCK
 Hydrophilic SASA    -3.190<   Hydrophilic SASA    -3.465<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.542                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -3.169    Total                0.305

