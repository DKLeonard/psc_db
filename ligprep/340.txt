 Primary Metabolites & Reactive FGs:
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: pyridine C2 hydroxylation

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =    83.133  ( 130.0 / 725.0)*
     Solute        Dipole Moment (D)     =     2.218  (   1.0 /  12.5) 
     Solute        Total        SASA     =   279.203  ( 300.0 /1000.0)*
     Solute        Hydrophobic  SASA     =   205.803  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    53.193  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    20.207  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   408.782  ( 500.0 /2000.0)*
     Solute        vdW Polar SA (PSA)    =    31.451  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     0.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     1.500  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.954  (  0.75 /  0.95)*
     Solute Ionization Potential (eV)    =     9.824  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -1.023  (  -0.9 /   1.7)*
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    10.398  (  13.0 /  70.0)*
   QP log P  for     hexadecane/gas      =     2.750  (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =     3.396  (   8.0 /  35.0)*
   QP log P  for     water/gas           =     2.368  (   4.0 /  45.0)*
   QP log P  for     octanol/water       =     0.760  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =     0.075  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -0.243  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.597M (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.174  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         2  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -2.372M (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      3100  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      1681M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.429  (Kp in cm/hr)
   Jm, max transdermal transport rate    =    58.672  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        94  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars = 10
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00340.mol:
          Name                           Similarity(%)
Fomepizole                                    89.10
Methimazole                                   82.65
Nicotinyl                                     82.58
Clomethiazole                                 82.02
Pempidine                                     81.39

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -0.730    H-bond Acceptor     -0.785 
 Volume               2.667    SASA                 5.291 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.368    Rotor Bonds          0.000 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.024    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.887    Total                0.723


                log BB                    log PMDCK
 Hydrophilic SASA    -0.390    Hydrophilic SASA    -0.545 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds          0.000                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.174    Total                3.226

