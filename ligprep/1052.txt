 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   364.438  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.638  (   1.0 /  12.5) 
     Solute        Total        SASA     =   579.388  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   353.907  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   184.870  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    40.612  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1100.482  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   117.304  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     7.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     5.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     7.650  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.890  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.042  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.465  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    33.780M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    11.292M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    22.549M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    15.961M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.327  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.995  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.462  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.174  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.415  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =        10  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -3.510  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       174  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        75  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.112  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.029  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        75  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01052.mol:
          Name                           Similarity(%)
Bumetanide                                    78.56
Pyritinol                                     78.32
Ebrotidine                                    77.62
Phentetramine                                 76.33
Prednisolone                                  76.23

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.500    H-bond Donor        -2.008 
 H-bond Acceptor     -3.726    H-bond Acceptor     -4.006 
 Volume               7.180    SASA                10.979 
 Ac x Dn^.5/SASA      1.310    Ac x Dn^.5/SASA      2.951 
 FISA                -1.279    Rotor Bonds         -1.140 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.048    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.327    Total                2.995


                log BB                    log PMDCK
 Hydrophilic SASA    -1.557    Hydrophilic SASA    -1.895 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.422                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.415    Total                1.876

