 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   310.306  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     1.868  (   1.0 /  12.5) 
     Solute        Total        SASA     =   545.703  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   152.193  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   128.909  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   264.601  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   941.919  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    81.560  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     2.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.750  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.852  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.801  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.924  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    32.784M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.946M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    14.321M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     8.426M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     3.037  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.554  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.028  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.378  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.742  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         2  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -5.196  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       593  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       281  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.771  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.015  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        94  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01699.mol:
          Name                           Similarity(%)
Alosetron                                     93.86
Oxandrolone                                   89.12
Pinazepam                                     88.99
Ondansetron                                   88.84
Oxaprozin                                     88.70

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -1.826    H-bond Acceptor     -1.963 
 Volume               6.145    SASA                10.341 
 Ac x Dn^.5/SASA      0.305    Ac x Dn^.5/SASA      0.687 
 FISA                -0.892    Rotor Bonds         -0.326 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.310    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                3.037    Total                4.554


                log BB                    log PMDCK
 Hydrophilic SASA    -1.185    Hydrophilic SASA    -1.321 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.121                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.742    Total                2.449

