 Primary Metabolites & Reactive FGs:
 > Reactive FG: heteroatom in 3-ring
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acceptor carbonyl or derivative
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   362.422  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.569  (   1.0 /  12.5) 
     Solute        Total        SASA     =   546.331  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   396.207  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   103.448  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    46.676  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1079.721  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    93.739  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     4.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     8.700  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.932  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.101  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.319  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    34.934M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.854M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    17.707M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    11.207M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.848  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.554  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.899  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.239  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.472  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -2.861  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      1034  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       513  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.878  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     1.342  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        92  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01298.mol:
          Name                           Similarity(%)
Cortisone                                     80.80
Scopolamine                                   79.06
Prednisone                                    78.20
Meprednisone                                  77.51
Meprednisone                                  77.15

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -4.237    H-bond Acceptor     -4.555 
 Volume               7.044    SASA                10.353 
 Ac x Dn^.5/SASA      0.706    Ac x Dn^.5/SASA      1.592 
 FISA                -0.716    Rotor Bonds         -0.651 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.055    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.848    Total                2.554


                log BB                    log PMDCK
 Hydrophilic SASA    -0.795    Hydrophilic SASA    -1.060 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.241                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.472    Total                2.710

