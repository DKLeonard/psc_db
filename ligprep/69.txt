 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   462.356  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     9.766  (   1.0 /  12.5) 
     Solute        Total        SASA     =   611.956  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =     0.000  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   411.632  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   198.265  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     2.059  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1095.914  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   232.349  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=     9.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     4.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    12.750  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.840  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.479  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     1.499  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    33.819M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    13.620M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    26.121M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    21.773M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -0.693  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.241  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.466  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.247  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -3.863  (  -3.0 /   1.2)*
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -1.451  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         0  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -7.543  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         1  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         0  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  3
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00069.mol:
          Name                           Similarity(%)
Picosulfuric                                  78.03
Methotrexate                                  77.81
Aminopterin                                   77.39
Sulfoxone                                     76.74
Theodrenaline                                 75.30

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.200    H-bond Donor        -1.606 
 H-bond Acceptor     -6.209    H-bond Acceptor     -6.676 
 Volume               7.150    SASA                11.597 
 Ac x Dn^.5/SASA      1.848    Ac x Dn^.5/SASA      4.165 
 FISA                -1.818    Rotor Bonds         -1.465 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.240    WPSA                 0.009 
 Constant            -0.705    Constant            -3.783 
 Total               -0.693    Total                2.241


                log BB                    log PMDCK
 Hydrophilic SASA    -3.889<   Hydrophilic SASA    -4.219<
 WPSA                 0.005    WPSA                 0.011 
 Rotor Bonds         -0.542                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids     -1.080 
 Constant             0.564    Constant             3.771
 Total               -3.863    Total               -1.518

