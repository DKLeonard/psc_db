 Primary Metabolites & Reactive FGs:
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   486.523  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.650  (   1.0 /  12.5) 
     Solute        Total        SASA     =   714.384  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   415.448  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    48.092  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   250.844  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1368.346  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    83.526  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     3.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     7.500  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.834  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.427  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.787  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    49.076M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    13.220M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    21.694M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    11.662M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     4.696  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -5.987  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -7.153  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.727  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.078  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -5.408  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      3466  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      1896M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.234  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.029  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02159.mol:
          Name                           Similarity(%)
Mizolastine                                   76.09
Vesnarinone                                   74.72
Ziprasidone                                   73.56
Aripiprazole                                  72.71
Irbesartan                                    72.55

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -3.652    H-bond Acceptor     -3.927 
 Volume               8.927    SASA                13.538 
 Ac x Dn^.5/SASA      0.466    Ac x Dn^.5/SASA      1.049 
 FISA                -0.333    Rotor Bonds         -0.488 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.294    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                4.696    Total                5.987


                log BB                    log PMDCK
 Hydrophilic SASA    -0.461    Hydrophilic SASA    -0.493 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.181                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.078    Total                3.278

