 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Reactive FG: unhindered ester
 > Reactive FG: unhindered ester
 > Reactive FG: unhindered ester
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   620.692  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.938  (   1.0 /  12.5) 
     Solute        Total        SASA     =   860.631  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   478.879  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   304.615  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    77.137  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1723.064  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   222.613  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    12.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     4.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    15.050  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.808  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.640  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.600  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    55.834M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    17.663M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    33.352M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    22.422M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.541  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.582  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.917  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.137  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -3.273M (  -3.0 /   1.2)*
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.013  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        12  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         4M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -5.710M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         2  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        30  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  2
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00798.mol:
          Name                           Similarity(%)
Penimepicycline                               71.00
Niceritrol                                    69.31
Sulfamazone                                   67.91
Dipyridamole                                  67.90
Nicofuranose                                  66.35

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.200    H-bond Donor        -1.606 
 H-bond Acceptor     -7.329    H-bond Acceptor     -7.880 
 Volume              11.241    SASA                16.309 
 Ac x Dn^.5/SASA      1.551    Ac x Dn^.5/SASA      3.496 
 FISA                -2.108    Rotor Bonds         -1.954 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.090    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.541    Total                4.582


                log BB                    log PMDCK
 Hydrophilic SASA    -3.114<   Hydrophilic SASA    -3.122 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.723                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -3.273    Total                0.648

