 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Reactive FG: acetal or analog
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   464.425  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     7.089  (   1.0 /  12.5) 
     Solute        Total        SASA     =   734.477  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   244.917  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   320.903  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   168.657  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1314.720  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   185.291  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    10.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     5.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    13.250  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.790  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.914  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.437  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    41.651M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    15.096M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    29.239M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    22.985M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -0.314  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.594  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.190  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.709  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -3.465  (  -3.0 /   1.2)*
   No. of Primary Metabolites            =         9  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.731  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         8  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         3M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -5.880  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         2  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        16  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  2
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02623.mol:
          Name                           Similarity(%)
Idarubicin                                    74.47
Dipyridamole                                  67.77
Clindamycin                                   67.02
Methacycline                                  66.28
Methotrexate                                  66.11

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.500    H-bond Donor        -2.008 
 H-bond Acceptor     -6.453    H-bond Acceptor     -6.938 
 Volume               8.577    SASA                13.918 
 Ac x Dn^.5/SASA      1.789    Ac x Dn^.5/SASA      4.032 
 FISA                -2.221    Rotor Bonds         -1.628 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.197    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               -0.314    Total                3.594


                log BB                    log PMDCK
 Hydrophilic SASA    -3.426<   Hydrophilic SASA    -3.289<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.603                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -3.465    Total                0.482

