 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: alpha hydroxylation of carbonyl
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   470.691  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.179  (   1.0 /  12.5) 
     Solute        Total        SASA     =   693.835  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   545.866  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   137.738  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    10.231  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1406.133  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    85.022  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     2.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     5.700  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.875  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.228  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.240  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    48.933M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    12.673M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    22.499M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    10.498M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     5.183  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -6.413  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -6.521  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     1.064  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.757  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -1.799  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       123  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        65M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.830  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         1  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        82  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00163.mol:
          Name                           Similarity(%)
Acetoxolone                                   86.20
Dutasteride                                   80.69
Pentagestrone                                 76.78
Atovaquone                                    75.94
Mifepristone                                  74.54

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -2.776    H-bond Acceptor     -2.985 
 Volume               9.174    SASA                13.148 
 Ac x Dn^.5/SASA      0.515    Ac x Dn^.5/SASA      1.161 
 FISA                -0.438    Rotor Bonds         -0.326 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.012    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                5.183    Total                6.413


                log BB                    log PMDCK
 Hydrophilic SASA    -1.200    Hydrophilic SASA    -1.412 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.121                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids     -0.540 
 Constant             0.564    Constant             3.771
 Total               -0.757    Total                1.819

