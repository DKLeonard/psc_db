 Primary Metabolites & Reactive FGs:
 > Reactive FG: unhindered ester
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acceptor carbonyl or derivative
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   512.642  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.125  (   1.0 /  12.5) 
     Solute        Total        SASA     =   879.941  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   663.033  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   147.029  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    69.879  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1649.043  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   126.630  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    10.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     8.450  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.767  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.509  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.654  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    54.087M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    15.724M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    25.174M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    12.359M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     5.005  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -7.394  (  -6.5 /   0.5)*
   QP log S - conformation independent   =    -6.092  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.940  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.705  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         8  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.633  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       399  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       183M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.023  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         2  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        77  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01022.mol:
          Name                           Similarity(%)
Pirozadil                                     72.58
Cilnidipine                                   71.14
"Estriol                                      69.55
Binifibrate                                   68.07
Hydrocortisone                                67.60

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -4.115    H-bond Acceptor     -4.424 
 Volume              10.758    SASA                16.675 
 Ac x Dn^.5/SASA      0.602    Ac x Dn^.5/SASA      1.358 
 FISA                -1.018    Rotor Bonds         -1.628 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.082    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                5.005    Total                7.394


                log BB                    log PMDCK
 Hydrophilic SASA    -1.666    Hydrophilic SASA    -1.507 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.603                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000<   COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.705    Total                2.264

BC01023.mol                              contains an ammonium  - should be neutral amine.
