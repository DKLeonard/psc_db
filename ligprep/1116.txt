 Primary Metabolites & Reactive FGs:
 > Reactive FG: unhindered ester
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   222.197  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.853  (   1.0 /  12.5) 
     Solute        Total        SASA     =   423.838  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   181.426  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   107.598  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   134.814  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   694.632  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    73.764  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     3.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     4.750  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.895  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.828  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.875  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    21.006M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     6.656M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    11.084  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     7.869  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.124  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.992  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.506  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.462  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.513  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -3.875  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       945  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       465  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.740  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     4.121  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        87  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01116.mol:
          Name                           Similarity(%)
Tenonitrozole                                 92.32
Metaxalone                                    91.55
Mephenoxalone                                 89.38
Nitroxoline                                   89.36
Mofebutazone                                  89.28

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -2.313    H-bond Acceptor     -2.487 
 Volume               4.532    SASA                 8.032 
 Ac x Dn^.5/SASA      0.497    Ac x Dn^.5/SASA      1.120 
 FISA                -0.745    Rotor Bonds         -0.488 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.158    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.124    Total                1.992


                log BB                    log PMDCK
 Hydrophilic SASA    -0.896    Hydrophilic SASA    -1.103 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.181                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.513    Total                2.668

