 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   154.252  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     1.880  (   1.0 /  12.5) 
     Solute        Total        SASA     =   396.149  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   328.440  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    31.867  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    35.842  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   650.520  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    19.120  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     2.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     0.750  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.917  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.312  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -1.204  (  -0.9 /   1.7)*
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    18.938  (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     5.154  (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =     7.138  (   8.0 /  35.0)*
   QP log P  for     water/gas           =     2.895  (   4.0 /  45.0)*
   QP log P  for     octanol/water       =     2.957  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.661  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.014  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.118  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.190  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         4  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -3.054M (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      4939  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      2780  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.789  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     4.038  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  4
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01640.mol:
          Name                           Similarity(%)
Sobrerol                                      88.05
Aptrol                                        85.74
Mephentermine                                 85.19
Pempidine                                     84.74
Phenylpropanol                                84.74

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -0.365    H-bond Acceptor     -0.393 
 Volume               4.244    SASA                 7.507 
 Ac x Dn^.5/SASA      0.084    Ac x Dn^.5/SASA      0.189 
 FISA                -0.221    Rotor Bonds         -0.326 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.042    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.779    Total                2.794


                log BB                    log PMDCK
 Hydrophilic SASA    -0.253    Hydrophilic SASA    -0.327 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.121                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.190    Total                3.444

