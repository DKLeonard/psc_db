 Primary Metabolites & Reactive FGs:
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   326.391  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.761  (   1.0 /  12.5) 
     Solute        Total        SASA     =   653.762  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   398.872  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    60.542  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   194.348  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1133.043  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    52.834  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     9.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.804  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.845  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.303  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    35.268M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    10.986M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    15.469M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     6.925M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     4.723  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.928  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.239  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.634  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.603  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -5.631  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      2641  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      1413  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.087  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.103  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02070.mol:
          Name                           Similarity(%)
Tolterodine                                   91.55
Benzestrol                                    87.96
Fenalcomine                                   87.44
Pargeverine                                   86.82
Sontoquine                                    84.91

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -1.461    H-bond Acceptor     -1.571 
 Volume               7.392    SASA                12.389 
 Ac x Dn^.5/SASA      0.288    Ac x Dn^.5/SASA      0.649 
 FISA                -0.419    Rotor Bonds         -1.465 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.228    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                4.723    Total                5.415


                log BB                    log PMDCK
 Hydrophilic SASA    -0.624    Hydrophilic SASA    -0.621 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.542                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.603    Total                3.150

