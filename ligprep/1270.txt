 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: amine dealkylation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   237.298  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.077  (   1.0 /  12.5) 
     Solute        Total        SASA     =   480.522  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   401.753  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    44.568  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    34.201  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   820.272  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    38.527  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     3.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     4.250  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.882  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.703  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.241  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    25.074M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     6.681M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    11.234  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     6.400  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.874  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.872  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -1.731  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.066  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.399  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -4.332  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       933  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       508  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.995  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.322  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        91  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01270.mol:
          Name                           Similarity(%)
Metaxalone                                    89.49
Ketobemidone                                  89.01
Moxonidine                                    86.72
Viloxazine                                    86.55
Mofebutazone                                  86.37

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -2.070    H-bond Acceptor     -2.225 
 Volume               5.351    SASA                 9.106 
 Ac x Dn^.5/SASA      0.392    Ac x Dn^.5/SASA      0.884 
 FISA                -0.308    Rotor Bonds         -0.488 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.040    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.874    Total                1.872


                log BB                    log PMDCK
 Hydrophilic SASA    -0.382    Hydrophilic SASA    -0.457 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.181                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.399    Total                2.706

