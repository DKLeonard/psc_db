 Primary Metabolites & Reactive FGs:
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   257.332  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     1.515  (   1.0 /  12.5) 
     Solute        Total        SASA     =   498.494  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   337.048  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    23.865  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   137.581  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   858.938  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    35.810  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     0.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     4.750  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.877  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.282  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.520  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    29.535M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     7.387M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    11.649M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     6.696M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.054  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.957  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -1.787  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.159  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.755  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         2  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      ++ 
   HERG K+ Channel Blockage: log IC50    =    -4.911  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      1467  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       828  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.537  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.824  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        96  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02091.mol:
          Name                           Similarity(%)
Anisindione                                   88.52
Methaqualone                                  87.95
Vinburnine                                    87.74
Phenindione                                   86.07
Phenytoin                                     85.79

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -2.313    H-bond Acceptor     -2.487 
 Volume               5.604    SASA                 9.446 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.165    Rotor Bonds          0.000 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.161    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.054    Total                1.957


                log BB                    log PMDCK
 Hydrophilic SASA    -0.207    Hydrophilic SASA    -0.245 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds          0.000                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.755    Total                2.918

