 Primary Metabolites & Reactive FGs:
 > Metabolism likely: alpha hydroxylation of cyclic ether

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   332.439  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.642  (   1.0 /  12.5) 
     Solute        Total        SASA     =   506.061  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   385.575  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    47.224  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    73.262  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   973.068  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    49.110  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     0.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     5.450  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.938  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.660  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.845  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    33.488M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     8.531M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    14.044M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     7.125M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.748  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.953  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.897  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.015  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.206  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         1  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -2.878  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      3532  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      1935  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.132  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     2.732  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01051.mol:
          Name                           Similarity(%)
Canrenone                                     89.49
Testolactone                                  85.26
Loxapine                                      84.14
Ketazolam                                     83.72
Dolasetron                                    83.54

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -2.654    H-bond Acceptor     -2.854 
 Volume               6.348    SASA                 9.590 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.327    Rotor Bonds          0.000 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.086    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.748    Total                2.953


                log BB                    log PMDCK
 Hydrophilic SASA    -0.358    Hydrophilic SASA    -0.484 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds          0.000                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.206    Total                3.287

