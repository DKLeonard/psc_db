 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   288.256  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.545  (   1.0 /  12.5) 
     Solute        Total        SASA     =   512.659  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =    48.530  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   245.130  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   218.999  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   861.731  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   121.773  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     4.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     3.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     4.750  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.854  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.976  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.426  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    27.856M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    10.113M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    16.748M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    12.300M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.976  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.193  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.903  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.179  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.917  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.935  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        46  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        18  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.882  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.002  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        63  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00287.mol:
          Name                           Similarity(%)
Papaveroline                                  89.04
Dantrolene                                    84.85
Sulfaphenazole                                83.51
Endralazine                                   82.42
Zolmitriptan                                  82.23

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.900    H-bond Donor        -1.205 
 H-bond Acceptor     -2.313    H-bond Acceptor     -2.487 
 Volume               5.622    SASA                 9.715 
 Ac x Dn^.5/SASA      0.712    Ac x Dn^.5/SASA      1.604 
 FISA                -1.697    Rotor Bonds         -0.651 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.256    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.976    Total                3.193


                log BB                    log PMDCK
 Hydrophilic SASA    -2.240    Hydrophilic SASA    -2.513 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.241                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.917    Total                1.258

