 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   546.529  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     7.991  (   1.0 /  12.5) 
     Solute        Total        SASA     =   749.763  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =    78.932  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   341.062  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   329.770  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1495.385  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   180.393  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     8.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     8.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =     9.400  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.843  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.726  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.216  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    51.704M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    18.513M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    35.847M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    25.298M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.673  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.533  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -7.394  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.164  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -3.115  (  -3.0 /   1.2)*
   No. of Primary Metabolites            =        12  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.637  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         5  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         1M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -5.876  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         2  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        24  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  6
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01834.mol:
          Name                           Similarity(%)
Clomocycline                                  65.39
Ebrotidine                                    62.25
Minocycline                                   62.20
Imatinib                                      61.34
Tritoqualine                                  61.32

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -2.399    H-bond Donor        -3.213 
 H-bond Acceptor     -4.578    H-bond Acceptor     -4.922 
 Volume               9.756    SASA                14.208 
 Ac x Dn^.5/SASA      1.573    Ac x Dn^.5/SASA      3.545 
 FISA                -2.360    Rotor Bonds         -1.302 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.386    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.673    Total                4.533


                log BB                    log PMDCK
 Hydrophilic SASA    -3.196<   Hydrophilic SASA    -3.496<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.482                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -3.115    Total                0.275

BC01835.mol                             
 Max # of non-H atoms is 100 - molecule not processed
