 Primary Metabolites & Reactive FGs:
 > Reactive FG: unhindered ester
 > Metabolism likely: benzylic-like H -> alcohol
 > Reactive FG: acceptor carbonyl or derivative
 > Reactive FG: acceptor carbonyl or derivative
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   426.465  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     8.849  (   1.0 /  12.5) 
     Solute        Total        SASA     =   730.208  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   424.426  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   108.213  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   197.570  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1346.914  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   103.630  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     5.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     7.250  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.808  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.325  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     1.040  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    46.426M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    12.908M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    19.924M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     9.595M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     4.034  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -5.445  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.416  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.376  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.844  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -5.585  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       932  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       458  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.338  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.007  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00998.mol:
          Name                           Similarity(%)
Nilvadipine                                   85.22
Nisoldipine                                   84.16
Aranidipine                                   84.15
Nimodipine                                    84.05
Diltiazem                                     83.78

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -3.531    H-bond Acceptor     -3.796 
 Volume               8.787    SASA                13.837 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.749    Rotor Bonds         -0.814 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.231    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                4.034    Total                5.445


                log BB                    log PMDCK
 Hydrophilic SASA    -1.106    Hydrophilic SASA    -1.109 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.301                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.844    Total                2.662

