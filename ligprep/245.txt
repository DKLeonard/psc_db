 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: aromatic OH oxidation
 > Reactive FG: acetal or analog
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   311.291  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.857  (   1.0 /  12.5) 
     Solute        Total        SASA     =   546.215  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   112.070  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   292.534  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   141.612  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   940.411  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   149.004  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    10.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     5.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    12.450  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.850  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.293  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.028  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    26.413M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    11.120M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    23.576M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    21.367M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -1.730  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.508  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.469  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.120  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -2.739  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.637  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        16  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         5  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -5.452  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.003  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        39  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00245.mol:
          Name                           Similarity(%)
Valganciclovir                                76.96
Valaciclovir                                  76.43
Exifone                                       73.18
Azacitidine                                   72.91
Sulfoxone                                     72.07

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.500    H-bond Donor        -2.008 
 H-bond Acceptor     -6.063    H-bond Acceptor     -6.519 
 Volume               6.135    SASA                10.351 
 Ac x Dn^.5/SASA      2.261    Ac x Dn^.5/SASA      5.095 
 FISA                -2.025    Rotor Bonds         -1.628 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.166    WPSA                 0.000 
 Constant            -0.705    Constant            -2.783 
 Total               -1.730    Total                2.508


                log BB                    log PMDCK
 Hydrophilic SASA    -2.700<   Hydrophilic SASA    -2.998 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.603                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -2.739    Total                0.772

