 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Reactive FG: acetal or analog

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   462.409  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     7.185  (   1.0 /  12.5) 
     Solute        Total        SASA     =   687.493  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   191.680  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   295.080  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   200.734  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1257.689  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   180.477  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     9.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     5.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    12.050  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.820  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.046  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     1.059  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    40.317M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    14.537M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    28.231M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    21.953M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.064  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.380  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.698  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.560  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -2.907  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         7  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.408  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        15  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         5M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -5.387  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.001  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         2  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        23  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01739.mol:
          Name                           Similarity(%)
Idarubicin                                    80.54
Methacycline                                  73.48
Vardenafil                                    69.29
Sildenafil                                    69.14
Doxycycline                                   68.42

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.500    H-bond Donor        -2.008 
 H-bond Acceptor     -5.868    H-bond Acceptor     -6.309 
 Volume               8.205    SASA                13.028 
 Ac x Dn^.5/SASA      1.739    Ac x Dn^.5/SASA      3.918 
 FISA                -2.042    Rotor Bonds         -1.465 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.235    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.064    Total                3.380


                log BB                    log PMDCK
 Hydrophilic SASA    -2.929<   Hydrophilic SASA    -3.025 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.542                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -2.907    Total                0.746

