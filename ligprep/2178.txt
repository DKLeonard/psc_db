 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   302.369  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.151  (   1.0 /  12.5) 
     Solute        Total        SASA     =   591.333  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   168.189  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   203.310  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   219.835  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1024.808  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    88.185  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     9.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     4.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.831  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.772  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.187  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    31.179M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    11.589M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    17.742M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    10.849M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.621  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.795  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.484  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.109  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.940  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.310  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       116  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        48  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.628  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.011  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        79  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02178.mol:
          Name                           Similarity(%)
Ritodrine                                     88.92
Flupirtine                                    87.39
Isoxsuprine                                   87.38
Denopamine                                    87.21
Nylidrin                                      87.18

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.200    H-bond Donor        -1.606 
 H-bond Acceptor     -1.461    H-bond Acceptor     -1.571 
 Volume               6.686    SASA                11.206 
 Ac x Dn^.5/SASA      0.450    Ac x Dn^.5/SASA      1.014 
 FISA                -1.407    Rotor Bonds         -1.465 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.257    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.621    Total                3.795


                log BB                    log PMDCK
 Hydrophilic SASA    -1.961    Hydrophilic SASA    -2.084 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.542                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.940    Total                1.687

