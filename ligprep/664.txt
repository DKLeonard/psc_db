 Primary Metabolites & Reactive FGs:
 > Metabolism likely: amine dealkylation
 > Reactive FG: unhindered ester
 > Metabolism likely: para hydroxylation of aryl
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Reactive FG: unhindered ester

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   714.851  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.231  (   1.0 /  12.5) 
     Solute        Total        SASA     =  1054.216  ( 300.0 /1000.0)*
     Solute        Hydrophobic  SASA     =   768.164  (   0.0 / 750.0)*
     Solute        Hydrophilic  SASA     =   141.305  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   144.747  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  2064.846  ( 500.0 /2000.0)*
     Solute        vdW Polar SA (PSA)    =   145.027  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    14.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    15.800  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.744  (  0.75 /  0.95)*
     Solute Ionization Potential (eV)    =     9.012  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.823  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    68.874M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    19.945M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    32.893M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    18.141M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     4.101  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -5.520  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -6.546  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.257  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.584M (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -7.093  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       112M (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        51M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.332M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         2  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        62  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  5
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00664.mol:
          Name                           Similarity(%)
Nicomol                                       69.59
Syrosingopine                                 66.47
Saquinavir                                    64.16
Clarithromycin                                63.23
Cefotiam                                      62.94

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -7.695    H-bond Acceptor     -8.273 
 Volume              13.471    SASA                19.977<
 Ac x Dn^.5/SASA      0.665    Ac x Dn^.5/SASA      1.498 
 FISA                -0.978    Rotor Bonds         -2.279 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.169    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                4.101    Total                5.520


                log BB                    log PMDCK
 Hydrophilic SASA    -1.703    Hydrophilic SASA    -1.448 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.844                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000<   COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.584    Total                1.714

