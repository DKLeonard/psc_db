 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   236.353  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.201  (   1.0 /  12.5) 
     Solute        Total        SASA     =   538.056  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   427.927  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    62.452  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    47.678  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   925.374  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    42.744  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     5.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     1.750  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.854  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.436  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.323  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    28.124M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     7.567M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =     9.725  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     2.384  (   4.0 /  45.0)*
   QP log P  for     octanol/water       =     4.104  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.683  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.446  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.624  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.309  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         7  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -4.129  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      2533  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      1351  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.022  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.047  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  2
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01485.mol:
          Name                           Similarity(%)
Clofibrate                                    86.54
Cloforex                                      85.89
Butibufen                                     85.22
Gemfibrozil                                   82.85
Fenfluramine                                  82.84

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -0.852    H-bond Acceptor     -0.916 
 Volume               6.037    SASA                10.196 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.432    Rotor Bonds         -0.814 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.056    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                4.104    Total                4.683


                log BB                    log PMDCK
 Hydrophilic SASA    -0.572    Hydrophilic SASA    -0.640 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.301                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.309    Total                3.131

