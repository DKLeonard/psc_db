 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   494.667  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.537  (   1.0 /  12.5) 
     Solute        Total        SASA     =   766.617  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   511.786  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   244.096  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    10.735  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1495.985  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   139.562  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    12.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     6.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    10.300  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.825  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.153  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.231  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    46.113M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    15.186M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    30.058M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    19.583M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.023  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.278  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.927  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.015  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -2.550  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         8  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.412  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        47  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        18M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.828  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         1  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        56  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00755.mol:
          Name                           Similarity(%)
Amprenavir                                    79.12
Methotrexate                                  72.70
Glucametacin                                  70.80
Dipyridamole                                  69.85
Moexipril                                     69.29

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.799    H-bond Donor        -2.410 
 H-bond Acceptor     -5.016    H-bond Acceptor     -5.393 
 Volume               9.760    SASA                14.527 
 Ac x Dn^.5/SASA      1.460    Ac x Dn^.5/SASA      3.290 
 FISA                -1.689    Rotor Bonds         -1.954 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.013    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.023    Total                4.278


                log BB                    log PMDCK
 Hydrophilic SASA    -2.390    Hydrophilic SASA    -2.502 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.723                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -2.550    Total                1.269

