 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: amine dealkylation
 > Metabolism likely: para hydroxylation of aryl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   165.235  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.414  (   1.0 /  12.5) 
     Solute        Total        SASA     =   406.205  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   174.466  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    47.818  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   183.921  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   663.091  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    32.778  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     4.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.200  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.905  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.082  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.041  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    19.571  (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     6.568  (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    10.412  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     7.472  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.339  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -0.428  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -0.669  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.369  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.332  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -4.667  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       869  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       470  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.431  (Kp in cm/hr)
   Jm, max transdermal transport rate    =    12.813  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        87  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)


       5 of   1712 molecules most similar to BC00106.mol:
          Name                           Similarity(%)
Clorprenaline                                 91.87
Betahistine                                   91.35
Methoxyphenamine                              90.99
Prothionamide                                 90.96
Pargyline                                     90.69

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -1.558    H-bond Acceptor     -1.676 
 Volume               4.326    SASA                 7.698 
 Ac x Dn^.5/SASA      0.494    Ac x Dn^.5/SASA      1.114 
 FISA                -0.331    Rotor Bonds         -0.651 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.215    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.315    Total                0.679


                log BB                    log PMDCK
 Hydrophilic SASA    -0.389    Hydrophilic SASA    -0.490 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.241                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.332    Total                2.673

