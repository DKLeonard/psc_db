 Primary Metabolites & Reactive FGs:
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   356.418  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.034  (   1.0 /  12.5) 
     Solute        Total        SASA     =   643.095  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   435.507  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    37.570  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   170.018  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1162.008  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    54.879  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     5.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     5.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.831  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.905  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.726  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    38.763M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    10.474M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    15.167M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     6.501M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     4.380  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.972  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.028  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.442  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.100  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         4  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -5.074  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      4361  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      2430  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.133  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.280  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02074.mol:
          Name                           Similarity(%)
Citalopram                                    88.42
Lorcainide                                    88.09
Clometacin                                    87.68
Fentanyl                                      87.45
Tofisopam                                     86.75

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -2.435    H-bond Acceptor     -2.618 
 Volume               7.581    SASA                12.187 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.260    Rotor Bonds         -0.814 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.199    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                4.380    Total                4.972


                log BB                    log PMDCK
 Hydrophilic SASA    -0.363    Hydrophilic SASA    -0.385 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.301                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.100    Total                3.386

