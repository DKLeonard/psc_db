 Primary Metabolites & Reactive FGs:
 > Reactive FG: heteroatom in 3-ring
 > Reactive FG: heteroatom in 3-ring
 > Metabolism likely: primary alcohol -> acid
 > Reactive FG: unhindered ester
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acceptor carbonyl or derivative
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   434.442  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     7.809  (   1.0 /  12.5) 
     Solute        Total        SASA     =   606.462  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   395.137  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   181.067  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    30.258  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1206.492  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   152.764  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     7.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    11.700  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.904  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.708  (   7.9 /  10.5)*
     Solute Electron Affinity    (eV)    =     0.721  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    37.924M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    11.026M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    19.581M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    12.596M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.251  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -0.444  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.654  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.307  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.336  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -3.234  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       190  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        82M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.078  (Kp in cm/hr)
   Jm, max transdermal transport rate    =    13.064  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        69  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01230.mol:
          Name                           Similarity(%)
Pivmecillinam                                 68.87
Cinepazide                                    67.75
Ampiroxicam                                   65.38
Lenampicillin                                 65.36
Penamecillin                                  64.79

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -5.698    H-bond Acceptor     -6.126 
 Volume               7.871    SASA                11.492 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -1.253    Rotor Bonds         -1.140 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.035    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.251    Total                0.444


                log BB                    log PMDCK
 Hydrophilic SASA    -1.478    Hydrophilic SASA    -1.856 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.422                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.336    Total                1.915

