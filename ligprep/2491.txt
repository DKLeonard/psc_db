 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Reactive FG: acetal or analog
 > Reactive FG: acetal or analog

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   610.524  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     9.857  (   1.0 /  12.5) 
     Solute        Total        SASA     =   820.390  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   186.257  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   415.940  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   218.193  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1588.644  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   270.500  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    16.000  (   0.0 /  15.0)*
     Solute as Donor -    Hydrogen Bonds =     9.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =    21.500  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.803  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.021  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.687  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    49.234M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    19.455M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    42.994M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    36.836M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -2.646  (  -2.0 /   6.5)*
   QP log S  for   aqueous solubility    =    -2.146  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.834  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.454  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -4.706  (  -3.0 /   1.2)*
   No. of Primary Metabolites            =        10  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.767  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         1  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -6.880M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.001M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         0  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars = 10
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02491.mol:
          Name                           Similarity(%)
Monoxerutin                                   87.37
Troxerutin                                    67.69
Lactitol                                      59.11
Lactulose                                     53.95
Lymecycline                                   46.55

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -2.699<   H-bond Donor        -3.614<
 H-bond Acceptor    -10.470    H-bond Acceptor    -11.257<
 Volume              10.364    SASA                15.546 
 Ac x Dn^.5/SASA      3.488    Ac x Dn^.5/SASA      7.859 
 FISA                -2.879    Rotor Bonds         -2.605<
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.256    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               -2.646    Total                2.146


                log BB                    log PMDCK
 Hydrophilic SASA    -4.305<   Hydrophilic SASA    -4.263<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.964<                        0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -4.706    Total               -0.493

