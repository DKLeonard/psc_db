 Primary Metabolites & Reactive FGs:
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   204.355  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     0.182  (   1.0 /  12.5)*
     Solute        Total        SASA     =   441.008  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   411.288  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =     0.000  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =    29.719  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   782.091  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =     0.000  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=     0.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     0.000  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.931  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.348  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -1.081  (  -0.9 /   1.7)*
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    25.429M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     6.087M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =     7.438  (   8.0 /  35.0)*
   QP log P  for     water/gas           =    -0.467  (   4.0 /  45.0)*
   QP log P  for     octanol/water       =     4.886  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -5.957  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.957  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.888  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     1.016  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      ++ 
   HERG K+ Channel Blockage: log IC50    =    -2.792  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      9906  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      5899  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.415  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.009  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  7
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01531.mol:
          Name                           Similarity(%)
Methsuximide                                  74.57
Xibornol                                      74.35
Levamisole                                    74.26
Phensuximide                                  74.14
Chlornaphazine                                73.05

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor      0.000    H-bond Acceptor      0.000 
 Volume               7.509    SASA                 8.140 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                 0.000    Rotor Bonds          0.000 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.000    WPSA                 0.000 
 Constant            -2.623    Constant            -2.183 
 Total                4.886    Total                5.957


                log BB                    log PMDCK
 Hydrophilic SASA     0.000    Hydrophilic SASA     0.000 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds          0.000                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.824    COOH/SO3H acids      0.000 
 Constant             0.192    Constant             3.771
 Total                1.016    Total                3.771

