 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: amine dealkylation
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   185.222  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.919  (   1.0 /  12.5) 
     Solute        Total        SASA     =   379.789  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   249.381  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   130.408  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =     0.000  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   625.574  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    72.454  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     2.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     4.700  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.931  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.486  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -1.269  (  -0.9 /   1.7)*
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    17.597  (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     5.480  (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =     9.521  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     7.173  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -1.451  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -0.020  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -0.221  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.717  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.161  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -1.561  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        36  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        19  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -5.794  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.037  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        46  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1

       5 of   1712 molecules most similar to BC02277.mol:
          Name                           Similarity(%)
Isosorbide                                    84.32
Epomediol                                     83.93
Tranexamic                                    83.27
Gabapentin                                    82.11
Isosorbide                                    79.88

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -2.289    H-bond Acceptor     -2.461 
 Volume               4.081    SASA                 7.197 
 Ac x Dn^.5/SASA      0.549    Ac x Dn^.5/SASA      1.237 
 FISA                -0.387    Rotor Bonds         -0.326 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.000    WPSA                 0.000 
 Constant            -2.579    Constant            -3.118 
 Total               -1.451    Total                0.908

 Aminoacid correction of -1.9 has been applied to log Po/w.

                log BB                    log PMDCK
 Hydrophilic SASA    -1.002    Hydrophilic SASA    -1.337 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.121                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids     -0.540 
 Constant             0.564    Constant             3.771
 Total               -0.161    Total                1.286

