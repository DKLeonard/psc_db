 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: furan epoxidation
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: alpha hydroxylation of carbonyl
 > Reactive FG: acetal or analog

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   524.564  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.554  (   1.0 /  12.5) 
     Solute        Total        SASA     =   698.140  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   323.889  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   279.774  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    94.478  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1406.067  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   195.008  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    11.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     7.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =    16.850  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.869  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.735  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.507  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    43.956M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    15.404M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    34.596M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    28.368M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -0.830  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.405  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.373  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.798  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -2.567  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =        10  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.096  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        22  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         8M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -5.287  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.011  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         7  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  2
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01012.mol:
          Name                           Similarity(%)
Thiamine                                      62.05
Idarubicin                                    61.53
Sulfamazone                                   58.79
Famotidine                                    57.08
Valganciclovir                                56.85

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -2.099    H-bond Donor        -2.811 
 H-bond Acceptor     -8.206    H-bond Acceptor     -8.823 
 Volume               9.173    SASA                13.230 
 Ac x Dn^.5/SASA      2.833    Ac x Dn^.5/SASA      6.383 
 FISA                -1.936    Rotor Bonds         -1.791 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.111    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               -0.830    Total                2.405


                log BB                    log PMDCK
 Hydrophilic SASA    -2.468<   Hydrophilic SASA    -2.868 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.663                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -2.567    Total                0.903

