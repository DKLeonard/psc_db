 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acetal or analog

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   301.341  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     1.187  (   1.0 /  12.5) 
     Solute        Total        SASA     =   481.508  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   304.246  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    45.367  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   131.895  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   873.642  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    52.990  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     2.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     6.900  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.918  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.883  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.036  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    28.785M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     8.122M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    14.133M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     9.903M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.284  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.215  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.200  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.340  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.482  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      ++ 
   HERG K+ Channel Blockage: log IC50    =    -4.216  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       917  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       498  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.762  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     3.183  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        87  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00520.mol:
          Name                           Similarity(%)
Indisetron                                    88.26
Metopon                                       88.12
Vinburnine                                    86.99
Piribedil                                     85.51
Granisetron                                   84.05

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -3.360    H-bond Acceptor     -3.613 
 Volume               5.700    SASA                 9.125 
 Ac x Dn^.5/SASA      0.636    Ac x Dn^.5/SASA      1.432 
 FISA                -0.314    Rotor Bonds         -0.326 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.154    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.284    Total                1.215


                log BB                    log PMDCK
 Hydrophilic SASA    -0.359    Hydrophilic SASA    -0.465 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.121                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.482    Total                2.698

