 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   156.267  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     1.899  (   1.0 /  12.5) 
     Solute        Total        SASA     =   400.537  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   360.460  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    40.077  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =     0.000  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   663.544  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    20.890  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     2.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     1.700  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.919  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.942  (   7.9 /  10.5)*
     Solute Electron Affinity    (eV)    =    -2.957  (  -0.9 /   1.7)*
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    19.117  (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     5.112  (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =     7.702  (   8.0 /  35.0)*
   QP log P  for     water/gas           =     3.654  (   4.0 /  45.0)*
   QP log P  for     octanol/water       =     2.761  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.584  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -1.635  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.004  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.126  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         1  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -2.850M (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      4129  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      2290  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.066  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     3.262  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  5
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00015.mol:
          Name                           Similarity(%)
Mecamylamine                                  90.41
Cyclopentamine                                90.19
Phenylpropanol                                89.37
Isaxonine                                     88.83
Pempidine                                     88.49

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -0.828    H-bond Acceptor     -0.890 
 Volume               4.329    SASA                 7.590 
 Ac x Dn^.5/SASA      0.188    Ac x Dn^.5/SASA      0.424 
 FISA                -0.277    Rotor Bonds         -0.326 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.000    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.407    Total                2.614


                log BB                    log PMDCK
 Hydrophilic SASA    -0.317    Hydrophilic SASA    -0.411 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.121                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.126    Total                3.360

