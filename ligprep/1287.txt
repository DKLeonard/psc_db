 Primary Metabolites & Reactive FGs:
 > Reactive FG: heteroatom in 3-ring
 > Reactive FG: unhindered ester
 > Reactive FG: unhindered ester
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Reactive FG: acceptor carbonyl or derivative
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   464.468  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     8.232  (   1.0 /  12.5) 
     Solute        Total        SASA     =   662.146  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   465.559  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   174.377  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    22.209  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1321.899  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   169.443  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     5.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    13.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.880  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.720  (   7.9 /  10.5)*
     Solute Electron Affinity    (eV)    =     1.066  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    43.750M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    11.854M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    22.325M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    14.352M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.407  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.144  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.688  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.234  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.240  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -3.584  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       219  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        96M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.175  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     2.228  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        71  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01287.mol:
          Name                           Similarity(%)
Pivmecillinam                                 66.33
Talampicillin                                 64.40
Scopolamine                                   64.20
Cefetamet                                     63.24
Bacampicillin                                 63.18

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -6.331    H-bond Acceptor     -6.807 
 Volume               8.624    SASA                12.548 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -1.207    Rotor Bonds         -0.814 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.026    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.407    Total                1.144


                log BB                    log PMDCK
 Hydrophilic SASA    -1.502    Hydrophilic SASA    -1.787 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.301                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.240    Total                1.983

