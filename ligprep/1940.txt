 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: ether dealkylation
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acceptor carbonyl or derivative
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   411.494  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.363  (   1.0 /  12.5) 
     Solute        Total        SASA     =   672.295  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   557.436  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    68.039  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    46.819  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1291.197  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   100.860  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    10.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     8.200  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.853  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.068  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.029  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    39.546M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    11.226M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    18.671M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     9.454M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     3.024  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.634  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.399  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.093  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.264  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         8  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -4.861  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       559  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       292  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.711  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.186  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        94  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01940.mol:
          Name                           Similarity(%)
Dixyrazine                                    81.51
Carphenazine                                  81.31
Zipeprol                                      79.86
Trimethobenzamide                             79.29
Acetophenazine                                78.93

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -3.993    H-bond Acceptor     -4.294 
 Volume               8.424    SASA                12.740 
 Ac x Dn^.5/SASA      0.541    Ac x Dn^.5/SASA      1.219 
 FISA                -0.471    Rotor Bonds         -1.628 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.055    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                3.024    Total                2.634


                log BB                    log PMDCK
 Hydrophilic SASA    -0.623    Hydrophilic SASA    -0.697 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.603                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.264    Total                2.465

