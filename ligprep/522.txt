 Primary Metabolites & Reactive FGs:
 > Metabolism likely: ether dealkylation
 > Metabolism likely: amine dealkylation
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   285.342  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.151  (   1.0 /  12.5) 
     Solute        Total        SASA     =   491.583  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   296.536  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    61.122  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   133.924  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   886.337  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    48.569  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     1.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     5.500  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.908  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.903  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.426  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    29.954M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     8.073M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    12.814M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     7.426M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.606  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.271  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -1.924  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.323  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.407  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         4  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -4.389  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       650  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       343  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.141  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     1.107  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        87  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00522.mol:
          Name                           Similarity(%)
Metopon                                       88.19
Vinburnine                                    85.58
Nimetazepam                                   84.96
Piribedil                                     83.83
Nitrazepam                                    83.52

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -2.679    H-bond Acceptor     -2.880 
 Volume               5.782    SASA                 9.315 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.423    Rotor Bonds         -0.163 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.157    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.606    Total                1.271


                log BB                    log PMDCK
 Hydrophilic SASA    -0.495    Hydrophilic SASA    -0.627 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.060                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.407    Total                2.536

BC00523.mol                             
 Has an odd number of electrons or is charged 381 - molecule not processed
