 Primary Metabolites & Reactive FGs:
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acetal or analog
 > Reactive FG: acetal or analog
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: acetal or analog
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   722.912  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.637  (   1.0 /  12.5) 
     Solute        Total        SASA     =  1007.696  ( 300.0 /1000.0)*
     Solute        Hydrophobic  SASA     =   749.252  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   234.555  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    23.889  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  2064.359  ( 500.0 /2000.0)*
     Solute        vdW Polar SA (PSA)    =   168.200  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    11.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     6.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    18.500  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.778  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.652  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.891  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    69.627M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    20.391M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    42.505M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    28.486M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.354  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -5.921  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -6.355  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.037  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -2.682M (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         8  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.237  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        59M (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        23M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.702M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        34  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  4
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00844.mol:
          Name                           Similarity(%)
Metildigoxin                                  73.20
Azithromycin                                  71.86
Flurithromycin                                71.22
Acetyldigoxin                                 69.75
beta                                          69.73

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.799    H-bond Donor        -2.410 
 H-bond Acceptor     -9.009    H-bond Acceptor     -9.687 
 Volume              13.468    SASA                19.096 
 Ac x Dn^.5/SASA      1.995    Ac x Dn^.5/SASA      4.495 
 FISA                -1.623    Rotor Bonds         -1.791 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.028    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.354    Total                5.921


                log BB                    log PMDCK
 Hydrophilic SASA    -2.583    Hydrophilic SASA    -2.404 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.663                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000<   COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -2.682    Total                1.367

