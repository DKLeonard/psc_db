 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aniline NH -> NOH or NCOR
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: benzylic-like H -> alcohol
 > Reactive FG: acceptor carbonyl or derivative

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   211.174  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     7.036  (   1.0 /  12.5) 
     Solute        Total        SASA     =   420.092  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =    73.589  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   302.217  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    44.286  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   672.751  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   149.685  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     4.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     3.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     7.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.884  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.731  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     1.225  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    18.624M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     7.527M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    15.462  (   8.0 /  35.0) 
   QP log P  for     water/gas           =    13.493  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -0.353  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.542  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -1.332  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.096  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -2.257  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -0.007  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         0  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -6.549  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.002  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        24  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00524.mol:
          Name                           Similarity(%)
Methaniazide                                  84.54
Fosfosal                                      83.27
Proxibarbal                                   82.65
Erdosteine                                    82.22
Sulfaguanidine                                80.69

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.900    H-bond Donor        -1.205 
 H-bond Acceptor     -3.409    H-bond Acceptor     -3.665 
 Volume               4.389    SASA                 7.961 
 Ac x Dn^.5/SASA      1.280    Ac x Dn^.5/SASA      2.885 
 FISA                -1.060    Rotor Bonds         -0.651 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.052    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               -0.353    Total                1.542


                log BB                    log PMDCK
 Hydrophilic SASA    -2.579<   Hydrophilic SASA    -3.098 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.241                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids     -1.080 
 Constant             0.564    Constant             3.771
 Total               -2.257    Total               -0.407

BC00525.mol                             
 Has an odd number of electrons or is charged 203 - molecule not processed
BC00526.mol                             
 Has an odd number of electrons or is charged 289 - molecule not processed
BC00527.mol                             
 Has an odd number of electrons or is charged 375 - molecule not processed
BC00528.mol                             
 Has an odd number of electrons or is charged 473 - molecule not processed
