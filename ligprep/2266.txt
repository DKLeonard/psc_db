 Primary Metabolites & Reactive FGs:
 > Reactive FG: unhindered ester
 > Metabolism likely: amine dealkylation
 > Metabolism likely: amine dealkylation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: para hydroxylation of aryl
 > Metabolism likely: para hydroxylation of aryl
 > Metabolism likely: para hydroxylation of aryl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   542.717  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.811  (   1.0 /  12.5) 
     Solute        Total        SASA     =   885.690  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   567.734  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    42.314  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   275.642  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1709.853  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    66.859  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     4.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     8.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.781  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.318  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.099  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    62.338M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    16.402M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    25.064M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    11.011M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     5.530  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -5.722  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.543  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     1.272  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.657  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      ++ 
   HERG K+ Channel Blockage: log IC50    =    -7.846  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       244  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       132M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -5.070  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         2  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        76  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02266.mol:
          Name                           Similarity(%)
Fenoverine                                    74.64
Cortivazol                                    73.02
Ketoconazole                                  71.50
Nicomorphine                                  70.69
Mosapramine                                   69.62

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -3.896    H-bond Acceptor     -4.189 
 Volume              11.155    SASA                16.784 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.293    Rotor Bonds         -0.651 
 Non-con amines      -1.054    N Protonation       -2.439 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.323    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                5.530    Total                5.722


                log BB                    log PMDCK
 Hydrophilic SASA    -0.463    Hydrophilic SASA    -0.434 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.241                         0.000 
 N Protonation        0.797    Non-con amines      -1.216 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.657    Total                2.121

