 Primary Metabolites & Reactive FGs:
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   222.370  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.188  (   1.0 /  12.5) 
     Solute        Total        SASA     =   578.010  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   488.065  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    55.530  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    34.415  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   976.889  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    23.039  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     8.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     1.700  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.824  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.389  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.815  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    28.133M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     8.069M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    10.579  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     3.096  (   4.0 /  45.0)*
   QP log P  for     octanol/water       =     4.327  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.870  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.988  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.602  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.464  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         9  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -4.548  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      2946  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      1590  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.654  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.067  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  3
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00198.mol:
          Name                           Similarity(%)
Butoctamide                                   82.29
Febuprol                                      77.64
Bifemelane                                    77.51
Butibufen                                     77.00
Mestilbol                                     76.14

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -0.828    H-bond Acceptor     -0.890 
 Volume               6.373    SASA                10.953 
 Ac x Dn^.5/SASA      0.130    Ac x Dn^.5/SASA      0.294 
 FISA                -0.384    Rotor Bonds         -1.302 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.040    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                4.327    Total                4.870


                log BB                    log PMDCK
 Hydrophilic SASA    -0.546    Hydrophilic SASA    -0.569 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.482                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.464    Total                3.202

