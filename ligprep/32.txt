 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   382.298  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     9.337  (   1.0 /  12.5) 
     Solute        Total        SASA     =   558.484  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =     0.000  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   354.893  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   203.591  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   976.665  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   186.149  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     7.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     4.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     9.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.852  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.249  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     1.212  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    30.382M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    12.133M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    22.715M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    18.041M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -0.188  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.564  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.307  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.754  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -3.115  (  -3.0 /   1.2)*
   No. of Primary Metabolites            =         4  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -3.083  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         1  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -6.671  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        26  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  2
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00032.mol:
          Name                           Similarity(%)
Theodrenaline                                 85.90
Azosemide                                     85.04
Sulfaloxic                                    82.13
Sulfasalazine                                 81.68
Phthalylsulfathiazole                         81.32

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.200    H-bond Donor        -1.606 
 H-bond Acceptor     -4.383    H-bond Acceptor     -4.712 
 Volume               6.372    SASA                10.583 
 Ac x Dn^.5/SASA      1.430    Ac x Dn^.5/SASA      3.222 
 FISA                -1.941    Rotor Bonds         -1.140 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.238    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               -0.188    Total                2.564


                log BB                    log PMDCK
 Hydrophilic SASA    -3.256<   Hydrophilic SASA    -3.638<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.422                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids     -0.540 
 Constant             0.564    Constant             3.771
 Total               -3.115    Total               -0.407

