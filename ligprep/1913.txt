 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acceptor carbonyl or derivative
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   397.467  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.227  (   1.0 /  12.5) 
     Solute        Total        SASA     =   651.089  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   488.234  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   118.242  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    44.613  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1228.668  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   113.380  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    10.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     7.200  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.852  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.175  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.040  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    37.022M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    11.008M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    17.233M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     8.744M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.702  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.642  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.520  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.129  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.726  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         8  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -4.864  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       186  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        89  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.644  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.021  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        83  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01913.mol:
          Name                           Similarity(%)
Roxatidine                                    80.51
Trimethobenzamide                             80.46
Cloricromen                                   79.54
Zipeprol                                      79.18
Dixyrazine                                    78.41

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -3.506    H-bond Acceptor     -3.770 
 Volume               8.016    SASA                12.338 
 Ac x Dn^.5/SASA      0.491    Ac x Dn^.5/SASA      1.105 
 FISA                -0.818    Rotor Bonds         -1.628 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.052    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.702    Total                2.642


                log BB                    log PMDCK
 Hydrophilic SASA    -1.086    Hydrophilic SASA    -1.212 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.603                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.726    Total                1.951

