 Primary Metabolites & Reactive FGs:
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Reactive FG: acceptor carbonyl or derivative
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   278.391  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.836  (   1.0 /  12.5) 
     Solute        Total        SASA     =   639.244  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   448.469  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    77.017  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   113.757  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1083.668  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    54.868  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    11.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.500  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.798  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.969  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.084  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    31.238M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.822M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    12.733M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     5.041M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     4.204  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.853  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.824  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.413  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.905  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -5.340  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      1843  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       958  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -1.482  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.129  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02021.mol:
          Name                           Similarity(%)
Butamirate                                    84.10
Oxprenolol                                    83.76
Camylofin                                     83.53
Trepibutone                                   82.48
Acitretin                                     82.14

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -1.704    H-bond Acceptor     -1.833 
 Volume               7.070    SASA                12.114 
 Ac x Dn^.5/SASA      0.243    Ac x Dn^.5/SASA      0.547 
 FISA                -0.533    Rotor Bonds         -1.791 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.133    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                4.204    Total                4.853


                log BB                    log PMDCK
 Hydrophilic SASA    -0.806    Hydrophilic SASA    -0.789 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.663                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.905    Total                2.981

