 Primary Metabolites & Reactive FGs:
 > Metabolism likely: ether dealkylation

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   164.163  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =    11.908  (   1.0 /  12.5) 
     Solute        Total        SASA     =   374.129  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   175.093  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   114.445  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    84.591  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   586.180  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    63.860  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     2.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     5.250  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.905  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.334  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.915  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    16.829  (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     5.070  (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    11.076  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     6.703  (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -0.093  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -1.320  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -1.484  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.170  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.488  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         1  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -3.466  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       813  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       396  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.139  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     6.984  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        78  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)


       5 of   1712 molecules most similar to BC00099.mol:
          Name                           Similarity(%)
N-Hydroxymethylnicotinamide                   90.46
Piperidione                                   90.10
Paramethadione                                88.52
Ethadione                                     88.35
Methyprylon                                   87.59

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -2.557    H-bond Acceptor     -2.749 
 Volume               3.824    SASA                 7.090 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.792    Rotor Bonds         -0.326 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.099    WPSA                 0.000 
 Constant            -0.705    Constant            -2.783 
 Total               -0.130    Total                1.232


                log BB                    log PMDCK
 Hydrophilic SASA    -0.931    Hydrophilic SASA    -1.173 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.121                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.488    Total                2.598

