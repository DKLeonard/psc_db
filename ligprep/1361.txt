 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   264.321  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.057  (   1.0 /  12.5) 
     Solute        Total        SASA     =   468.803  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   252.185  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   151.785  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    64.832  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   843.558  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    83.101  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     3.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     6.400  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.921  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.145  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.241  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    26.298M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     8.366M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    15.405M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    10.892M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.964  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.388  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -1.992  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.305  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.810  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -3.120  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       360  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       164  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.800  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.171  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        78  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01361.mol:
          Name                           Similarity(%)
Heptabarbital                                 83.18
Didanosine                                    82.95
Molindone                                     82.83
Pyricarbate                                   81.98
Azathioprine                                  81.34

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -3.117    H-bond Acceptor     -3.351 
 Volume               5.503    SASA                 8.884 
 Ac x Dn^.5/SASA      0.856    Ac x Dn^.5/SASA      1.930 
 FISA                -1.051    Rotor Bonds         -0.488 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.076    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.964    Total                2.388


                log BB                    log PMDCK
 Hydrophilic SASA    -1.193    Hydrophilic SASA    -1.556 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.181                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.810    Total                2.215

