 Primary Metabolites & Reactive FGs:
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: benzylic-like H -> alcohol
 > Reactive FG: acetal or analog

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   325.363  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     1.532  (   1.0 /  12.5) 
     Solute        Total        SASA     =   546.209  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   321.356  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    40.304  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   184.549  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   967.219  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    50.640  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     2.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     5.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.866  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.651  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.059  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    33.032M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     9.059M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    14.547M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     8.512M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.687  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.918  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.656  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.216  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.483  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         6  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      ++ 
   HERG K+ Channel Blockage: log IC50    =    -5.307  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      1024  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       561  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.483  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.129  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        97  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00248.mol:
          Name                           Similarity(%)
Dolasetron                                    88.30
Paroxetine                                    88.27
Piribedil                                     86.31
Mepixanox                                     85.25
Granisetron                                   84.89

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -2.435    H-bond Acceptor     -2.618 
 Volume               6.310    SASA                10.351 
 Ac x Dn^.5/SASA      0.406    Ac x Dn^.5/SASA      0.915 
 FISA                -0.279    Rotor Bonds         -0.326 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.216    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.687    Total                2.918


                log BB                    log PMDCK
 Hydrophilic SASA    -0.358    Hydrophilic SASA    -0.413 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.121                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.483    Total                2.750

