 Primary Metabolites & Reactive FGs:
 > Metabolism likely: furan epoxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   286.240  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     5.393  (   1.0 /  12.5) 
     Solute        Total        SASA     =   512.450  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =    12.659  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   252.302  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   247.488  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   856.605  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   124.490  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     6.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     4.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     5.500  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.851  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.791  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     1.219  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    26.638M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    10.525M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    18.322M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    14.304M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.506  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.661  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.618  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.429  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -2.120  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.120  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        40  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        15  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.722  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.013  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        59  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00678.mol:
          Name                           Similarity(%)
Papaveroline                                  91.53
Trimethoprim                                  89.69
Sulfadoxine                                   86.48
Brodimoprim                                   85.14
Sulfalene                                     84.21

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.200    H-bond Donor        -1.606 
 H-bond Acceptor     -2.679    H-bond Acceptor     -2.880 
 Volume               5.588    SASA                 9.711 
 Ac x Dn^.5/SASA      0.952    Ac x Dn^.5/SASA      2.146 
 FISA                -1.746    Rotor Bonds         -0.977 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.290    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.501    Total                2.611


                log BB                    log PMDCK
 Hydrophilic SASA    -2.322<   Hydrophilic SASA    -2.586 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.362                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -2.120    Total                1.185

