 Primary Metabolites & Reactive FGs:
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: benzylic-like H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   256.257  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     6.406  (   1.0 /  12.5) 
     Solute        Total        SASA     =   478.584  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =    90.329  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   176.480  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   211.776  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   804.196  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    89.378  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     4.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.200  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.874  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.139  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.522  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    25.484M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     8.652M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    12.236M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     7.246M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.007  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.226  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.905  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.032  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.218  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         4  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -4.583  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       210  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        91  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.642  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.035  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        80  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02641.mol:
          Name                           Similarity(%)
Benzarone                                     88.64
Pranoprofen                                   88.18
Rizatriptan                                   86.76
Tramadol                                      86.64
Prothipendyl                                  86.29

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -1.558    H-bond Acceptor     -1.676 
 Volume               5.247    SASA                 9.069 
 Ac x Dn^.5/SASA      0.297    Ac x Dn^.5/SASA      0.668 
 FISA                -1.221    Rotor Bonds         -0.651 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.248    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.007    Total                3.226


                log BB                    log PMDCK
 Hydrophilic SASA    -1.541    Hydrophilic SASA    -1.809 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.241                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.218    Total                1.962

