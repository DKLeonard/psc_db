 Primary Metabolites & Reactive FGs:
 > Reactive FG: unhindered ester
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   196.289  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     2.966  (   1.0 /  12.5) 
     Solute        Total        SASA     =   468.658  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   389.014  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    43.947  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    35.697  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   786.073  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    32.521  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     2.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     2.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.879  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.502  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -1.014  (  -0.9 /   1.7)*
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    24.361  (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     6.099  (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =     8.329  (   8.0 /  35.0) 
   QP log P  for     water/gas           =     2.918  (   4.0 /  45.0)*
   QP log P  for     octanol/water       =     3.187  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.725  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.414  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.280  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.064  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -3.661  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      3794  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =      2090  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.012  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.360  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  2

       5 of   1712 molecules most similar to BC02467.mol:
          Name                           Similarity(%)
Levamisole                                    82.58
Diloxanide                                    82.32
Cyclobutyrol                                  81.57
Ibuprofen                                     81.20
Pyrantel                                      81.20

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -0.974    H-bond Acceptor     -1.047 
 Volume               5.128    SASA                 8.881 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.304    Rotor Bonds         -0.326 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.042    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                3.187    Total                3.725


                log BB                    log PMDCK
 Hydrophilic SASA    -0.379    Hydrophilic SASA    -0.450 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.121                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.064    Total                3.320

