 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: alpha hydroxylation of carbonyl
 > Reactive FG: acceptor carbonyl or derivative
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acetal or analog

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   398.452  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     7.417  (   1.0 /  12.5) 
     Solute        Total        SASA     =   626.734  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   418.603  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   203.840  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =     4.292  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1184.828  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   139.673  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     8.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     4.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    11.000  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.864  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.890  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -0.414  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    36.167M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    11.629M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    24.237M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    17.572M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.620  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.934  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -2.960  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.441  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -1.739  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         8  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -3.753  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       115  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        48  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.493  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.015  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         1  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        67  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02769.mol:
          Name                           Similarity(%)
Camostat                                      77.33
Cefdinir                                      75.19
Ceftibuten                                    73.84
Cefpodoxime                                   73.38
Triamcinolone                                 73.10

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.200    H-bond Donor        -1.606 
 H-bond Acceptor     -5.357    H-bond Acceptor     -5.760 
 Volume               7.730    SASA                11.877 
 Ac x Dn^.5/SASA      1.557    Ac x Dn^.5/SASA      3.509 
 FISA                -1.411    Rotor Bonds         -1.302 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.005    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.620    Total                2.934


                log BB                    log PMDCK
 Hydrophilic SASA    -1.820    Hydrophilic SASA    -2.089 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.482                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -1.739    Total                1.681

