 Primary Metabolites & Reactive FGs:
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Reactive FG: unhindered ester
 > Metabolism likely: amine dealkylation
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   682.809  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     8.045  (   1.0 /  12.5) 
     Solute        Total        SASA     =   902.420  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   629.177  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   127.311  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   145.931  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1862.011  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   137.331  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=    10.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    15.300  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.811  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.642  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.923  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    63.336M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    17.575M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    32.878M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    19.543M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     3.218  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -4.053  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -6.297  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.177  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.931M (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -6.011  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       153M (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =        72M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -4.454M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.002M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         2  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        59  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00658.mol:
          Name                           Similarity(%)
Nicomol                                       75.32
Nicofuranose                                  70.79
Syrosingopine                                 69.82
Olmesartan                                    68.23
Meproscillarin                                65.82

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -7.451    H-bond Acceptor     -8.011 
 Volume              12.148    SASA                17.101 
 Ac x Dn^.5/SASA      1.064    Ac x Dn^.5/SASA      2.397 
 FISA                -0.881    Rotor Bonds         -1.628 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.171    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                3.218    Total                4.053


                log BB                    log PMDCK
 Hydrophilic SASA    -1.290    Hydrophilic SASA    -1.305 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.603                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.931    Total                1.858

