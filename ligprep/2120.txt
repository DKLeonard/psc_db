 Primary Metabolites & Reactive FGs:
 > Metabolism likely: ether dealkylation
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: amine dealkylation
 > Metabolism likely: para hydroxylation of aryl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   285.299  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     7.898  (   1.0 /  12.5) 
     Solute        Total        SASA     =   514.092  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   238.794  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    80.939  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   194.360  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   890.599  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    62.374  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     3.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     3.750  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.871  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     8.184  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.688  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    29.418M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     8.469M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    12.258M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     5.861M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.947  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.550  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.231  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =     0.073  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.329  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -4.550  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      1691  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       873  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.039  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.812  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02120.mol:
          Name                           Similarity(%)
Isothipendyl                                  92.20
Promethazine                                  90.62
Dibenzepin                                    90.16
Prothipendyl                                  89.66
Chlorphenethazine                             89.47

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -1.826    H-bond Acceptor     -1.963 
 Volume               5.810    SASA                 9.742 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.560    Rotor Bonds         -0.488 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.228    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.947    Total                3.507


                log BB                    log PMDCK
 Hydrophilic SASA    -0.712    Hydrophilic SASA    -0.830 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.181                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.329    Total                2.941

