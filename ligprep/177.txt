 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   264.367  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     4.535  (   1.0 /  12.5) 
     Solute        Total        SASA     =   490.407  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   396.274  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =    94.134  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =     0.000  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=   869.718  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =    54.639  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     1.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     1.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     6.700  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.899  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.118  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =    -1.315  (  -0.9 /   1.7)*
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    28.010M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =     7.374M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    14.065M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    12.024M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     0.208  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -0.403  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -0.418  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.606  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =     0.124  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         2  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       + 
   HERG K+ Channel Blockage: log IC50    =    -2.526  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       164  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       157  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -5.221  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.628  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        68  (<25% is poor)
   Qual. Model for Human Oral Absorption =    Medium  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  1
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00177.mol:
          Name                           Similarity(%)
Thalidomide                                   84.36
Fenspiride                                    81.93
Azathioprine                                  80.91
Endralazine                                   79.32
Pemirolast                                    79.12

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.300    H-bond Donor        -0.402 
 H-bond Acceptor     -3.263    H-bond Acceptor     -3.508 
 Volume               5.674    SASA                 9.293 
 Ac x Dn^.5/SASA      0.606    Ac x Dn^.5/SASA      1.366 
 FISA                -0.651    Rotor Bonds         -0.163 
 Non-con amines      -0.527    N Protonation       -1.220 
 Non-con amides      -0.626    Non-con amides      -1.181 
 WPSA & PISA          0.000    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                0.208    Total                0.403


                log BB                    log PMDCK
 Hydrophilic SASA    -0.777    Hydrophilic SASA    -0.965 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.060                         0.000 
 N Protonation        0.399    Non-con amines      -0.608 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total                0.124    Total                2.198

