 Primary Metabolites & Reactive FGs:
 > Metabolism likely: alpha hydroxylation of cyclic ether
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: primary alcohol -> acid
 > Metabolism likely: ether dealkylation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: aromatic OH oxidation
 > Metabolism likely: benzylic-like H -> alcohol
 > Reactive FG: acetal or analog

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   592.552  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.570  (   1.0 /  12.5) 
     Solute        Total        SASA     =   797.438  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   235.476  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   388.151  (   7.0 / 330.0)*
     Solute        Carbon Pi    SASA     =   173.811  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1557.353  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   229.190  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    12.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     7.000  (   0.0 /   6.0)*
     Solute as Acceptor - Hydrogen Bonds =    19.050  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.815  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.127  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.875  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    50.126M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    18.154M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    38.320M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    31.830M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =    -1.600  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -2.907  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.496  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -1.018  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -4.057  (  -3.0 /   1.2)*
   No. of Primary Metabolites            =        11  (   1.0 /   8.0)*
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.357  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =         2  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         0M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -6.909  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =         0  (<25% is poor)
   Qual. Model for Human Oral Absorption =       low  (>80% is high)

       A * indicates a violation of the 95% range. # stars =  7
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC02486.mol:
          Name                           Similarity(%)
Monoxerutin                                   69.37
Penimepicycline                               60.38
Troxerutin                                    57.85
Lactulose                                     57.49
Thiamine                                      55.35

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -2.099    H-bond Donor        -2.811 
 H-bond Acceptor     -9.277    H-bond Acceptor     -9.975 
 Volume              10.160    SASA                15.111 
 Ac x Dn^.5/SASA      2.804    Ac x Dn^.5/SASA      6.318 
 FISA                -2.686    Rotor Bonds         -1.954 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.204    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total               -1.600    Total                2.907


                log BB                    log PMDCK
 Hydrophilic SASA    -3.898<   Hydrophilic SASA    -3.979<
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.723                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -4.057    Total               -0.208

