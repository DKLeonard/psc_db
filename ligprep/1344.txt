 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Reactive FG: unhindered ester
 > Reactive FG: acetal or analog
 > Metabolism likely: benzylic-like H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: alpha, beta dehydrogenation at carbonyl

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   366.410  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     3.193  (   1.0 /  12.5) 
     Solute        Total        SASA     =   595.427  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   423.756  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   140.297  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    31.373  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1119.134  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   113.388  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     5.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     2.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     8.200  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.876  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =    10.210  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.258  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    35.723M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    10.353M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    18.993M (   8.0 /  35.0) 
   QP log P  for     water/gas           =    12.223M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.933  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.537  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -3.496  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.064  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.958  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         5  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =       - 
   HERG K+ Channel Blockage: log IC50    =    -3.696  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =       462  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       215  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -3.515  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.033  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        86  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01344.mol:
          Name                           Similarity(%)
Prednisone                                    84.06
Meprednisone                                  83.63
Cloprednol                                    82.09
Prednylidene                                  81.81
Oxametacin                                    81.72

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -0.600    H-bond Donor        -0.803 
 H-bond Acceptor     -3.993    H-bond Acceptor     -4.294 
 Volume               7.301    SASA                11.283 
 Ac x Dn^.5/SASA      0.864    Ac x Dn^.5/SASA      1.947 
 FISA                -0.971    Rotor Bonds         -0.814 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.037    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.933    Total                3.537


                log BB                    log PMDCK
 Hydrophilic SASA    -1.220    Hydrophilic SASA    -1.438 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.301                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.958    Total                2.333

