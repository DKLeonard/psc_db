 Primary Metabolites & Reactive FGs:
 > Reactive FG: unhindered ester
 > Reactive FG: unhindered ester
 > Metabolism likely: benzylic-like H -> alcohol
 > Reactive FG: acceptor carbonyl or derivative
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: allylic H -> alcohol

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   386.401  ( 130.0 / 725.0) 
     Solute        Dipole Moment (D)     =     8.901  (   1.0 /  12.5) 
     Solute        Total        SASA     =   610.916  ( 300.0 /1000.0) 
     Solute        Hydrophobic  SASA     =   327.293  (   0.0 / 750.0) 
     Solute        Hydrophilic  SASA     =   100.225  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =   183.398  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  1156.813  ( 500.0 /2000.0) 
     Solute        vdW Polar SA (PSA)    =   104.667  (   7.0 / 200.0) 
     Solute        No. of Rotatable Bonds=     3.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     0.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =     7.250  (   2.0 /  20.0) 
     Solute Globularity   (Sphere = 1)   =     0.872  (  0.75 /  0.95) 
     Solute Ionization Potential (eV)    =     9.284  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.992  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    39.967M (  13.0 /  70.0) 
   QP log P  for     hexadecane/gas      =    11.019M (   4.0 /  18.0) 
   QP log P  for     octanol/gas         =    17.991M (   8.0 /  35.0) 
   QP log P  for     water/gas           =     9.598M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     2.832  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -3.510  (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -4.561  (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.091  (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -0.495  (  -3.0 /   1.2) 
   No. of Primary Metabolites            =         3  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      +/-
   HERG K+ Channel Blockage: log IC50    =    -4.472  (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =      1110  (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =       553  (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -2.433  (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.441  (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         0  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         0  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =       100  (<25% is poor)
   Qual. Model for Human Oral Absorption =      HIGH  (>80% is high)

      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC01151.mol:
          Name                           Similarity(%)
Isradipine                                    86.10
Aranidipine                                   85.56
Hydroxyestrone                                84.06
Repirinast                                    83.26
Nilvadipine                                   82.75

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor         0.000    H-bond Donor         0.000 
 H-bond Acceptor     -3.531    H-bond Acceptor     -3.796 
 Volume               7.547    SASA                11.577 
 Ac x Dn^.5/SASA      0.000    Ac x Dn^.5/SASA      0.000 
 FISA                -0.694    Rotor Bonds         -0.488 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.215    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                2.832    Total                3.510


                log BB                    log PMDCK
 Hydrophilic SASA    -0.878    Hydrophilic SASA    -1.027 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.181                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000    COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -0.495    Total                2.743

