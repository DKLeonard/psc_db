 Primary Metabolites & Reactive FGs:
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: tertiary alcohol E1 or SN1
 > Metabolism likely: allylic H -> alcohol
 > Reactive FG: acetal or analog
 > Reactive FG: acetal or analog
 > Reactive FG: acetal or analog
 > Metabolism likely: allylic H -> alcohol
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone
 > Metabolism likely: secondary alcohol -> ketone

 Principal Descriptors:                             (Range 95% of Drugs)
     Solute        Molecular Weight      =   780.948  ( 130.0 / 725.0)*
     Solute        Dipole Moment (D)     =    11.697  (   1.0 /  12.5) 
     Solute        Total        SASA     =  1111.429  ( 300.0 /1000.0)*
     Solute        Hydrophobic  SASA     =   818.279  (   0.0 / 750.0)*
     Solute        Hydrophilic  SASA     =   275.228  (   7.0 / 330.0) 
     Solute        Carbon Pi    SASA     =    17.922  (   0.0 / 450.0) 
     Solute        Weakly Polar SASA     =     0.000  (   0.0 / 175.0) 
     Solute        Molecular Volume (A^3)=  2203.087  ( 500.0 /2000.0)*
     Solute        vdW Polar SA (PSA)    =   204.268  (   7.0 / 200.0)*
     Solute        No. of Rotatable Bonds=    12.000  (   0.0 /  15.0) 
     Solute as Donor -    Hydrogen Bonds =     6.000  (   0.0 /   6.0) 
     Solute as Acceptor - Hydrogen Bonds =    22.450  (   2.0 /  20.0)*
     Solute Globularity   (Sphere = 1)   =     0.737  (  0.75 /  0.95)*
     Solute Ionization Potential (eV)    =    10.166  (   7.9 /  10.5) 
     Solute Electron Affinity    (eV)    =     0.686  (  -0.9 /   1.7) 
 Predictions for Properties:
   QP Polarizability (Angstroms^3)       =    74.480M (  13.0 /  70.0)*
   QP log P  for     hexadecane/gas      =    22.441M (   4.0 /  18.0)*
   QP log P  for     octanol/gas         =    46.988M (   8.0 /  35.0)*
   QP log P  for     water/gas           =    32.642M (   4.0 /  45.0) 
   QP log P  for     octanol/water       =     1.246  (  -2.0 /   6.5) 
   QP log S  for   aqueous solubility    =    -6.107M (  -6.5 /   0.5) 
   QP log S - conformation independent   =    -5.996M (  -6.5 /   0.5) 
   QP log K hsa Serum Protein Binding    =    -0.464M (  -1.5 /   1.5) 
   QP log BB for     brain/blood         =    -3.540M (  -3.0 /   1.2)*
   No. of Primary Metabolites            =         8  (   1.0 /   8.0) 
   Predicted CNS Activity (-- to ++)     =      -- 
   HERG K+ Channel Blockage: log IC50    =    -5.878M (concern below -5)
   Apparent Caco-2 Permeability (nm/sec) =        24M (<25 poor, >500 great)
   Apparent MDCK   Permeability (nm/sec) =         8M (<25 poor, >500 great)
   QP log Kp for skin permeability       =    -5.377M (Kp in cm/hr)
   Jm, max transdermal transport rate    =     0.000M (micrograms/cm^2-hr)
   Lipinski Rule of 5 Violations         =         3  (maximum is 4)
   Jorgensen Rule of 3 Violations        =         2  (maximum is 3)
   % Human Oral Absorption in GI (+-20%) =        20M (<25% is poor)
   Qual. Model for Human Oral Absorption =    MediumM (>80% is high)

       A * indicates a violation of the 95% range. # stars = 11
      An M indicates MW is outside training range.


       5 of   1712 molecules most similar to BC00394.mol:
          Name                           Similarity(%)
Metildigoxin                                  90.81
beta                                          85.41
Acetyldigoxin                                 85.39
Azithromycin                                  69.30
Gitoformate                                   67.45

    QP Breakdown (< for descriptor over training max)
                log Po/w                    -log S
 H-bond Donor        -1.799    H-bond Donor        -2.410 
 H-bond Acceptor    -10.933    H-bond Acceptor    -11.755<
 Volume              14.373    SASA                21.062<
 Ac x Dn^.5/SASA      2.195    Ac x Dn^.5/SASA      4.946 
 FISA                -1.905    Rotor Bonds         -1.954 
 Non-con amines       0.000    N Protonation        0.000 
 Non-con amides       0.000    Non-con amides       0.000 
 WPSA & PISA          0.021    WPSA                 0.000 
 Constant            -0.705    Constant            -3.783 
 Total                1.246    Total                6.107


                log BB                    log PMDCK
 Hydrophilic SASA    -3.381<   Hydrophilic SASA    -2.821 
 WPSA                 0.000    WPSA                 0.000 
 Rotor Bonds         -0.723                         0.000 
 N Protonation        0.000    Non-con amines       0.000 
 FOSA                 0.000<   COOH/SO3H acids      0.000 
 Constant             0.564    Constant             3.771
 Total               -3.540    Total                0.950

