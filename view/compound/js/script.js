$(document).ready(function () { 
	var url = window.location;
	var id = url.toString().split("compound/")[1];
	$.ajax  ({
		"url": "../../controller/compound/getCompoundById",
		"type": "post",
		"data": { "id" : id },
		success:  function (response) {
			var obj = jQuery.parseJSON(response );
			var Info;
			
			if (obj.names !="null"){
				$("#id").append(obj.id_comp);
				$("#id_aux").css("display", "table-row");
			}
			if (!(typeof obj.names === "undefined" || !obj.names)){
				$("#name_pr").append(obj.names[0]);
				for (i = 0; i < obj.names.length; i++) { 
					$("#name").append(obj.names[i] + "<br/>");
				}
				$("#name_aux").css("display", "table-row");
				$("#name_pr_aux").css("display", "table-row");
			}
			if (!(typeof obj.mol_weight === "undefined" || !obj.mol_weight)){
				$("#mol_weight").append(obj.mol_weight);
				$("#mol_aux").css("display", "table-row");
			}
			if (!(typeof obj.activity === "undefined" || !obj.activity)){
				$("#activity").append(obj.activity);
				$("#act_aux").css("display", "table-row");
			}
			if (!(typeof obj.origen === "undefined" || !obj.origen)){
				$("#origen").append(obj.origen);
				$("#origen_aux").css("display", "table-row");
			}
			if (!(typeof obj.formula === "undefined" || !obj.formula)){
				$("#formula").append(obj.formula);
				$("#form_aux").css("display", "table-row");
			}
			if (!(typeof obj.class === "undefined" || !obj.class)){
				var text;
				var name
				for (i = 0; i < obj.class.length; i++) {
					text = document.createElement("a");
					name="";
					name+="-";
					for (x = 0; x < i; x++) {
						name+="-";
					}
					name+="> ";
					name+=obj.class[i][0];
					text.appendChild(document.createTextNode(name));
					text.target="_blank";
					text.setAttribute("role","button");
					text.className ="btn btn-warning btn-outline";
					text.href="../search/" + obj.class[i][1];
					$("#class").append(text , "<br>");
				}
			}
			$("#table_max").addClass("table table-striped");
			
			if (!(typeof obj.ext_db === "undefined" || !obj.ext_db)){
				for (i = 0; i < obj.ext_db.length; i++) {
					if (obj.ext_db[i][0] != 'CAS'){
						text = document.createElement("a");
						name="";
						name+=obj.ext_db[i][0];
						text.appendChild(document.createTextNode(name));
						text.target="_blank";
						text.setAttribute("role","button");
						text.className ="btn btn-info btn-outline";
						text.href=obj.ext_db[i][1];
						$("#exter").append(text , "<br>");
					}
					if (obj.ext_db[i][0] == 'CAS'){
						$("#cas").append(obj.ext_db[i][1]);
						$("#cas_aux").css("display", "table-row");
					}
					
				}
			}
			$.ajax  ({
				"url": "../../ligprep/"+id+".txt",
				success:  function (response) {
					
					
					var text=response.replace("Primary Metabolites & Reactive FGs:", "</xmp><h5 style='color:#5cb85c'>Primary Metabolites & Reactive FGs:</h5><xmp style='display:inline'>");
					text=text.replace("Predictions for Properties:", "</xmp><h5 style='color:#5cb85c'>Predictions for Properties:</h5><xmp style='display:inline'>");
					text=text.replace("Principal Descriptors:                             (Range 95% of Drugs)", "</xmp><h5 style='color:#5cb85c'>Principal Descriptors:          (Range 95% of Drugs)</h5><xmp style='display:inline'>");
					text=text.replace("QP Breakdown (< for descriptor over training max)", "</xmp><h5 style='color:#5cb85c'>QP Breakdown (< for descriptor over training max)</h5><xmp style='display:inline'>");
					text=text.replace("log BB                    log PMDCK", "</xmp><h5 class='text-danger'>               log BB               log PMDCK</h5><xmp style='display:inline'>");
					text=text.replace("log Po/w                    -log S", "</xmp><h5 class='text-danger'>                log Po/w             -log S</h5><xmp style='display:inline'>");
					text=text.replace("Name                           Similarity(%)", "</xmp><h5 class='text-primary'>Name                           Similarity(%)</h5><xmp style='display:inline'>");
					text=text.replace("BC0", "");
					text=text.replace(".mol", "");
					text=text.replace(/(.. of   .... molecules most similar to ....:)/,"</xmp><mark style='background-color:#E0E6F8'>$1</mark><xmp style='display:inline'>");
					
					
					var arr2  = text.split("QP Breakdown (< for descriptor over training max)");
					var arr = arr2[0].split("Predictions for Properties:");
					
					arr[0]=arr[0].replace(/(.........\(..............\).)/g,"</xmp><mark style='background-color:#CEF6CE'>$1</mark><xmp style='display:inline'>");
					
					
					arr[1]=arr[1].replace(/(     \d\d\.\d\d)/g,"</xmp><mark style='background-color:#E0E6F8'>$1</mark><xmp style='display:inline'>");
					arr[1]=arr[1].replace(/( ........\(...........\))/g,"</xmp><mark style='background-color:#E0E6F8'>$1</mark><xmp style='display:inline'>");
					arr[1]=arr[1].replace(/(.........\(............\))/g,"</xmp><mark style='background-color:#E0E6F8'>$1</mark><xmp style='display:inline'>");
					arr[1]=arr[1].replace(/(.........\(..............\))/g,"</xmp><mark style='background-color:#E0E6F8'>$1</mark><xmp style='display:inline'>");
					arr[1]=arr[1].replace(/(.........\(................\))/g,"</xmp><mark style='background-color:#E0E6F8'>$1</mark><xmp style='display:inline'>");
					arr[1]=arr[1].replace(/(.........\(..................\))/g,"</xmp><mark style='background-color:#E0E6F8'>$1</mark><xmp style='display:inline'>");
					arr[1]=arr[1].replace(/(.........\(....................\))/g,"</xmp><mark style='background-color:#E0E6F8'>$1</mark><xmp style='display:inline'>");
					
					arr2[1]=arr2[1].replace(/(   ...\....  )/g,"</xmp><mark style='background-color:#F6CECE'>$1</mark><xmp style='display:inline'>");
					arr2[1]=arr2[1].replace(/(   ...\.... \n)/g,"</xmp><mark style='background-color:#F6CECE'>$1</mark><xmp style='display:inline'>");
					arr2[1]=arr2[1].replace(/(   ...\....\n)/g,"</xmp><mark style='background-color:#F6CECE'>$1</mark><xmp style='display:inline'>");
					
					$("#info1").append(arr[0]);
					$("#info2").append(arr[1]);
					$("#info3").append(arr2[1]);
				},
				error:  function (request, status, error) {
					console.log("text no found");
					$("#info1").append("No info available");
					$("#info2").append("No info available");
					$("#info3").append("No info available");
				}
			});
			
			$.ajax({
				  type: 'HEAD',
				  url: '../../model_mol/'+id+'.mol',
				  success: function (){
						Jmol.loadFile(jmol,'../../model_mol/'+id+'.mol');
						$("#img3d").attr("href", "../../model/"+id+".mol2");
						$("#img3d").attr("value", id);
				  },
				  error:  function (request, status, error) {
					  console.log("No model available");
				  }
				});
			$("#model_image").attr("src","../../img/error.png");
			var http = new XMLHttpRequest();
			http.open('HEAD', "../../images/"+id+".png", false);
			http.send();
			if( http.status != 404){
				$("#model_image").attr("src","../../images/"+id+".png");
				$("#img2d").attr("href", "../../images/"+id+".png");
			}
			else{
				console.log("No image available");
			}
			document.getElementById('2d-image').click();
		}
	});
});

function myFunction() {
    document.getElementById("myForm").submit();
}
