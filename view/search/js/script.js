var jsonData;
var graph;

$(document).ready(function() {	
	//~ Carga arbol
	$.getJSON(
		'../../controller/class/getTree',
		function(data) {
			var tree = $('#tree'),
        filter = $('#filter'),
        filtering = false,
        thread = null;
    
    tree.tree({
        data: data,
      
        
        useContextMenu: false,
        onCreateLi: function(node, $li) {
            var title = $li.find('.jqtree-title'),
                search = filter.val().toLowerCase(),
                value = title.text().toLowerCase();

            if(search !== '') {
                $li.hide();
                if(value.indexOf(search) > -1) {
                    $li.show();
                    var parent = node.parent;
                    while(typeof(parent.element) !== 'undefined') {
                        $(parent.element)
                            .show()
                            .addClass('jqtree-filtered');
                        parent = parent.parent;
                    }
                }
                if(!filtering) {
                    filtering = true;
                };
                if(!tree.hasClass('jqtree-filtering')) {
                    tree.addClass('jqtree-filtering');
                };
            } else {
                if(filtering) {
                    filtering = false;
                };
                if(tree.hasClass('jqtree-filtering')) {
                    tree.removeClass('jqtree-filtering');
                };
            };
            
        }
    });
    filter.keyup(function() {
		clearTimeout(thread);
		thread = setTimeout(function () {
			tree.tree('loadData', data);
		}, 50);
	});
		}
	);
	
	

	
	//~ Si existe una url especial
	var dataTable;
	var url = window.location;
	
	var id_extra = url.toString().split("search/")[1];
	if (id_extra != null && id_extra != "search" && $.isNumeric(id_extra) ){
		dataTable = $('#mytable').DataTable( {
			lengthChange: false,
			buttons: ['copy', 'csv', 'excel', 'pdf', 'print','selectAll','selectNone',{text: 'Download models',
                action: function ( e, dt, node, config ) {
                    var text="";
                    var f = document.createElement("form");
					f.setAttribute('method',"post");
					f.setAttribute('target',"_blank");
					f.setAttribute('action',"../../controller/models/");
                    if(dataTable.rows('.selected').data().length > 6000){
						var i = document.createElement("input");
						i.setAttribute('value',"all");
						i.setAttribute('name',"rquest");
						f.appendChild(i);
					}
					
					else{
						p=0;
						for (j = 0; j < dataTable.rows('.selected').data().length; j++) { 
							text+=dataTable.rows('.selected').data()[j][0];
							text+=",";
							if (j%50==0){
								var i = document.createElement("input");
								i.setAttribute('value',text);
								i.setAttribute('name',"rquest["+p+"]");
								f.appendChild(i);
								p++;
								text="";
							}
						}
						if (text!=""){
							var i = document.createElement("input");
							i.setAttribute('value',text);
							i.setAttribute('name',"rquest["+p+"]");
							f.appendChild(i);
						}
					}
					f.submit();
                }}],

			dom: 'Bfrtip',
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": "../../controller/compound/getClass",
				"type": "post",
				"data": { 
					"id" :	id_extra ,
				}
			}
		});
		//~ Graficos
		var ret = null;
		$.ajax  ({
			url: '../../controller/properties/getPropertiesClass',
			dataType: "json",
			type: "post",
			"data": { "id" : id_extra },
			success: function(data) {
				var jsonurl = "./jsondata.txt";
				jsonData = JSON.parse(data);
				var ret = [];
				ret.push(jsonData.stars);
				graph = $.jqplot('myChart',ret,{
					title: "Stars",
					series: [
						{renderer:$.jqplot.LineRenderer, color:'blue' },
					],

					axes: {
						yaxis: {
							min: jsonData['stars'][0],
							max: jsonData['stars'][jsonData['stars'].length -1 ]
						}
					}
				});
				createit(jsonData['stars']);
				document.getElementById('graph-aux').click();
				document.getElementById('table-aux').click();
				$('#loader').css("display","none");
			},
			error: function() {
				alert('Error occured');
			}
		});	
		//~ Graficos
		
	}
	else{
		dataTable = $('#mytable').DataTable( {
			lengthChange: false,

			dom: 'Bfrtip',
			buttons: ['copy', 'csv', 'excel', 'pdf', 'print','selectAll','selectNone',{text: 'Download models',
                action: function ( e, dt, node, config ) {
                    var text="";
					var f = document.createElement("form");
					f.setAttribute('method',"post");
					f.setAttribute('target',"_blank");
					f.setAttribute('action',"../../controller/models/");
					if(dataTable.rows('.selected').data().length > 6000){
						var i = document.createElement("input");
						i.setAttribute('value',"all");
						i.setAttribute('name',"rquest");
						f.appendChild(i);
					}
					
					else{
						p=0;
						for (j = 0; j < dataTable.rows('.selected').data().length; j++) { 
							text+=dataTable.rows('.selected').data()[j][0];
							text+=",";
							if (j%50==0){
								var i = document.createElement("input");
								i.setAttribute('value',text);
								i.setAttribute('name',"rquest["+p+"]");
								f.appendChild(i);
								p++;
								text="";
							}
						}
						if (text!=""){
							var i = document.createElement("input");
							i.setAttribute('value',text);
							i.setAttribute('name',"rquest["+p+"]");
							f.appendChild(i);
						}
					}
					f.submit();
                }}],
			"ajax": "../../controller/compound/getCompounds"	
		});
		//~ Graficos
		$.ajax  ({
			url: '../../controller/properties/getProperties',
			success: function(data) {
				jsonData = JSON.parse(data);
				var ret = [];
				ret.push(jsonData.stars);
				
				
				graph = $.jqplot('myChart',ret,{
					title: "Stars",
					series: [
						{renderer:$.jqplot.LineRenderer, color:'blue' },
					],
					axes: {
						yaxis: {
							min: jsonData['stars'][0],
							max: jsonData['stars'][jsonData['stars'].length -1 ]
						}
					}
				});
				createit(jsonData['stars']);
				document.getElementById('graph-aux').click();
				document.getElementById('table-aux').click();
				$('#loader').css("display","none");
			},
			error: function() {
				alert('Error occured');
			}
		});		
		//~ Graficos
	}
	
	
	//~ BUSQUEDA DE INFORMACION
	$('#button2').click( function () {
		var values = {};
		var x=0;
		var $inputs = $('#form-esp :input');
		$inputs.each(function() {
			values[this.name] = $(this).val();
			
			if($(this).val() != ""){
				x=1;
			}
		});
		if (x==1){
			dataTable.clear().draw().destroy();
			dataTable = $('#mytable').DataTable( {
				lengthChange: false,
	
				dom: 'Bfrtip',
				buttons: ['copy', 'csv', 'excel', 'pdf', 'print','selectAll','selectNone',{text: 'Download models',
                action: function ( e, dt, node, config ) {
                    var text="";
					var f = document.createElement("form");
					f.setAttribute('method',"post");
					f.setAttribute('target',"_blank");
					f.setAttribute('action',"../../controller/models/");
                    if(dataTable.rows('.selected').data().length > 6000){
						var i = document.createElement("input");
						i.setAttribute('value',"all");
						i.setAttribute('name',"rquest");
						f.appendChild(i);
					}
					
					else{
						p=0;
						for (j = 0; j < dataTable.rows('.selected').data().length; j++) { 
							text+=dataTable.rows('.selected').data()[j][0];
							text+=",";
							if (j%50==0){
								var i = document.createElement("input");
								i.setAttribute('value',text);
								i.setAttribute('name',"rquest["+p+"]");
								f.appendChild(i);
								p++;
								text="";
							}
						}
						if (text!=""){
							var i = document.createElement("input");
							i.setAttribute('value',text);
							i.setAttribute('name',"rquest["+p+"]");
							f.appendChild(i);
						}
					}
					f.submit();
                }}],
				"ajax": {
					"url": "../../controller/compound/searchCompound",
					"type": "post",
					"data": { 
						"id" 		:	values["id_comp"] ,
						"name" 		:	values["name"] ,
						"min_num"	:	values["min_num"] ,
						"max_num"	:	values["max_num"] ,
						"formula" 	:	values["formula"],
						"type"		:	values["type"],
						"activity"	:	values["activity"],
						"origen"	:	values["origen"],
						}
				}
			});
			$.ajax  ({
				url: '../../controller/properties/getPropertiesSearch',
				"type": "post",
				"data": { 
						"id" 		:	values["id_comp"] ,
						"name" 		:	values["name"] ,
						"min_num"	:	values["min_num"] ,
						"max_num"	:	values["max_num"] ,
						"formula" 	:	values["formula"],
						"type"		:	values["type"],
						"activity"	:	values["activity"],
						"origen"	:	values["origen"],
						},
				success: function(data) {
					jsonData = JSON.parse(data);
					var ret = [];
					ret.push(jsonData.stars);
					
					//~ if(graph){
						//~ graph.destroy();
					//~ }
					graph = $.jqplot('myChart',ret,{
						title: "Stars",
						series: [
							{renderer:$.jqplot.LineRenderer, color:'blue' },
						],
						axes: {
							yaxis: {
								min: jsonData['stars'][0],
								max: jsonData['stars'][jsonData['stars'].length -1 ]
							}
						}
					}).replot(true);
					createit(jsonData['stars']);
					document.getElementById('graph-aux').click();
					document.getElementById('table-aux').click();
				},
				error: function() {
					alert('Error occured');
				}
			});		
			
		}
	});
	
	//~  Cuando se selecciona algo del arbol
	$('#tree').bind(
		'tree.click',
		function(event) {
			var node = event.node;
			var id = node.id;
			if (id) {
				
				dataTable.clear().draw().destroy();
				dataTable = $('#mytable').DataTable( {
					lengthChange: false,
		
					dom: 'Bfrtip',
					buttons: ['copy', 'csv', 'excel', 'pdf', 'print','selectAll','selectNone',{text: 'Download models',
                action: function ( e, dt, node, config ) {
                    var text="";
					var f = document.createElement("form");
					f.setAttribute('method',"post");
					f.setAttribute('target',"_blank");
					f.setAttribute('action',"../../controller/models/");
                    if(dataTable.rows('.selected').data().length > 6000){
						var i = document.createElement("input");
						i.setAttribute('value',"all");
						i.setAttribute('name',"rquest");
						f.appendChild(i);
					}
					
					else{
						p=0;
						for (j = 0; j < dataTable.rows('.selected').data().length; j++) { 
							text+=dataTable.rows('.selected').data()[j][0];
							text+=",";
							if (j%50==0){
								var i = document.createElement("input");
								i.setAttribute('value',text);
								i.setAttribute('name',"rquest["+p+"]");
								f.appendChild(i);
								p++;
								text="";
							}
						}
						if (text!=""){
							var i = document.createElement("input");
							i.setAttribute('value',text);
							i.setAttribute('name',"rquest["+p+"]");
							f.appendChild(i);
						}
					}
					f.submit();
                }}],
					"ajax": {
						"url": "../../controller/compound/getClass",
						"type": "post",
						"data": { 
							"id" :	id ,
						}
					}
				});
				$.ajax  ({
					url: '../../controller/properties/getPropertiesClass',
					"type": "post",
					"data": { "id" : id },
					success: function(data) {
						jsonData = JSON.parse(data);
						var ret = [];
						ret.push(jsonData.stars);
						graph = $.jqplot('myChart',ret,{
							title: "Stars",
							series: [
								{renderer:$.jqplot.LineRenderer, color:'blue' },
							],
							axes: {
								yaxis: {
									min: jsonData['stars'][0],
									max: jsonData['stars'][jsonData['stars'].length -1 ]
								}
							}
						}).replot(true);
						createit(jsonData['stars']);
						document.getElementById('graph-aux').click();
						document.getElementById('table-aux').click();
						
					},
					error: function() {
						alert('Error occured');
					}
				});		
			}
		}
	);
	
	
	
	
	

});


$('select').on('change', function() {
	var ret = [];
	ret.push(jsonData[this.value]);
	graph = $.jqplot('myChart',ret,{
		title: this.value,
		axes: {
			yaxis: {
				min: jsonData[this.value][0],
				max: jsonData[this.value][jsonData[this.value].length -1 ]
			}
		}
	}).replot(true);
	createit(jsonData[this.value]);
});

function createit(data2){
		var data =data2;
		$("#label").html('<div class="panel-heading text-center" style="background-color:#4CAF50"><b>STATISTICS</b></div>');
		$("#Statistics").html("");
		$("#Statistics").append("<tr><td>MIN :</td><td>"+ss.minSorted(data2)+"</td></tr>");
		$("#Statistics").append("<tr><td>MAX :</td><td>"+ss.maxSorted(data2)+"</td></tr>");
		$("#Statistics").append("<tr><td>MEAN:</td><td>"+Math.round(ss.mean(data2))+"</td></tr>");
		$("#Statistics").append("<tr><td>VARIANCE:</td><td>"+Math.round(ss.variance(data2))+"</td></tr>");
		$("#Statistics").append("<tr><td>STANDART DEV.:</td><td>"+Math.round(ss.standardDeviation(data2))+"</td></tr>");
        var buckets = 10;
		var last = data.slice(-1)[0];
		var rstep = Math.round((last/buckets)*100)/100;
		var ranges = [];
		var values = [];
		var next = 0;

		for (i=0; i<buckets; i++){ 
			if(i<buckets-1){
				var first = next;
				var next = Math.round((next + rstep));
				var bin = first+"\n-\n"+next;
			} else {
				var mylast = Math.round(last);
				var bin = next+"\n-\n"+mylast;
				var next = mylast;
			}
			var count = 0;

			while (data[0]<=next) {
				count++;
				data.shift();
			};    

			values.push(count);
			ranges.push(bin);
		}
		plot2 = $.jqplot('myChart2', [values,values], {
			title:'Histogram',
            axesDefaults: {
                tickRenderer: $.jqplot.CanvasAxisTickRenderer ,
                tickOptions: {
                  fontSize: '14px'
                }
            },
			series: [
				{renderer:$.jqplot.BarRenderer,color:'green' },
				{renderer:$.jqplot.LineRenderer, color:'blue' },
			],
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,    
                rendererOptions: {
					smooth: true,
                    barWidth: null,
                    barPadding: 0,
                    barMargin: 5
                }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ranges
                }
            }
    }).replot(true);  
      
	
}
