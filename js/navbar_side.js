
$(function(){
			     		
			$.ajax(
			{	async: false,
				cache: true,
				timeout: 80000,
               
                url: '../../controller/usuarios/obtenerSideBar',
                type:  'post', 
				dataType:'JSON',	
                success:  function (response) {
					
					console.log(response);
						
						var usuarioNombre=response['nombre']+" "+response['apellido'];
						var usuarioRol=response['rol'];
						var acreditacion=response['acreditacion'];
						var historico=response['historico'];
						var foto=response['foto'];
						var html='<nav class="navbar-default navbar-static-side" role="navigation">'+
								'<div class="sidebar-collapse">'+
								 '   <ul class="nav" id="side-menu">'+
								  '      <li class="nav-header">'+
								   '         <div class="dropdown profile-element"> <span>'+
									'            <img alt="image" class="img-circle img-responsive" src="../../img/profiles/'+foto+'">'+
									 '            </span>'+
									  '          <a data-toggle="dropdown" class="dropdown-toggle" href="#">'+
									   '         <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">'+usuarioNombre+'</strong>'+
										'         </span> <span class="text-muted text-xs block">'+usuarioRol+'<b class="caret"></b></span> </span> </a>'+
										 '       <ul class="dropdown-menu animated fadeInRight m-t-xs">'+
										  '          <li><a href="perfil.html">Mi perfil</a></li>'+         
							  
											 '       <li class="divider"></li>'+
												'      <li><a href="login.html">Salir</a></li>'+
												   ' </ul>'+
												'</div>'+
												'<div class="logo-element">'+
												 ' <i class="fa fa-university">+</i>'+
												'</div>'+
											'</li>'+
											
											
											' <li><a href="../index/home"><i class="fa fa-home"></i> Inicio</a></li>'+
											'<li>'+
											
											 '<a href="#"><i class="fa fa-graduation-cap"></i> <span class="nav-label">Acreditación</span><span class="fa arrow"></span></a>'+
											' <ul class="nav nav-second-level">'+
											acreditacion+						
											' </ul>'+
											'</li>'+
											' <li>'+
											historico+
											'</li>'+
										'</ul>'+
										'</div>'+
									'</nav>';
									
								$('#side_bar').html(html);
											}
			
			
			});
	
});
