$(function(){
	var html='';
	html = '<link href="../../css/navbar.css" rel="stylesheet">'+
	'<nav class="navbar navbar-default navbar-fixed-top" style="z-index:9002">'+
	'	<div class="container-fluid">'+
	'		<div class="navbar-header">'+
	'			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">'+
	'				<span class="icon-bar"></span>'+
	'				<span class="icon-bar"></span>'+
	'				<span class="icon-bar"></span>'+
	'			</button>'+
	'			<a class="navbar-brand" title="Go to home"  href="../../view/index/index"><img src="../../img/logo_mini.svg" style="width:50px; margin-top: -13px;"></img></a>'+
	'		</div>'+
	'		<div class="collapse navbar-collapse" id="myNavbar">'+
	'			<ul class="nav navbar-nav">'+
	'				<li class="nav-item"><a class="nav-link" title="Search in the databases" href="../../view/search/search">Search</a></li>'+
	'				<li class="nav-item"><a class="nav-link" title="Frecuent Questions" href="../../view/help/help">Help</a></li>'+
	'				<li class="nav-item"><a class="nav-link" title="Contact us" href="../../view/contact/contact">Contact</a></li>'+
	'			</ul>'+
	'		</div>'+
	'	</div>'+
	'</nav>'+
	'<br/><br/>'+
	'';
	$('#top_bar').html(html);
});
