<?php
//conexion a la DB
session_start();
//objetos de persistencia de datos



require_once ("../../config/medoo.php");
require_once ("../../config/conexion.php");

//objetos DAO, 
require_once("../../VO/CompoundVO.php");
require_once("../../DAO/CompoundDAO.php");

require_once("../../config/Rest.inc.php");

class controlador_compound extends REST {
	
	public function __construct(){
		parent::__construct();// Init parent contructor
	}
	
	public function processApi(){
		$func = strtolower(trim(str_replace("/","",$_REQUEST['rquest'])));
		if((int)method_exists($this,$func) > 0){
			$this->$func();
		}
		else{
			$this->response('',404);	//Si no existe el metodo en esta clase, la respuesta ser� "Page not found".
		}
	}

	function getCompoundById() //obtiene todos los datos del compuesto en formato JSON
	{
		$id;
		if (isset($_POST['id']))
			$id=$_POST['id'];
		
		$compound=CompoundDAO::getCompoundById($id);	
		echo json_encode($compound);
	}
	
	function getClass() //obtiene todos los datos del compuesto en formato JSON
	{
		$id_class;
		if (isset($_POST['id']))
			$id_class=$_POST['id'];
		
		$compounds=CompoundDAO::getClass($id_class);	
		echo '{"draw":0,"recordsTotal":"';
		echo count($compounds);
		echo '","recordsFiltered":"';
		echo count($compounds);
		echo '", "data" :[';
		$i=0;
		if(count($compounds)>0){
			foreach  ($compounds as $compound){
				if(!$i)
					$i=1;
				else
					echo ',';	
				echo '["';
				echo $compound->getId_comp();
				echo '","';
				echo "<a href='../compound/";	
				echo $compound->getId_comp();
				echo "'>";
				echo str_replace("'","",str_replace('"',"",$compound->getNames()));
				echo '</a>';
				echo '","';
				echo $compound->getFormula();
				echo '","';
				echo $compound->getMol_weight();
				echo '"]';
				//~ 
			}
		}
		echo '] }';
	}
	
	
	
	function getCompounds()
	{
		$compounds=CompoundDAO::getCompounds();	
		$compound=null;
		echo '{"draw":0,"recordsTotal":"';
		echo count($compounds);
		echo '","recordsFiltered":"';
		echo count($compounds);
		echo '", "data" :[';
		$i=0;
		foreach  ($compounds as $compound){
			if(!$i)
				$i=1;
			else
				echo ',';	
			echo '["';	
			echo $compound->getId_comp();
			echo '","';
			echo "<a href='../compound/";	
			echo $compound->getId_comp();
			echo "'>";
			echo str_replace("'","",str_replace('"',"",$compound->getNames()));
			echo '</a>';
			echo '","';
			echo $compound->getFormula();
			echo '","';
			echo $compound->getMol_weight();
			echo '"]';
			
		}
		echo '] }';
	}
	
	function searchCompound()
	{
		$id_comp=null;
		$name=null;
		$mol_weight_max=null;
		$mol_weight_min=null;
		$formula=null;
		$type=null;
		$activity=null;
		$origen=null;
		$class=null;
		$ext_db=null;
		if (isset($_POST['id'])){
			$id_comp=$_POST['id'] ;
		}
		if (isset($_POST['name'])){
			$name=$_POST['name'];
		}
		if (isset($_POST['min_num'])){
			$mol_weight_min=$_POST['min_num'];
		}
		if (isset($_POST['max_num'])){
			$mol_weight_max=$_POST['max_num'];
		}
		if (isset($_POST['formula'])){
			$formula=$_POST['formula'];
		}
		if (isset($_POST['type'])){
			$type=$_POST['type'];
		}
		if (isset($_POST['activity'])){
			$activity=$_POST['activity'];
		}
		if (isset($_POST['origen'])){
			$origen=$_POST['origen'];
		}
		if (isset($_POST['class'])){
			$class=$_POST['class'];
		}
		if (isset($_POST['ext_db'])){
			$ext_db=$_POST['ext_db'];
		}
		
		$compounds=CompoundDAO::searchCompound($id_comp,$name,$mol_weight_min,$mol_weight_max,$formula,$activity,$origen,$class,$ext_db);
#		echo $compounds;
		echo '{"draw":0,"recordsTotal":"';
		echo count($compounds);
		echo '","recordsFiltered":"';
		echo count($compounds);
		echo '", "data" :[';
		$i=0;
		if(count($compounds)>0){
			foreach  ($compounds as $compound){
				if(!$i)
					$i=1;
				else
					echo ',';	
				echo '["';	
				echo $compound->getId_comp();
				echo '","';
				echo "<a href='../compound/";	
				echo $compound->getId_comp();
				echo "'>";
				echo str_replace("'","",str_replace('"',"",$compound->getNames()));
				echo '</a>';
				echo '","';
				echo $compound->getFormula();
				echo '","';
				echo $compound->getMol_weight();
				echo '"]';
				//~ 
			}
		}
		echo '] }';
		
	}
	
	function getMol()
	{
		$compounds=CompoundDAO::getMol();
		echo "[";
		$x=0;
		foreach ($compounds as $compound){
			if($x==0)
				$x=1;
			else
				echo ",";
			echo $compound;
			
		}
		echo "]";
	}
	function getMolClass()
	{
		$id_class;
		if (isset($_POST['id']))
			$id_class=$_POST['id'];
		
		$compounds=CompoundDAO::getMolClass($id_class);
		echo "[";
		$x=0;
		foreach ($compounds as $compound){
			if($x==0)
				$x=1;
			else
				echo ",";
			echo $compound;
		}
		echo "]";
	}
	function getMolSearch()
	{
		$id_comp=null;
		$name=null;
		$mol_weight_max=null;
		$mol_weight_min=null;
		$formula=null;
		$type=null;
		$activity=null;
		$origen=null;
		$class=null;
		$ext_db=null;
		if (isset($_POST['id'])){
			$id_comp=$_POST['id'] ;
		}
		if (isset($_POST['name'])){
			$name=$_POST['name'];
		}
		if (isset($_POST['min_num'])){
			$mol_weight_min=$_POST['min_num'];
		}
		if (isset($_POST['max_num'])){
			$mol_weight_max=$_POST['max_num'];
		}
		if (isset($_POST['formula'])){
			$formula=$_POST['formula'];
		}
		if (isset($_POST['type'])){
			$type=$_POST['type'];
		}
		if (isset($_POST['activity'])){
			$activity=$_POST['activity'];
		}
		if (isset($_POST['origen'])){
			$origen=$_POST['origen'];
		}
		if (isset($_POST['class'])){
			$class=$_POST['class'];
		}
		if (isset($_POST['ext_db'])){
			$ext_db=$_POST['ext_db'];
		}
		
		$compounds=CompoundDAO::getMolSearch($id_comp,$name,$mol_weight_min,$mol_weight_max,$formula,$activity,$origen,$class,$ext_db);
		echo "[";
		$x=0;
		foreach ($compounds as $compound){
			if($x==0)
				$x=1;
			else
				echo ",";
			echo $compound;
		}
		echo "]";
	}
	
	
	function insertCompound()
	{
		$id=$_POST['id'];
		$name=$_POST['name'];
		$mol_weight=$_POST['mol_weight'];
		$formula=$_POST['formula'];
		$type=$_POST['type'];
		$activity=$_POST['activity'];
		$origen=$_POST['origen'];
		$class=$_POST['class'];
		$ext_db=$_POST['ext_db'];
		$compound=CompoundDAO::insertCompound($id_comp,$name,$mol_weight,$formula,$type,$activity,$origen,$class,$ext_db);
		echo json_encode($compound);	
	}
	function deleteCompound()
	{
		if($_SESSION['USER_PRIVILEGIO']==12)
		{
			$id=$_POST['id'];
			$compound=CompoundDAO::deleteCompound($id);	
			echo json_encode($compound);	
		}
	}
}
$api = new controlador_compound();
$api->processApi();

?>
