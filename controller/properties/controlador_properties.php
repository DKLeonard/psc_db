<?php
//conexion a la DB
session_start();
//objetos de persistencia de datos



require_once ("../../config/medoo.php");
require_once ("../../config/conexion.php");

//objetos DAO, 
require_once("../../VO/PropertiesVO.php");
require_once("../../DAO/PropertiesDAO.php");

require_once("../../config/Rest.inc.php");

class controlador_properties extends REST {
	
	public function __construct(){
		parent::__construct();// Init parent contructor
	}
	
	public function processApi(){
		$func = strtolower(trim(str_replace("/","",$_REQUEST['rquest'])));
		if((int)method_exists($this,$func) > 0){
			$this->$func();
		}
		else{
			$this->response('',404);	//Si no existe el metodo en esta clase, la respuesta ser� "Page not found".
		}
	}

	function getPropertiesById() //obtiene todos los datos del compuesto en formato JSON
	{
		$id;
		if (isset($_POST['id']))
			$id=$_POST['id'];
		
		$properties=PropertiesDAO::getPropertiesById($id);	
		echo json_encode($properties);
	}
	
	function getProperties()
	{
		$properties=PropertiesDAO::getProperties();
		echo json_encode($properties);
	}
	function getPropertiesClass()
	{
		$id_class;
		if (isset($_POST['id']))
			$id_class=$_POST['id'];
		
		$properties=PropertiesDAO::getPropertiesClass($id_class);
		echo json_encode($properties);
	}
	function getPropertiesSearch()
	{
		$id_comp=null;
		$name=null;
		$mol_weight_max=null;
		$mol_weight_min=null;
		$formula=null;
		$type=null;
		$activity=null;
		$origen=null;
		$class=null;
		$ext_db=null;
		if (isset($_POST['id'])){
			$id_comp=$_POST['id'] ;
		}
		if (isset($_POST['name'])){
			$name=$_POST['name'];
		}
		if (isset($_POST['min_num'])){
			$mol_weight_min=$_POST['min_num'];
		}
		if (isset($_POST['max_num'])){
			$mol_weight_max=$_POST['max_num'];
		}
		if (isset($_POST['formula'])){
			$formula=$_POST['formula'];
		}
		if (isset($_POST['type'])){
			$type=$_POST['type'];
		}
		if (isset($_POST['activity'])){
			$activity=$_POST['activity'];
		}
		if (isset($_POST['origen'])){
			$origen=$_POST['origen'];
		}
		if (isset($_POST['class'])){
			$class=$_POST['class'];
		}
		if (isset($_POST['ext_db'])){
			$ext_db=$_POST['ext_db'];
		}
				
		$properties=PropertiesDAO::getPropertiesSearch($id_comp,$name,$mol_weight_min,$mol_weight_max,$formula,$activity,$origen,$class,$ext_db);
		echo json_encode($properties);
	}
	
	//~ function searchProperties()
	//~ {
	//~ 	$properties=PropertiesDAO::searchProperties($id_comp,$name,$mol_weight_min,$mol_weight_max,$formula,$activity,$origen,$class,$ext_db);
	//~ 	echo json_encode($properties);
	//~ }
}
$api = new controlador_properties();
$api->processApi();

?>
