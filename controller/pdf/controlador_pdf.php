<?php
//conexion a la DB
session_start();
//objetos de persistencia de datos



require_once ("../../config/medoo.php");
require_once ("../../config/conexion.php");

//objetos DAO, 
require_once("../../VO/CompoundVO.php");
require_once("../../DAO/PdfDAO.php");

require_once("../../config/Rest.inc.php");

class controlador_compound extends REST {
	
	public function __construct(){
		parent::__construct();// Init parent contructor
	}
	
	public function processApi(){
		$func = strtolower(trim(str_replace("/","",$_REQUEST['rquest'])));
		if((int)method_exists($this,$func) > 0){
			$this->$func();
		}
		else{
			$this->response('',404);	//Si no existe el metodo en esta clase, la respuesta ser� "Page not found".
		}
	}

	function getPdf() //obtiene todos los datos del compuesto en formato JSON
	{
		$id=0;
		if (isset($_POST['id']))
			$id=$_POST['id'];
		
		$compound=PdfDAO::getPdf($id);	
		//~ Creador de PDF
		require('../../apis/fpdf/fpdf.php');
		$pdf= new FPDF();
		$pdf->SetAuthor('Hector Montecino Garrido');
		$pdf->SetTitle($id);
		$pdf->SetFont('Courier','B',20);
		$pdf->AddPage('P');
		$pdf->Image("../../img/logo_mini.png",5,10,40,0,"png");
		$pdf->SetFontSize(10);
		$pdf->SetY(35);
		$pdf->SetX(70);
		
		$pdf->SetFontSize(23);
		$pdf->Write(4, $compound->names[0]."\n\n");
		$pdf->SetFontSize(10);
		$pdf->Ln();
		$pdf->Ln();
		$num=1;
		
		$pdf->SetFontSize(12);
			$pdf->Write(4,"Entry number:\n");
			$pdf->SetFontSize(10);
			$pdf->Write(4,"                   ".$compound->id_comp."\n\n");
			$pdf->SetFontSize(12);
			$pdf->Write(4,"Names:\n");
			$pdf->SetFontSize(10);
			foreach ($compound->names as $name){
				$pdf->Write(4,"                   ".$name."\n");
			}
			$pdf->SetFontSize(12);
			$pdf->Write(4,"\nChemical Formula:\n");
			$pdf->SetFontSize(10);
			$pdf->Write(4,"                   ".$compound->formula."\n");
			
			
			if ($compound->mol_weight!=''){
				$pdf->SetFontSize(12);
				$pdf->Write(4,"Molecular weight (g/mol):\n");
				$pdf->SetFontSize(10);
				$pdf->Write(4,"                   ".$compound->mol_weight."\n");
			}
			if (file_exists ("../../images/".$id.".png")){
				$pdf->Image("../../images/".$id.".png",110,50,80,0,"png");
			}
			if( file_exists ("../../ligprep/".$id.".txt")){
				$pdf->SetFontSize(12);
				$pdf->Ln();
				$pdf->Write(4,"Physicochemical properties:\n\n");
				$pdf->SetFontSize(10);
				$myfile = fopen("../../ligprep/".$id.".txt", "r") or die("Unable to open file!");
				while ($duck = fgets($myfile)){
					$pdf->Write(4,"$duck");
				}
				fclose($myfile);
				$pdf->Write(4,"Legend:\n");
				$pdf->Write(4,"SASA: Solvent accessible surface area\n");
				$pdf->Write(4,"PSA: Polar surface area\n");
				$pdf->Write(4,"Solute as Donor -    Hydrogen Bonds: Estimated number of H-Bond that would be donated by the solute to water molecules in an aqueous solutions. Values are averages takes over a number of conformations, so they can be non-integer\n");
				$pdf->Write(4,"Solute as Acceptor - Hydrogen Bonds: Estimated number of H-Bond that would be accepted by the solute to water molecules in an aqueous solutions. Values are averages takes over a number of conformations, so they can be non-integer\n");
				$pdf->Write(4,"Log P: Partition coefficient\n");
				$pdf->Write(4,"CNS: Central Nervous system\n");
				$pdf->Write(4,"Caco-2: is a continuous cell of heterogeneous human epithelial colorectal adenocarcinoma cells\n");
				$pdf->Write(4,"MDCK: Madin-Darby Canine Kidney Epithelial Cells\n");
			}
			$pdf->Output("Report_".$id.".pdf",'D');
		
		//~ END Creador de PDF
	}
	
}
$api = new controlador_compound();
$api->processApi();

?>
