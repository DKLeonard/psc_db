<?php
//objetos de persistencia de datos
require_once ("../../config/medoo.php");
require_once ("../../config/conexion.php");

//objetos DAO, 
require_once("../../DAO/ClassDAO.php");
require_once("../../VO/ClassVO.php");
require_once("../../config/Rest.inc.php");


class controlador_class extends REST {
	
	public function __construct(){
		parent::__construct();// Init parent contructor
	}
	
	public function processApi(){
		$func = strtolower(trim(str_replace("/","",$_REQUEST['rquest'])));
		if((int)method_exists($this,$func) > 0){
			$this->$func();
		}
		else{
			$this->response('',404);	//Si no existe el metodo en esta clase, la respuesta será "Page not found".
		}
	}

	function getTree()
	{
		$file=ClassDAO::getTree();	
		echo json_encode($file);
	}
	function getNode()
	{
		$file=ClassDAO::getNode();	
		echo json_encode($file);
	}
}
$api = new controlador_class();
$api->processApi();

?>
