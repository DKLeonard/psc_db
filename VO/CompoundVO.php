<?php
    class Compound {
		
		public $id_comp;
		public $names=[];
		public $formula;
		public $mol_weight;
		public $type;
		public $activity;
		public $origen;
		public $class=[];
		public $ext_db=[];
	
		public function __construct() {

        }
		
		public function getId_comp(){
			return $this->id_comp;
		}

		public function setId_comp($id_comp){
			$this->id_comp = $id_comp;
		}

		public function getMol_weight(){
			return $this->mol_weight;
		}

		public function setMol_weight($mol_weight){
			$this->mol_weight = $mol_weight;
		}

		public function getFormula(){
			return $this->formula;
		}

		public function setFormula($formula){
			$this->formula = $formula;
		}

		public function getType(){
			return $this->type;
		}

		public function setType($type){
			$this->type = $type;
		}

		public function getActivity(){
			return $this->activity;
		}

		public function setActivity($activity){
			$this->activity = $activity;
		}

		public function getOrigen(){
			return $this->origen;
		}

		public function setOrigen($origen){
			$this->origen = $origen;
		}

		public function getNames(){
			return $this->names;
		}

		public function setNames($names){
			$this->names = $names;
		}

		public function getClass(){
			return $this->class;
		}

		public function setClass($class){
			$this->class = $class;
		}

		public function getExt_db(){
			return $this->ext_db;
		}

		public function setExt_db($ext_db){
			$this->ext_db = $ext_db;
		}
	}
?>
