<?php
    class ClassVO {
		
		
		public $id;
		public $label;
		private $id_father;
		private $level;
		public $children=[];
		
	
		public function __construct() {

        }
		
		public function getId_class(){
			return $this->id;
		}

		public function setId_class($id_class){
			$this->id = $id_class;
		}

		public function getName_class(){
			return $this->label;
		}

		public function setName_class($name_class){
			$this->label = $name_class;
		}

		public function getId_father(){
			return $this->id_father;
		}

		public function setId_father($id_father){
			$this->id_father = $id_father;
		}
		public function getLevel(){
			return $this->level;
		}

		public function setLevel($level){
			$this->level = $level;
		}

		public function getChildren(){
			return $this->children;
		}

		public function setChildren($children){
			$this->children = $children;
		}

	}
?>
